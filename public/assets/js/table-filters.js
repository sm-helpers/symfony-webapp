/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 5/15/18, 12:53 AM
 */

/*
    Table filters accepted values

    'followers', 'following', 'muted', 'whitelisted', 'blacklisted', 'profile_pic', 'verified_accounts',

 */

function createFiltersForTable(table_filters, hide_others) {
    if (hide_others !== true) {
        hide_others = false;
    }
    var filter_actions_ele = $("div.filter_actions");

    var followers = "disabled";
    if (inArray('followers', table_filters)) {
        followers = "";
    }
    if ((hide_others && inArray('followers', table_filters)) || (!hide_others)) {
        var my_followers_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + followers + ' checkbox my_followers">\n' +
            '    <input type="checkbox" name="my_followers">\n' +
            '    <label>Show my Followers</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + followers + ' checkbox not_my_followers">\n' +
            '    <input type="checkbox" name="not_my_followers">\n' +
            '    <label>Show non Followers</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(my_followers_html);
        $('.my_followers.checkbox').checkbox({
            onChecked: function () {
                $('.not_my_followers.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.not_my_followers.checkbox').checkbox({
            onChecked: function () {
                $('.my_followers.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }


    var following = "disabled";
    if (inArray('following', table_filters)) {
        following = "";
    }
    if ((hide_others && inArray('following', table_filters)) || (!hide_others)) {
        var iam_following_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + following + ' checkbox iam_following">\n' +
            '    <input type="checkbox" name="iam_following">\n' +
            '    <label>I\'m following</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + following + ' checkbox iam_not_following">\n' +
            '    <input type="checkbox" name="iam_not_following">\n' +
            '    <label>I\'m not following</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(iam_following_html);
        $('.iam_following.checkbox').checkbox({
            onChecked: function () {
                $('.iam_not_following.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.iam_not_following.checkbox').checkbox({
            onChecked: function () {
                $('.iam_following.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }

    var muted = "disabled";
    if (inArray('muted', table_filters)) {
        muted = "";
    }
    if ((hide_others && inArray('muted', table_filters)) || (!hide_others)) {
        var muted_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + muted + ' checkbox muted">\n' +
            '    <input type="checkbox" name="muted">\n' +
            '    <label>Show Muted</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + muted + ' checkbox not_muted">\n' +
            '    <input type="checkbox" name="not_muted">\n' +
            '    <label>Skip Muted</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(muted_html);
        $('.muted.checkbox').checkbox({
            onChecked: function () {
                $('.not_muted.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.not_muted.checkbox').checkbox({
            onChecked: function () {
                $('.muted.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }

    var whitelisted = "disabled";
    if (inArray('whitelisted', table_filters)) {
        whitelisted = "";
    }
    if ((hide_others && inArray('whitelisted', table_filters)) || (!hide_others)) {
        var whitelisted_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + whitelisted + ' checkbox whitelisted">\n' +
            '    <input type="checkbox" name="whitelisted">\n' +
            '    <label>Show Whitelisted</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + whitelisted + ' checkbox skip_whitelisted">\n' +
            '    <input type="checkbox" name="skip_whitelisted">\n' +
            '    <label>Skip Whitelisted</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(whitelisted_html);
        $('.whitelisted.checkbox').checkbox({
            onChecked: function () {
                $('.blacklisted.checkbox').checkbox("set unchecked");
                $('.skip_whitelisted.checkbox').checkbox("set unchecked");
                $('.skip_blacklisted.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.skip_whitelisted.checkbox').checkbox({
            onChecked: function () {
                $('.whitelisted.checkbox').checkbox("set unchecked");
                $('.blacklisted.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }

    var blacklisted = "disabled";
    if (inArray('blacklisted', table_filters)) {
        blacklisted = "";
    }
    if ((hide_others && inArray('blacklisted', table_filters)) || (!hide_others)) {
        var blacklisted_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + blacklisted + ' checkbox blacklisted">\n' +
            '    <input type="checkbox" name="blacklisted">\n' +
            '    <label>Show Blacklisted</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + blacklisted + ' checkbox skip_blacklisted">\n' +
            '    <input type="checkbox" name="skip_blacklisted">\n' +
            '    <label>Skip Blacklisted</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(blacklisted_html);
        $('.blacklisted.checkbox').checkbox({
            onChecked: function () {
                $('.whitelisted.checkbox').checkbox("set unchecked");
                $('.skip_whitelisted.checkbox').checkbox("set unchecked");
                $('.skip_blacklisted.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.skip_blacklisted.checkbox').checkbox({
            onChecked: function () {
                $('.whitelisted.checkbox').checkbox("set unchecked");
                $('.blacklisted.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }

    var profile_pic = "disabled";
    if (inArray('profile_pic', table_filters)) {
        profile_pic = "";
    }
    if ((hide_others && inArray('profile_pic', table_filters)) || (!hide_others)) {
        var show_with_profile_pic_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + profile_pic + ' checkbox show_with_profile_pic">\n' +
            '    <input type="checkbox" name="show_with_profile_pic">\n' +
            '    <label>Show with Profile Pic</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + profile_pic + ' checkbox show_without_profile_pic">\n' +
            '    <input type="checkbox" name="show_without_profile_pic">\n' +
            '    <label>Show without Profile Pic</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(show_with_profile_pic_html);
        $('.show_with_profile_pic.checkbox').checkbox({
            onChecked: function () {
                $('.show_without_profile_pic.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.show_without_profile_pic.checkbox').checkbox({
            onChecked: function () {
                $('.show_with_profile_pic.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }


    var verified_accounts = "disabled";
    if (inArray('verified_accounts', table_filters)) {
        verified_accounts = "";
    }
    if ((hide_others && inArray('verified_accounts', table_filters)) || (!hide_others)) {
        var show_verified_accounts_html = '<div class="col-6 col-sm-6 col-md-3 col-lg-3 my-2">' +
            '  <div class="ui ' + verified_accounts + ' checkbox show_verified_accounts">\n' +
            '    <input type="checkbox" name="show_verified_accounts">\n' +
            '    <label>Show Verified Accounts</label>\n' +
            '  </div>' +
            '  <div class="ui fitted hidden divider"></div>' +
            '  <div class="ui ' + verified_accounts + ' checkbox skip_verified_accounts">\n' +
            '    <input type="checkbox" name="skip_verified_accounts">\n' +
            '    <label>Skip Verified Accounts</label>\n' +
            '  </div>' +
            '</div>';
        filter_actions_ele.append(show_verified_accounts_html);
        $('.show_verified_accounts.checkbox').checkbox({
            onChecked: function () {
                $('.skip_verified_accounts.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
        $('.skip_verified_accounts.checkbox').checkbox({
            onChecked: function () {
                $('.show_verified_accounts.checkbox').checkbox("set unchecked");
            },
            onChange: function () {
                table.ajax.reload();
            }
        });
    }

}

function addFilterParams(d) {
    var my_followers = $('.my_followers.checkbox');
    if (my_followers.length) {
        d.my_followers = my_followers.checkbox("is checked");
    } else {
        d.my_followers = false;
    }
    var not_my_followers = $('.not_my_followers.checkbox');
    if (not_my_followers.length) {
        d.not_my_followers = not_my_followers.checkbox("is checked");
    } else {
        d.not_my_followers = false;
    }

    var muted = $('.muted.checkbox');
    if (muted.length) {
        d.muted = muted.checkbox("is checked");
    } else {
        d.muted = false;
    }
    var not_muted = $('.not_muted.checkbox');
    if (not_muted.length) {
        d.not_muted = not_muted.checkbox("is checked");
    } else {
        d.not_muted = false;
    }

    var whitelisted = $('.whitelisted.checkbox');
    if (whitelisted.length) {
        d.whitelisted = whitelisted.checkbox("is checked");
    } else {
        d.whitelisted = false;
    }
    var blacklisted = $('.blacklisted.checkbox');
    if (blacklisted.length) {
        d.blacklisted = blacklisted.checkbox("is checked");
    } else {
        d.blacklisted = false;
    }

    var skip_whitelisted = $('.skip_whitelisted.checkbox');
    if (skip_whitelisted.length) {
        d.skip_whitelisted = skip_whitelisted.checkbox("is checked");
    } else {
        d.skip_whitelisted = false;
    }
    var skip_blacklisted = $('.skip_blacklisted.checkbox');
    if (skip_blacklisted.length) {
        d.skip_blacklisted = skip_blacklisted.checkbox("is checked");
    } else {
        d.skip_blacklisted = false;
    }

    var show_with_profile_pic = $('.show_with_profile_pic.checkbox');
    if (show_with_profile_pic.length) {
        d.show_with_profile_pic = show_with_profile_pic.checkbox("is checked");
    } else {
        d.show_with_profile_pic = false;
    }
    var show_without_profile_pic = $('.show_without_profile_pic.checkbox');
    if (show_without_profile_pic.length) {
        d.show_without_profile_pic = show_without_profile_pic.checkbox("is checked");
    } else {
        d.show_without_profile_pic = false;
    }

    var show_verified_accounts = $('.show_verified_accounts.checkbox');
    if (show_verified_accounts.length) {
        d.show_verified_accounts = show_verified_accounts.checkbox("is checked");
    } else {
        d.show_verified_accounts = false;
    }
    var skip_verified_accounts = $('.skip_verified_accounts.checkbox');
    if (skip_verified_accounts.length) {
        d.skip_verified_accounts = skip_verified_accounts.checkbox("is checked");
    } else {
        d.skip_verified_accounts = false;
    }

    var iam_following = $('.iam_following.checkbox');
    if (iam_following.length) {
        d.iam_following = iam_following.checkbox("is checked");
    } else {
        d.iam_following = false;
    }
    var iam_not_following = $('.iam_not_following.checkbox');
    if (iam_not_following.length) {
        d.iam_not_following = iam_not_following.checkbox("is checked");
    } else {
        d.iam_not_following = false;
    }


    return d;
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}