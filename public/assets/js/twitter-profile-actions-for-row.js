/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 2/23/18, 1:24 AM
 */

var create_whitelist_identifier = "whitelist_account";
var create_blacklist_identifier = "blacklist_account";
var destroy_whitelist_identifier = "destroy_whitelist_account";
var destroy_blacklist_identifier = "destroy_blacklist_account";
var create_mute_identifier = "mute_account";
var create_unmute_identifier = "unmute_account";
var create_follow_identifier = "follow_account";
var create_unfollow_identifier = "unfollow_account";
var action_url_identifier = "sm-action-url";
var tw_user_id_identifier = "sm-tw-user-id";
var bulk_select_identifier = "select_all";
var row_select_identifier = "select_row";
var bulk_actions_button_identifier = "bulk_actions_button";
var follow_bulk_action_identifier = "follow_bulk_action";
var unfollow_bulk_action_identifier = "unfollow_bulk_action";
var mute_bulk_action_identifier = "mute_bulk_action";
var unmute_bulk_action_identifier = "unmute_bulk_action";
var whitelist_bulk_action_identifier = "whitelist_bulk_action";
var blacklist_bulk_action_identifier = "blacklist_bulk_action";
var remove_whitelist_bulk_action_identifier = "remove_whitelist_bulk_action";
var remove_blacklist_bulk_action_identifier = "remove_blacklist_bulk_action";

var follow_action_name = "follow";
var unfollow_action_name = "unfollow";
var mute_action_name = "mute";
var unmute_action_name = "unmute";
var whitelist_action_name = "create_whitelist";
var blacklist_action_name = "create_blacklist";
var remove_whitelist_action_name = "remove_whitelist";
var remove_blacklist_action_name = "remove_blacklist";

var noOfCurrentRequests = 0;


function bindTableEvents() {
    $('.' + create_follow_identifier + ', .' + create_unfollow_identifier + ', .' + create_mute_identifier + ', .' + create_unmute_identifier +
        ', .' + create_whitelist_identifier + ', .' + create_blacklist_identifier + ', .' + destroy_whitelist_identifier +
        ', .' + destroy_blacklist_identifier).click(function () {
        processAction($(this));
    });
}

function processAction(element) {
    preAction(element);
    var url = element.attr(action_url_identifier);
    var data = {
        'tw_user_id': element.attr(tw_user_id_identifier)
    };
    var action_name = getActionName(element);
    $.ajaxq(action_name, {
        url: url,
        data: data
    }).done(function (element, result) {
            if (result['success']) {
                onSuccessAction(result['message'], element);
            } else {
                if (result['rate_limit_status']) {
                    var action_name = getActionName(element);
                    $.ajaxq.clear(action_name);
                    new Noty({
                        type: "error",
                        text: "Rate limit is being applied on " + action_name + " by twitter. Please wait for few minutes.",
                        timeout: 10000
                    }).show();
                    var action_identifier = getActionIdentifier(element);
                    $('.' + action_identifier).removeClass("loading");

                } else if (!result['silent']) {
                    onErrorAction(result['error']);
                }

            }
        }.bind(this, element)
    ).fail(function () {
        onErrorAction("An error occurred, please try again later.");
    }).always(function (element) {
            postAction(element);
        }.bind(this, element)
    );
}


function preAction(element) {
    element.addClass('loading');
    noOfCurrentRequests++;
}

function onSuccessAction(msg, element) {
    // Hide success message as it is of no use instead utilizes more browser memory
    // new Noty({
    //     type: "success",
    //     text: msg,
    // }).show();
    manageUI(element);
}

function onErrorAction(msg) {
    if (msg) {
        new Noty({
            type: "error",
            text: msg,
        }).show();
    }
}

function postAction(element) {
    element.removeClass('loading');
    // element.addClass('animated flash');
    // setTimeout(function () {
    //     element.removeClass('animated flash');
    // }, 2000);
    noOfCurrentRequests--;
}

function getActionName(element) {
    if (element.hasClass(create_whitelist_identifier)) { // create_whitelist
        return whitelist_action_name;
    } else if (element.hasClass(destroy_whitelist_identifier)) { // destroy_whitelist
        return remove_whitelist_action_name;
    } else if (element.hasClass(create_blacklist_identifier)) { // create_blacklist
        return blacklist_action_name;
    } else if (element.hasClass(destroy_blacklist_identifier)) { // destroy_blacklist
        return remove_blacklist_action_name;
    } else if (element.hasClass(create_follow_identifier)) { // follow
        return follow_action_name;
    } else if (element.hasClass(create_unfollow_identifier)) { // unfollow
        return unfollow_action_name;
    } else if (element.hasClass(create_mute_identifier)) { // mute
        return mute_action_name;
    } else if (element.hasClass(create_unmute_identifier)) { // unmute
        return unmute_action_name;
    }

}

function getActionIdentifier(element) {
    if (element.hasClass(create_whitelist_identifier)) { // create_whitelist
        return create_whitelist_identifier;
    } else if (element.hasClass(destroy_whitelist_identifier)) { // destroy_whitelist
        return destroy_whitelist_identifier;
    } else if (element.hasClass(create_blacklist_identifier)) { // create_blacklist
        return create_blacklist_identifier;
    } else if (element.hasClass(destroy_blacklist_identifier)) { // destroy_blacklist
        return destroy_blacklist_identifier;
    } else if (element.hasClass(create_follow_identifier)) { // follow
        return create_follow_identifier;
    } else if (element.hasClass(create_unfollow_identifier)) { // unfollow
        return create_unfollow_identifier;
    } else if (element.hasClass(create_mute_identifier)) { // mute
        return create_mute_identifier;
    } else if (element.hasClass(create_unmute_identifier)) { // unmute
        return create_unmute_identifier;
    }

}


/* Manage UI below */

function manageUI(element) {
    if (element.hasClass(create_whitelist_identifier)) { // create_whitelist
        manageStyleOnCreateWhitelist(element);
    } else if (element.hasClass(destroy_whitelist_identifier)) { // destroy_whitelist
        manageStyleOnDestroyWhitelist(element);
    } else if (element.hasClass(create_blacklist_identifier)) { // create_blacklist
        manageStyleOnCreateBlacklist(element);
    } else if (element.hasClass(destroy_blacklist_identifier)) { // destroy_blacklist
        manageStyleOnDestroyBlacklist(element);
    } else if (element.hasClass(create_follow_identifier)) { // follow
        manageStyleOnFollow(element);
    } else if (element.hasClass(create_unfollow_identifier)) { // unfollow
        manageStyleOnUnfollow(element);
    } else if (element.hasClass(create_mute_identifier)) { // mute
        manageStyleOnMute(element);
    } else if (element.hasClass(create_unmute_identifier)) { // unmute
        manageStyleOnUnmute(element);
    }

}

function manageStyleOnCreateWhitelist(element) {
    element.removeClass(create_whitelist_identifier);
    element.addClass(destroy_whitelist_identifier);
    element.removeClass("black");
    element.addClass("green");
    element.attr(action_url_identifier, destroy_whitelist_url);

    var account_id = element.attr(tw_user_id_identifier);
    var inverse_element = $('#' + create_blacklist_identifier + '_' + account_id);
    inverse_element.removeClass(destroy_blacklist_identifier);
    inverse_element.addClass(create_blacklist_identifier);
    inverse_element.removeClass("red");
    inverse_element.addClass("black");
    inverse_element.attr(action_url_identifier, create_blacklist_url);
}

function manageStyleOnDestroyWhitelist(element) {
    element.removeClass(destroy_whitelist_identifier);
    element.addClass(create_whitelist_identifier);
    element.removeClass("green");
    element.addClass("black");
    element.attr(action_url_identifier, create_whitelist_url);
}

function manageStyleOnCreateBlacklist(element) {
    element.removeClass(create_blacklist_identifier);
    element.addClass(destroy_blacklist_identifier);
    element.removeClass("black");
    element.addClass("red");
    element.attr(action_url_identifier, destroy_blacklist_url);

    var account_id = element.attr(tw_user_id_identifier);
    var inverse_element = $('#' + create_whitelist_identifier + '_' + account_id);
    inverse_element.removeClass(destroy_whitelist_identifier);
    inverse_element.addClass(create_whitelist_identifier);
    inverse_element.removeClass("green");
    inverse_element.addClass("black");
    inverse_element.attr(action_url_identifier, create_whitelist_url);
}

function manageStyleOnDestroyBlacklist(element) {
    element.removeClass(destroy_blacklist_identifier);
    element.addClass(create_blacklist_identifier);
    element.removeClass("red");
    element.addClass("black");
    element.attr(action_url_identifier, create_blacklist_url);
}

function manageStyleOnFollow(element) {
    element.addClass('disabled');
    var account_id = element.attr(tw_user_id_identifier);
    var inverse_element = $('#' + create_unfollow_identifier + '_' + account_id);
    inverse_element.removeClass('disabled');
}

function manageStyleOnUnfollow(element) {
    element.addClass('disabled');
    var account_id = element.attr(tw_user_id_identifier);
    var inverse_element = $('#' + create_follow_identifier + '_' + account_id);
    inverse_element.removeClass('disabled');
}

function manageStyleOnMute(element) {
    element.addClass('disabled');
    var account_id = element.attr(tw_user_id_identifier);
    var inverse_element = $('#' + create_unmute_identifier + '_' + account_id);
    inverse_element.removeClass('disabled');
}

function manageStyleOnUnmute(element) {
    element.addClass('disabled');
    var account_id = element.attr(tw_user_id_identifier);
    var inverse_element = $('#' + create_mute_identifier + '_' + account_id);
    inverse_element.removeClass('disabled');
}

function createBulkActionsButton() {
    var bulk_action_html = '<button class="ui primary button bulk_actions_button">Bulk Actions</button>';
    $("div.bulk_actions").html(bulk_action_html);
    $('.' + bulk_actions_button_identifier).click(function () {
        $('.bulk_actions_modal')
            .modal({
                onDeny: function () {
                    return true;
                },
                onApprove: function () {
                    bulkActionHandler();
                }
            }).modal('show');
    });
}

function checkSelectedRows() {
    var flagIsAllSelected = true;
    var flagIsAnySelected = false;
    $('.' + row_select_identifier).each(function () {
        if (!$(this).checkbox('is checked')) {
            flagIsAllSelected = false;
        } else {
            flagIsAnySelected = true;
        }
    });
    if (flagIsAllSelected && flagIsAnySelected) {
        $('.' + bulk_select_identifier).checkbox('set checked');
    } else {
        $('.' + bulk_select_identifier).checkbox('set unchecked');
    }
    if (flagIsAnySelected) {
        $('.' + bulk_actions_button_identifier).removeClass('disabled');
    } else {
        $('.' + bulk_actions_button_identifier).addClass('disabled');
    }
}

function bindBulkActionSelector() {
    checkSelectedRows();
    $('.' + row_select_identifier).checkbox({
        onChecked: function () {
            var user_id = $(this).val();
            $('#' + user_id + '_row').addClass('selected');
        },
        onUnchecked: function () {
            var user_id = $(this).val();
            $('#' + user_id + '_row').removeClass('selected');
        },
        onChange: function () {
            checkSelectedRows();
        }
    });
}

function bindBulkActions() {
    // Bind select all table header checkbox
    $('.' + bulk_select_identifier).checkbox({
        onChecked: function () {
            $('.' + row_select_identifier).checkbox('check');
        },
        onUnchecked: function () {
            $('.' + row_select_identifier).checkbox('uncheck');
        },
    });

    // Bind bulk select actions (follow, mute, etc)
    $('.' + follow_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + unfollow_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + unfollow_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + follow_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + mute_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + unmute_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + unmute_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + mute_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + whitelist_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + blacklist_bulk_action_identifier).checkbox('uncheck');
            $('.' + remove_whitelist_bulk_action_identifier).checkbox('uncheck');
            $('.' + remove_blacklist_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + blacklist_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + whitelist_bulk_action_identifier).checkbox('uncheck');
            $('.' + remove_whitelist_bulk_action_identifier).checkbox('uncheck');
            $('.' + remove_blacklist_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + remove_whitelist_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + whitelist_bulk_action_identifier).checkbox('uncheck');
            $('.' + blacklist_bulk_action_identifier).checkbox('uncheck');
        }
    });
    $('.' + remove_blacklist_bulk_action_identifier).checkbox({
        onChecked: function () {
            $('.' + whitelist_bulk_action_identifier).checkbox('uncheck');
            $('.' + blacklist_bulk_action_identifier).checkbox('uncheck');
        }
    });
}

function bulkActionHandler() {
    var selectedRows = [];
    $('.' + row_select_identifier).each(function () {
        if ($(this).checkbox('is checked')) {
            selectedRows.push($(this).find('input').val());
        }
    });
    var element = null;
    for (var i = 0; i < selectedRows.length; i++) {
        var user_id = selectedRows[i];
        if ($('.' + follow_bulk_action_identifier).checkbox('is checked')) {
            element = $('#' + create_follow_identifier + '_' + user_id);
            if (!element.hasClass('disabled')) {
                element.click();
            }
        }
        if ($('.' + unfollow_bulk_action_identifier).checkbox('is checked')) {
            element = $('#' + create_unfollow_identifier + '_' + user_id);
            if (!element.hasClass('disabled')) {
                element.click();
            }
        }
        if ($('.' + mute_bulk_action_identifier).checkbox('is checked')) {
            element = $('#' + create_mute_identifier + '_' + user_id);
            if (!element.hasClass('disabled')) {
                element.click();
            }
        }
        if ($('.' + unmute_bulk_action_identifier).checkbox('is checked')) {
            element = $('#' + create_unmute_identifier + '_' + user_id);
            if (!element.hasClass('disabled')) {
                element.click();
            }
        }
        if ($('.' + whitelist_bulk_action_identifier).checkbox('is checked')) {
            $('#' + create_whitelist_identifier + '_' + user_id + '.' + create_whitelist_identifier).click();
        } else if ($('.' + blacklist_bulk_action_identifier).checkbox('is checked')) {
            $('#' + create_blacklist_identifier + '_' + user_id + '.' + create_blacklist_identifier).click();
        } else {
            if ($('.' + remove_whitelist_bulk_action_identifier).checkbox('is checked')) {
                $('#' + create_whitelist_identifier + '_' + user_id + '.' + destroy_whitelist_identifier).click();
            }
            if ($('.' + remove_blacklist_bulk_action_identifier).checkbox('is checked')) {
                $('#' + create_blacklist_identifier + '_' + user_id + '.' + destroy_blacklist_identifier).click();
            }
        }
    }
}

$(document).ready(function () {
    $.extend(true, $.fn.dataTable.defaults, {
        language: {
            "search": "_INPUT_",
            "searchPlaceholder": "Search Username, Name or Location",
            "lengthMenu": "_MENU_ users"
        },
        "responsive": false,
        "processing": true,
        "serverSide": true,
        "info": true,
        "stateSave": true,
        "scrollX": true,
        "pagingType": 'full_numbers',
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "initComplete": function (settings, json) {
            createBulkActionsButton();
            bindBulkActions();
            $("div.filters").hide();
        },
        createdRow: function (row, data, dataIndex) {
            if (data['is_whitelisted']) {
                $(row).addClass('rowWhiteListBorder');
            } else if (data['is_blacklisted']) {
                $(row).addClass('rowBlackListBorder');
            }
        },
        drawCallback: function () {
            bindTableEvents();
        }
    });
    $('#users_table').on('processing.dt', function (e, settings, processing) {
        if (processing) {
            $('body').LoadingOverlay('show');
        } else {
            $('body').LoadingOverlay('hide', true);
            $('.ui.dropdown')
                .dropdown()
            ;
            $('.' + bulk_select_identifier).checkbox('uncheck');
            bindBulkActionSelector();
        }
    });

});

window.onbeforeunload = function (e) {
    if ($.ajaxq.isRunning()) {

        var message = noOfCurrentRequests + " operations are pending. Are you sure you want to close?",
            e = e || window.event;
        // For IE and Firefox
        if (e) {
            e.returnValue = message;
        }

        // For Safari
        return message;
    }
};
