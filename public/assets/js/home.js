// Navigation

var mt_nav = document.getElementById("mt_nav");
var mt_nav2 = document.getElementById("mt_nav2");

window.addEventListener("scroll", navScroll);

function navScroll() {

    var yPos = window.pageYOffset;

    if (yPos > 120) {
        mt_nav.style = "background:linear-gradient(45deg, #58caff,#7f8cff ) !important;box-shadow: 0px 5px 10px rgba(96, 101, 150, 0.3);" +
            "transition:all .3s;transition-delay:.2s;";
        mt_nav2.style = "background:linear-gradient(45deg, #58caff, #7f8cff) !important;box-shadow: 0px 5px 10px rgba(96, 101, 150, 0.3);" +
            "transition:all .3s;transition-delay:.2s;";
    }
    else {
        mt_nav.style = "background:transparent;transition:all .3s;transition-delay:;";
        mt_nav2.style = "background:transparent;transition:all .3s;transition-delay:;";
    }

}

