/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Project: this-town
 * Created at: 2/10/18, 1:38 AM
 */

jconfirm.defaults = {
    title: 'Hello',
    titleClass: '',
    type: 'purple',
    typeAnimated: true,
    draggable: false,
    dragWindowGap: 15,
    dragWindowBorder: true,
    animateFromElement: true,
    smoothContent: true,
    content: 'Are you sure to continue?',
    buttons: {},
    defaultButtons: {
        ok: {
            action: function () {
            }
        },
        close: {
            action: function () {
            }
        },
    },
    contentLoaded: function (data, status, xhr) {
    },
    icon: '',
    lazyOpen: false,
    bgOpacity: null,
    theme: 'supervan',
    animation: 'scale',
    closeAnimation: 'scale',
    animationSpeed: 400,
    animationBounce: 1,
    rtl: false,
    container: 'body',
    containerFluid: true,
    backgroundDismiss: false,
    backgroundDismissAnimation: 'shake',
    autoClose: false,
    closeIcon: false,
    closeIconClass: 'fa fa-close',
    watchInterval: 100,
    columnClass: ' col-10 col-sm-8 col-md-6 col-lg-6 mx-auto',
    boxWidth: '50%',
    scrollToPreviousElement: true,
    scrollToPreviousElementAnimate: true,
    useBootstrap: false,
    offsetTop: 40,
    offsetBottom: 40,
    bootstrapClasses: {
        container: 'container',
        containerFluid: 'container-fluid',
        row: 'row',
    },
    onContentReady: function () {
    },
    onOpenBefore: function () {
    },
    onOpen: function () {
    },
    onClose: function () {
    },
    onDestroy: function () {
    },
    onAction: function () {
    }
};