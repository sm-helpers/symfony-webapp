SMBooster
=========

<h4>Project setup:</h4>
<ul>
    <li>Create a copy of .env.dist file without .dist extension. 
    Configure it according to your machine. 
    E.g database configurations, mail configuratiuons, 
    and create a key for APP_SECRET</li>
    <li>Create a copy of *.yaml.dist files without 
        .dist extension. Add related info in them.
        <ul>
            <li><strong>public/.htaccess </strong></li>
            <li><strong>config/private/api_keys.yaml : </strong>A private file, add related keys and tokens into it.</li>
            <li><strong>config/app_configurations.yaml : </strong>Set application configurations.</li>
            <li><strong>config/packages/swiftmailer.yaml : </strong>Set swiftmailer configrations etc. set delivery_address to route all emails to single email address for dev/test environment.</li>
        </ul>
     </li>
    <li>Install libraries via composer by executing following command in project's root dir
    <pre><code>composer install</code></pre></li>
    <li>Run two commands in project's root dir
    to create database and its schema.
        <pre><code>php bin/console doctrine:database:create</code></pre>
        <pre><code>php bin/console doctrine:schema:update --force</code></pre>
    </li>
    <li>
        Create a record in platform table using query
        <pre><code>INSERT INTO `platforms` (`id`, `name`, `is_active`) VALUES (1, 'twitter', 1);</code></pre>
    </li>
    <li>
        You are good to go! Now configure commands below in crontab.
    </li>
</ul>
<h4>Commands</h4>
<ul>
    <li>Create tasks (fetch followers, friends, mutes, unfollowers) for authenticated users
        <pre><code>php bin/console app:tasks:create</code></pre>
        <p>This command will be run as cron job using following.</p>
        <pre><code>0 0 * * * php /path/to/application/bin/console app:tasks:create >> /path/to/application/var/log/task_create/cron_errors.log</code></pre>
        <p>To rotate cron error file, follow <a href="https://serverfault.com/a/703788">https://serverfault.com/a/703788</a></p>
    </li>
    <li>Update twitter accounts
        <pre><code>php bin/console app:twaccounts:update</code></pre>
        <p>This command will be run as cron job using following.</p>
        <pre><code>5 * * * * flock -n /path/to/application/var/log/tw_accounts_update/flock.lock php /path/to/application/bin/console app:twaccounts:update >> /path/to/application/var/log/tw_accounts_update/cron_errors.log</code></pre>
        <p>flock will prevent duplicate cron job.</p>
        <p>To rotate cron error file, follow <a href="https://serverfault.com/a/703788">https://serverfault.com/a/703788</a></p>
    </li>
    <li>Run all tasks
        <pre><code>php bin/console app:tasks:run</code></pre>
        <p>This command will be run as cron job using following.</p>
        <pre><code>5 * * * * php /path/to/application/bin/console app:tasks:run >> /path/to/application/var/log/tasks/cron_errors.log</code></pre>
        <p>To rotate cron error file, follow <a href="https://serverfault.com/a/703788">https://serverfault.com/a/703788</a></p>
    </li>
    <li>Run single task using task ID
        <pre><code>php bin/console app:task:run id</code></pre>
        <p>This command will be called automatically, we never have to run this command manually except in local/debug env, in prod there are routines for this.</p>
    </li>
</ul>
