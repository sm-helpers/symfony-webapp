<?php

namespace App\Repository;

use App\Entity\TwitterUnfollowers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TwitterUnfollowersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TwitterUnfollowers::class);
    }

    public function countTotalUnfollowers($twUserId)
    {
        try {
            $data = $this->createQueryBuilder('twitterUnfollowers')
                ->select('count(twitterUnfollowers.id) as total')
                ->where('twitterUnfollowers.twUserId = :twUserId')
                ->setParameters(array(
                    'twUserId' => $twUserId
                ))
                ->getQuery()->setMaxResults(1)->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return false;
        }
        return $data['total'];

    }
}
