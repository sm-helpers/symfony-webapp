<?php

namespace App\Repository;

use App\Entity\AuthCreds;
use App\Entity\Tasks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TasksRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tasks::class);
    }

    /**
     * @param \DateTime $date
     * @param AuthCreds $authCred
     * @param string $taskName
     * @param array $params
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findExistingTasks(\DateTime $date, AuthCreds $authCred, string $taskName, array $params = array())
    {

        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare("SELECT * FROM tasks WHERE DATE(task_datetime) = :task_datetime AND auth_creds_id = :auth_creds_id AND name = :task_name AND parameters = :parameters");
        $statement->bindValue('task_datetime', $date->format('Y-m-d'));
        $statement->bindValue('auth_creds_id', $authCred->getId());
        $statement->bindValue('task_name', $taskName);
        $statement->bindValue('parameters', serialize($params));
        $statement->execute();
        return $statement->fetchAll();

//        return $taskQB->select('tasks')
//            ->where('tasks.taskDatetime BETWEEN :from AND :to')
//            ->andWhere('tasks.authCreds = :authCreds')
//            ->andWhere('tasks.name = :taskName')
//            ->andWhere('tasks.parameters = :params')
//            ->setParameters(array(
//                'from' => $from,
//                'to' => $to,
//                'authCreds' => $authCred,
//                'taskName' => $taskName,
//                'params' => serialize($params)
//            ))
//            ->getQuery()
//            ->getArrayResult();
    }
}
