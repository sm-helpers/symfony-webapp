<?php
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: TasksManager.php
 * Date: 12/16/17
 * Time: 12:51 AM
 */


namespace App\Utils;


use App\Entity\AuthCreds;
use App\Entity\Tasks;
use App\Routines\TwitterAccountsRoutines;
use Cocur\BackgroundProcess\BackgroundProcess;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Validator\Constraints\DateTime;

class TasksManager
{
    /*
     * CONST variables start
     */
    const STATUS_PENDING = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_PARTIAL = 3;
    const STATUS_COMPLETE = 4;
    const STATUS_CANCELED = 5;
    const STATUS_PENDING_NAME = "PENDING";
    const STATUS_PROCESSING_NAME = "PROCESSING";
    const STATUS_PARTIAL_NAME = "PARTIAL";
    const STATUS_COMPLETE_NAME = "COMPLETE";
    const STATUS_CANCELED_NAME = "CANCELED";
    const STATUS_PENDING_COLOR = "#CDD322";
    const STATUS_PROCESSING_COLOR = "#22D36E";
    const STATUS_PARTIAL_COLOR = "#D36622";
    const STATUS_COMPLETE_COLOR = "#2FD322";
    const STATUS_CANCELED_COLOR = "#D32223";

    /*
     * Class Data Members
     */

    private $entityManager;
    private $twitterAccountsRoutines;
    private $appAbsolutePath;

    /**
     * TasksManager constructor.
     * @param EntityManager $entityManager
     * @param TwitterAccountsRoutines $twitterAccountsRoutines
     * @param $appAbsolutePath
     */
    public function __construct(EntityManager $entityManager, TwitterAccountsRoutines $twitterAccountsRoutines, $appAbsolutePath)
    {
        $this->entityManager = $entityManager;
        $this->twitterAccountsRoutines = $twitterAccountsRoutines;
        $this->appAbsolutePath = $appAbsolutePath;
    }

    /**
     * @param $status
     * @return mixed
     * @throws \Exception
     */
    public function resolveTaskStatus($status)
    {
        switch ($status) {
            case TasksManager::STATUS_PENDING:
                $resolvedTask = array(
                    "name" => TasksManager::STATUS_PENDING_NAME,
                    "color" => TasksManager::STATUS_PENDING_COLOR,
                );
                break;
            case TasksManager::STATUS_PROCESSING:
                $resolvedTask = array(
                    "name" => TasksManager::STATUS_PROCESSING_NAME,
                    "color" => TasksManager::STATUS_PROCESSING_COLOR,
                );
                break;
            case TasksManager::STATUS_PARTIAL:
                $resolvedTask = array(
                    "name" => TasksManager::STATUS_PARTIAL_NAME,
                    "color" => TasksManager::STATUS_PARTIAL_COLOR,
                );
                break;
            case TasksManager::STATUS_COMPLETE:
                $resolvedTask = array(
                    "name" => TasksManager::STATUS_COMPLETE_NAME,
                    "color" => TasksManager::STATUS_COMPLETE_COLOR,
                );
                break;
            case TasksManager::STATUS_CANCELED:
                $resolvedTask = array(
                    "name" => TasksManager::STATUS_CANCELED_NAME,
                    "color" => TasksManager::STATUS_CANCELED_COLOR,
                );
                break;
            default:
                throw new \Exception("Invalid status to resolve.");
                break;
        }
        return $resolvedTask;
    }

    /**
     * @param string $name Name of the task
     * @param integer $auth_creds_id Related Auth Credentials ID
     * @param array $parameters Associate array for parameters
     * @param integer $priority Priority of task (1-10) 10 is for highest priority @todo Will be added later
     * @param DateTime $taskDatetime Set the date and time of when this task should start, if null, current DateTime will be set
     * @return Tasks
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createTask($name, $auth_creds_id, $parameters, $priority = 0, $taskDatetime = null)
    {
        if ($taskDatetime === null) {
            $taskDatetime = new \DateTime();
        }
        $currentDatetime = new \DateTime();

        $task = new Tasks();
        $task->setName($name);
        $task->setAuthCreds($this->entityManager->getRepository('App:AuthCreds')->find($auth_creds_id));
        $task->setParameters($parameters);
        $task->setPriority($priority);
        $task->setCreatedAt($currentDatetime);
        $task->setModifiedAt($currentDatetime);
        $task->setStatus(TasksManager::STATUS_PENDING);
        $task->setErrorCount("0");
        if ($taskDatetime) {
            $task->setTaskDatetime($taskDatetime);
        } else {
            $task->setTaskDatetime($currentDatetime);
        }
        $this->entityManager->persist($task);
        $this->entityManager->flush();
        return $task;
    }

    /**
     * @param integer $task_id
     * @return Tasks
     * @throws \Exception
     */
    public function getTaskById($task_id)
    {
        $task = null;
        if (intval($task_id) > 0) {
            /** @noinspection PhpUndefinedMethodInspection */
            $task = $this->entityManager->getRepository('App:Tasks')
                ->findOneById($task_id);
        } else {
            throw new \Exception("Task ID is invalid.");
        }
        return $task;
    }

    /**
     * Get next queued task object with status pending or partial. Be careful to change status of previous task if using iteratively.
     * @param string $taskName
     * @param AuthCreds|null $authCreds
     * @return Tasks
     * @throws \Exception
     */
    public function getNextTask($taskName = null, AuthCreds $authCreds = null)
    {
        $task_id = $this->entityManager->getRepository('App:Tasks')->createQueryBuilder('tasks')
            ->select('tasks.id')
            ->andWhere('tasks.status = :status1 OR tasks.status = :status2')
            ->setParameters(array(
                'status1' => TasksManager::STATUS_PENDING,
                'status2' => TasksManager::STATUS_PARTIAL
            ));
        if ($taskName) {
            $task_id = $task_id->andWhere('tasks.name = :task_name')
                ->setParameter('task_name', $taskName);
        }
        if ($authCreds) {
            $task_id = $task_id->andWhere('tasks.authCreds = :auth_creds')
                ->setParameter('auth_creds', $authCreds);
        }
        $currentDatetime = new \DateTime();
        $task_id = $task_id->andWhere('tasks.taskDatetime < :currentDatetime')
            ->setParameter('currentDatetime', $currentDatetime)
            ->addOrderBy('tasks.status', 'DESC')
            ->addOrderBy('tasks.taskDatetime', 'ASC')
            ->addOrderBy('tasks.id', 'ASC')
            ->getQuery();
        $task_id = $task_id->setMaxResults(1)
            ->getResult();

        if ($task_id) {
            $task = $this->getTaskById($task_id[0]['id']);
        } else {
            return null;
        }
        return $task;
    }

    /**
     * @param $status
     * @return boolean
     */
    public function verifyStatusParameter($status)
    {
        $is_valid_status = true;
        switch ($status) {
            case TasksManager::STATUS_PENDING:
            case TasksManager::STATUS_PROCESSING:
            case TasksManager::STATUS_PARTIAL:
            case TasksManager::STATUS_COMPLETE:
            case TasksManager::STATUS_CANCELED:
                break;
            default:
                $is_valid_status = false;
                break;
        }
        return $is_valid_status;
    }

    /**
     * @param $task_id
     * @param $status
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function changeStatus($task_id, $status)
    {
        if (!$this->verifyStatusParameter($status)) {
            throw new \Exception("Invalid status parameter for task");
        }
        if ($task = $this->getTaskById($task_id)) {
            $task->setStatus($status);
            $this->entityManager->persist($task);
            $this->entityManager->flush();
        } else {
            throw new \Exception("Invalid task ID.");
        }
    }

    /**
     * Call it when starting a task. It will set relevant status automatically.
     * @param $task_id
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function updateTaskOnStart($task_id)
    {
        if (!($task = $this->getTaskById($task_id))) {
            throw new \Exception("Invalid task ID.");
        }
        $currentDatetime = new \DateTime();
        if ($task->getStatus() == TasksManager::STATUS_PENDING) {
            if ($task->getErrorCount() == 0) {
                $task->setStartDatetime($currentDatetime);
            }
        } elseif ($task->getStatus() == TasksManager::STATUS_PARTIAL) {
            //
        } else {
            throw new \Exception("You can not start a task with status: " .
                ($this->resolveTaskStatus($task->getStatus()))['name']);
        }
        $task->setLastStartDatetime($currentDatetime);
        $task->setStatus(TasksManager::STATUS_PROCESSING);
        $task->setModifiedAt($currentDatetime);
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * Sets tasks status to user specified status and add relevant closing dates
     * @param $task_id
     * @param $new_status
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function updateTaskOnClosing($task_id, $new_status)
    {
        if (!$this->verifyStatusParameter($new_status)) {
            throw new \Exception("Invalid status parameter for task");
        }
        if (!($task = $this->getTaskById($task_id))) {
            throw new \Exception("Invalid task ID.");
        }
        $currentDatetime = new \DateTime();
        if ($task->getStatus() != TasksManager::STATUS_PROCESSING && $task->getStatus() != TasksManager::STATUS_CANCELED) {
            throw new \Exception("You can not close a task with status: " .
                ($this->resolveTaskStatus($task->getStatus()))['name']);
        }
        if ($task->getErrorCount() < 4 && $new_status == TasksManager::STATUS_CANCELED) {
            $task->setErrorCount($task->getErrorCount() + 1);
            $new_status = TasksManager::STATUS_PENDING;
        }
        if ($new_status == TasksManager::STATUS_COMPLETE || $new_status == TasksManager::STATUS_CANCELED) {
            $task->setCloseDatetime($currentDatetime);
        } elseif ($new_status == TasksManager::STATUS_PARTIAL || $new_status == TasksManager::STATUS_PENDING) {
            // Ignore
        } else {
            throw new \Exception("You can not assign status \"{$this->resolveTaskStatus($new_status)['name']}\" to a task after processing.");
        }
        $task->setLastCloseDatetime($currentDatetime);
        $task->setStatus($new_status);
        $task->setModifiedAt($currentDatetime);
        $this->entityManager->persist($task);
        $this->entityManager->flush();
    }

    /**
     * Run a task either synchronously or asynchronously. If called with $async=false, will return output other wise will save output to file.
     * @param $taskId
     * @param bool $async
     * @return string
     */
    public function runTask($taskId, $async = true)
    {
        $command = "php {$this->appAbsolutePath}/bin/console app:task:run {$taskId}";
        if ($async) {
            $process = new BackgroundProcess($command);
            $outputFile = "{$this->appAbsolutePath}/var/log/tasks/breakdown/tasks.log";
            $process->run($outputFile, true);
            // sleep for 1 second for delay to status to be updated otherwise same task will be called multiple times if used iteratively
            sleep(1);
        } else {
            $process = new Process($command);
            try {
                $process->mustRun();
                echo $process->getOutput();
                return $process->getOutput();
            } catch (ProcessFailedException $exception) {
                echo $exception->getMessage();
                return $exception->getMessage();
            }
        }
    }

}