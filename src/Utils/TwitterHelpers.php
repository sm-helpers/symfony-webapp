<?php
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: TwitterHelpers.php
 * Date: 11/16/17
 * Time: 2:29 AM
 */

namespace App\Utils;

use App\Entity\AuthCreds;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class TwitterHelpers
{
    /*
     * CONST variables start
     */
    const TWITTER_USERS_UPDATE_DELAY_HOURS = 168; // 7 Days
    // This information is being used to determine the memory and time usage for information
    const TWITTER_NO_OF_USERS_FOR_CUSTOM_INI = 50000; // Memory and time will be set according to N(this)
    const TWITTER_MEMORY_FOR_CUSTOM_INI = '700M';
    const TWITTER_TIME_FOR_CUSTOM_INI = 170;

    /*
     * CONST variables end
     */

    protected $consumerKey;
    protected $consumerSecret;
    protected $twIdsInApiCall;
    protected $entityManager;
    protected $twAccountsInApiCall;
    protected $maxAccountsToUpdate;
    protected $timeToUpdateAccount;
    protected $twMinLastUsedAuthCredTime;
    protected $remainingRateLimit;
    protected $twDeleteDisabledAccountsAfterRetries;
    protected $defaultTwitterProfilePictureUrl;


    /**
     * TwitterHelpers constructor.
     * @param string $consumerKey
     * @param string $consumerSecret
     * @param int $twIdsInApiCall
     * @param int $twAccountsInApiCall
     * @param int $maxAccountsToUpdate
     * @param int $timeToUpdateAccount
     * @param int $twMinLastUsedAuthCredTime
     * @param $twDeleteDisabledAccountsAfterRetries
     * @param $defaultTwitterProfilePictureUrl
     * @param EntityManager $entityManager
     */
    public function __construct($consumerKey, $consumerSecret, $twIdsInApiCall,
                                $twAccountsInApiCall, $maxAccountsToUpdate, $timeToUpdateAccount,
                                $twMinLastUsedAuthCredTime, $twDeleteDisabledAccountsAfterRetries,
                                $defaultTwitterProfilePictureUrl, EntityManager $entityManager)
    {
        $this->remainingRateLimit = -1;

        // Dependency Injections
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->twIdsInApiCall = $twIdsInApiCall;
        $this->entityManager = $entityManager;
        $this->twAccountsInApiCall = $twAccountsInApiCall;
        $this->maxAccountsToUpdate = $maxAccountsToUpdate;
        $this->timeToUpdateAccount = $timeToUpdateAccount;
        $this->twMinLastUsedAuthCredTime = $twMinLastUsedAuthCredTime;
        $this->twDeleteDisabledAccountsAfterRetries = $twDeleteDisabledAccountsAfterRetries;
        $this->defaultTwitterProfilePictureUrl = $defaultTwitterProfilePictureUrl;
    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @param bool $enforceAll
     * @param bool $waitOnRateLimit
     * @param int $maxCalls
     * @return array|bool
     * @throws DBALException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getFollowersIDs(AuthCreds $authCreds, $twUserId = -1, $enforceAll = true, $waitOnRateLimit = true, $maxCalls = -1)
    {
        if ($twUserId === -1) {
            $twUserId = $authCreds->getTwUserId();
        }
        $twitterAccount = $this->entityManager->getRepository('App:TwitterAccounts')
            ->find($twUserId);
        if (!$twitterAccount) {
            $process_response = $this->fetchAndCreateTwitterUsers($authCreds, $twUserId);
            if ($process_response['success']) {
                $twitterAccount = $this->entityManager->getRepository('App:TwitterAccounts')
                    ->find($twUserId);
            } else {
                return false;
            }
        }
        $apiAuth = $this->getApiAuth($authCreds);
        $cursor = -1;
        $followersIDs = array();
        $totalCalls = 0;
        while ($cursor != 0) {
            $result = $apiAuth->get('followers/ids', array("screen_name" => $twitterAccount->getTwUsername(),
                "cursor" => $cursor, "count" => $this->twIdsInApiCall
            ));
            if (!$result) {
                break;
            }
            if (isset($result->next_cursor)) {
                $cursor = $result->next_cursor;
                $followersIDs = array_merge($followersIDs, $result->ids);
            }
            $lastHeaders = $apiAuth->getLastXHeaders();
            if (!$this->sleep_or_break_on_rate_limit($lastHeaders, $waitOnRateLimit)) {
                break;
            }
            $totalCalls++;
            if ($totalCalls >= $maxCalls && $maxCalls > 0) {
                break;
            }

        }
        if ($cursor != 0 && $enforceAll == true) {
            return false;
        }
        $followersIDs = array_reverse($followersIDs);
        return $followersIDs;
    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @param bool $enforceAll
     * @param bool $waitOnRateLimit
     * @param int $maxCalls
     * @return array|bool
     * @throws DBALException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getFriendsIDs(AuthCreds $authCreds, $twUserId = -1, $enforceAll = true, $waitOnRateLimit = true, $maxCalls = -1)
    {
        if ($twUserId === -1) {
            $twUserId = $authCreds->getTwUserId();
        }
        $twitterAccount = $this->entityManager->getRepository('App:TwitterAccounts')
            ->find($twUserId);
        if (!$twitterAccount) {
            $process_response = $this->fetchAndCreateTwitterUsers($authCreds, $twUserId);
            if ($process_response['success']) {
                $twitterAccount = $this->entityManager->getRepository('App:TwitterAccounts')
                    ->find($twUserId);
            } else {
                return false;
            }
        }

        $apiAuth = $this->getApiAuth($authCreds);
        $cursor = -1;
        $friendsIDs = array();
        $totalCalls = 0;
        while ($cursor != 0) {
            $result = $apiAuth->get('friends/ids', array("screen_name" => $twitterAccount->getTwUsername(),
                "cursor" => $cursor, "count" => $this->twIdsInApiCall
            ));
            if (!$result) {
                break;
            }
            if (isset($result->next_cursor)) {
                $cursor = $result->next_cursor;
                $friendsIDs = array_merge($friendsIDs, $result->ids);
            }
            $lastHeaders = $apiAuth->getLastXHeaders();
            if (!$this->sleep_or_break_on_rate_limit($lastHeaders, $waitOnRateLimit)) {
                break;
            }
            $totalCalls++;
            if ($totalCalls >= $maxCalls && $maxCalls > 0) {
                break;
            }

        }
        if ($cursor != 0 && $enforceAll == true) {
            return false;
        }
        $friendsIDs = array_reverse($friendsIDs);
        return $friendsIDs;
    }

    /**
     * @param AuthCreds $authCreds
     * @param bool $enforceAll
     * @param bool $waitOnRateLimit
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getMutedIDs(AuthCreds $authCreds, $enforceAll = true, $waitOnRateLimit = true)
    {
        $apiAuth = $this->getApiAuth($authCreds);
        $cursor = -1;
        $muteIDs = array();
        while ($cursor != 0) {
            $result = $apiAuth->get('mutes/users/ids', array(
                "cursor" => $cursor, "count" => $this->twIdsInApiCall
            ));
            if (!$result) {
                break;
            }
            if (isset($result->next_cursor)) {
                $cursor = $result->next_cursor;
                $muteIDs = array_merge($muteIDs, $result->ids);
            }
            $lastHeaders = $apiAuth->getLastXHeaders();
            if (!$this->sleep_or_break_on_rate_limit($lastHeaders, $waitOnRateLimit)) {
                break;
            }

        }
        if ($cursor != 0 && $enforceAll == true) {
            return false;
        }
        $muteIDs = array_reverse($muteIDs);
        return $muteIDs;
    }

    /**
     * @param AuthCreds $authCreds
     * @param string $requiredEndPoint
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function get_rate_limits(AuthCreds $authCreds, string $requiredEndPoint = null)
    {
        $apiAuth = $this->getApiAuth($authCreds);
        do {
            $result = $apiAuth->get('application/rate_limit_status');
            if ($result === false) {
                return false;
            }
            if (isset($result->errors)) {
                $lastHeaders = $apiAuth->getLastXHeaders();
                $this->sleep_or_break_on_rate_limit($lastHeaders);
            }
        } while (isset($result->errors));
        if ($requiredEndPoint && isset($result->resources)) {
            foreach ($result->resources as $endPoints) {
                foreach ($endPoints as $endPoint => $data) {
                    if ($endPoint == $requiredEndPoint || $endPoint == "/" . $requiredEndPoint) {
                        return $data;
                    }
                }
            }
        }
        return $result->resources;
    }

    /**
     * @param array $headers
     * @param bool $waitOnRateLimit
     * @return bool
     */
    public function sleep_or_break_on_rate_limit(array $headers, bool $waitOnRateLimit = true)
    {
        // Sometimes Twitter response headers does not contains limit
        // Check if limit is available in headers
        // Otherwise maintain limit on our side
        if (isset($headers["x_rate_limit_remaining"])) {
            $this->remainingRateLimit = intval($headers["x_rate_limit_remaining"]);
        } else {
            $this->remainingRateLimit -= 1;
        }
        // If the limit is not received and our maintained limit is not set yet
        if ($this->remainingRateLimit < 0) {
            return false;
        }
        // Lets sleep or break or continue process
        if ($this->remainingRateLimit < 1) {
            if (!$waitOnRateLimit) {
                return false;
            }
            $limit_reset_time = $headers['x_rate_limit_reset'];
            $cur_time = \time();
            $needed_delay = (($limit_reset_time - $cur_time) < 0) ? 0 : $limit_reset_time - $cur_time + 5;
            sleep($needed_delay);
        }
        return true;
    }

    /**
     * @param AuthCreds $authCreds
     * @param $twUserIds
     * @param null $twUsernames
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws DBALException
     */
    public function fetchAndCreateTwitterUsers(AuthCreds $authCreds, $twUserIds = null, $twUsernames = null)
    {
        $updatedAccounts = 0;
        $twitterAccounts = false;
        $apiAuth = $this->getApiAuth($authCreds);
        $conn = $this->entityManager->getConnection();
        $response = array(
            'success' => true,
            'count' => 0,
            'error' => "",
            'last_headers' => null
        );
        if ($twUserIds) {
            if (!is_array($twUserIds)) {
                $twUserIds = array($twUserIds);
            }
            $twitterAccounts = $apiAuth->get('users/lookup', array("user_id" => \implode(',', $twUserIds),
                "include_entities" => 'false'
            ));
            $disabled_accounts = $twUserIds;
        } else if ($twUsernames) {
            if (!is_array($twUsernames)) {
                $twUsernames = array($twUsernames);
            }
            $twitterAccounts = $apiAuth->get('users/lookup', array("screen_name" => \implode(',', $twUsernames),
                "include_entities" => 'false'
            ));
            $disabled_accounts = array();
        }

        if ($twitterAccounts === false) {
            $response['success'] = false;
            $response['error'] = "Unable to fetch data.";
            return $response;
        }
        $lastHeadersAccounts = $apiAuth->getLastXHeaders();
        foreach ($twitterAccounts as $twitterAccount) {
            $queryData = array();
            if (!isset($twitterAccount->id)) {
                continue;
            }
            unset($disabled_accounts[array_search($twitterAccount->id, $disabled_accounts)]);
            $updatedAccounts++;

            // create/update twitter account record
            if ($this->saveTwitterUser($twitterAccount, true) == 0) {
                continue;
            }
        }
        foreach ($disabled_accounts as $disabled_account) {
            $statement = $conn->prepare("SELECT update_retries FROM twitter_accounts WHERE tw_user_id = :tw_user_id");
            $statement->bindValue('tw_user_id', $disabled_account);
            $statement->execute();
            $updateRetries = $statement->fetchAll();
            $updateRetries = $updateRetries[0]['update_retries'] + 1;
            $conn->update('twitter_accounts', array(
                'is_disabled' => 1,
                'update_retries' => $updateRetries,
                'modified_at' => date("Y-m-d H:i:s"),
            ), array(
                'tw_user_id' => $disabled_account
            ));
        }
        $response['success'] = true;
        $response['count'] = $updatedAccounts;
        $response['last_headers'] = $lastHeadersAccounts;
        return $response;

    }

    /**
     * @param array|\ArrayObject $twitterAccount
     * @param bool $updateOnFail
     * @return int
     */
    public function saveTwitterUser($twitterAccount, bool $updateOnFail = true)
    {

        if (!isset($twitterAccount->id)) {
            return 0;
        }
        $conn = $this->entityManager->getConnection();

        $queryData["tw_user_id"] = $twitterAccount->id;

        if (isset($twitterAccount->screen_name)) {
            $queryData["tw_username"] = $twitterAccount->screen_name;
        } else {
            $queryData["tw_username"] = "";
        }
        if (isset($twitterAccount->name)) {
            $queryData["tw_name"] = $twitterAccount->name;
        } else {
            $queryData["tw_name"] = "";
        }
        if (isset($twitterAccount->location)) {
            $queryData["tw_location"] = $twitterAccount->location;
        } else {
            $queryData["tw_location"] = "";
        }
        if (isset($twitterAccount->created_at)) {
            $queryData["tw_created_at"] = date("Y-m-d H:i:s", strtotime($twitterAccount->created_at));
        } else if ($updateOnFail) {
            $queryData["tw_created_at"] = date("Y-m-d H:i:s");
        }
        if (isset($twitterAccount->lang)) {
            $queryData["tw_lang"] = $twitterAccount->lang;
        } else {
            $queryData["tw_lang"] = "";
        }
        if (isset($twitterAccount->protected)) {
            $queryData["tw_protected"] = ($twitterAccount->protected) ? 1 : 0;
        } else {
            $queryData["tw_protected"] = 0;
        }
        if (isset($twitterAccount->time_zone)) {
            $queryData["tw_timezone"] = $twitterAccount->time_zone;
        } else {
            $queryData["tw_timezone"] = "";
        }
        if (isset($twitterAccount->followers_count)) {
            $queryData["tw_followers_count"] = $twitterAccount->followers_count;
        } else {
            $queryData["tw_followers_count"] = 0;
        }
        if (isset($twitterAccount->friends_count)) {
            $queryData["tw_friends_count"] = $twitterAccount->friends_count;
        } else {
            $queryData["tw_friends_count"] = 0;
        }
        if (isset($twitterAccount->statuses_count)) {
            $queryData["tw_statuses_count"] = $twitterAccount->statuses_count;
        } else {
            $queryData["tw_statuses_count"] = 0;
        }
        if (isset($twitterAccount->geo_enabled)) {
            $queryData["tw_geo_enabled"] = ($twitterAccount->geo_enabled) ? 1 : 0;
        } else {
            $queryData["tw_geo_enabled"] = 0;
        }
        if (isset($twitterAccount->profile_image_url_https)) {
            $queryData["tw_profile_image"] = $twitterAccount->profile_image_url_https;
        } else {
            $queryData["tw_profile_image"] = $this->defaultTwitterProfilePictureUrl;
        }
        if (isset($twitterAccount->verified)) {
            $queryData["tw_verified"] = ($twitterAccount->verified) ? 1 : 0;
        } else {
            $queryData["tw_verified"] = 0;
        }
        $queryData["created_at"] = date("Y-m-d H:i:s");
        $queryData["modified_at"] = date("Y-m-d H:i:s");
        $queryData["is_disabled"] = 0;
        $queryData["update_retries"] = 0;
        try {
            $conn->insert('twitter_accounts', $queryData);
        } catch (\Exception $constraintViolationException) {
            if ($constraintViolationException instanceof UniqueConstraintViolationException) {
                if ($constraintViolationException->getErrorCode() != 1062) {
                    return 0;
                    //throw $constraintViolationException;
                }
                if ($updateOnFail) {
                    unset($queryData['tw_user_id']);
                    unset($queryData['created_at']);
                    $conn->update('twitter_accounts', $queryData, array(
                        "tw_user_id" => $twitterAccount->id
                    ));
                }
            }
            // Ignore if duplicate key error as that can occur in case of multiple processes with
            // high number of accounts. Record will be ignored by MYSQL
        }

        return $twitterAccount->id;
    }


    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @param array $followersIDs
     * @param bool $enforceAll
     * @param bool $waitOnRateLimit
     * @return array|bool
     * @throws DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function executeFollowersTask($authCreds, $twUserId = -1, $followersIDs = array(), $enforceAll = true, $waitOnRateLimit = true, $deleteExisting = false)
    {
        if ($twUserId === -1) {
            $twUserId = $authCreds->getTwUserId();
        }
        $ignoreUnfollowers = false;
        GeneralHelpers::increase_php_limits();
        $newFollowersIDs = array();
        $unfollowersIDs = array();

        $lastHeadersFollowersIDs = null;
        $start_time = microtime(true);

        if ($authCreds->getEnabled()) {
            $conn = $this->entityManager->getConnection();
            $existingFollowersIDs = array();
            try {
                $existingTwitterAccounts = $this->entityManager->getRepository('App:TwitterFollowers')
                    ->createQueryBuilder('twitterFollowers')
                    ->select('twitterFollowers.twUserIdX')
                    ->andWhere('twitterFollowers.twUserId = :tw_user_id')
                    ->setParameters(array(
                        'tw_user_id' => $twUserId
                    ))
                    ->orderBy('twitterFollowers.twUserIdX', 'ASC')
                    ->getQuery()
                    ->getArrayResult();
                $existingFollowersIDs = GeneralHelpers::getArrayFromAssociateArray($existingTwitterAccounts, 'twUserIdX');
            } catch (\Exception $e) {
                $exceptionMsg = $e->getMessage();
                return array(
                    'success' => false,
                    'error' => "Database server error [{$exceptionMsg}]"
                );
            }
            if (count($followersIDs) == 0) {
                $followersIDs = $this->getFollowersIDs($authCreds, $twUserId, $enforceAll, $waitOnRateLimit);
            } else {
                $ignoreUnfollowers = true;
            }
            if (!is_array($followersIDs)) {
                return array(
                    'success' => false,
                    'error' => 'Unable to fetch complete followers'
                );
            }

            if ($deleteExisting) {
                $this->deleteAllFollowers($twUserId);
                $existingFollowersIDs = array();
            }
            $newFollowersIDs = array();
            foreach ($followersIDs as $followersID) {
                if (!GeneralHelpers::binary_search($followersID, $existingFollowersIDs)) {
                    $newFollowersIDs[] = $followersID;
                }
            }
            foreach ($newFollowersIDs as $newFollowersID) {
                $twitterAccount = new \ArrayObject();
                $twitterAccount->id = $newFollowersID;

                // create twitter account record
                if ($this->saveTwitterUser($twitterAccount, false) == 0) {
                    continue;
                }

                // set twitter account as follower of current authCreds
                $queryData = array(
                    "tw_user_id" => $twUserId,
                    'tw_user_id_x' => $twitterAccount->id,
                    "created_at" => date("Y-m-d H:i:s"),
                    "modified_at" => date("Y-m-d H:i:s")
                );
                try {
                    $conn->insert('twitter_followers', $queryData);
                } catch (UniqueConstraintViolationException $constraintViolationException) {
                    // Ignore if duplicate key error as that can occur in case of multiple processes with
                    // high number of accounts. Record will be ignored by MYSQL
                    if ($constraintViolationException->getErrorCode() != 1062) {
                        continue;
                        //throw $constraintViolationException;
                    }
                }
            }
            if (!$ignoreUnfollowers) {
                // Detect Unfollowers
                sort($followersIDs);
                // Check if follower ID is in unfollower list, if it is then delete it from unfollower list
                foreach ($followersIDs as $followersID) {
                    $isUnfollower = $this->entityManager->getRepository('App:TwitterUnfollowers')
                        ->findOneBy(array(
                            'twUserId' => $twUserId,
                            'twUserIdX' => $followersID
                        ));
                    if ($isUnfollower) {
                        $this->entityManager->remove($isUnfollower);
                        $this->entityManager->flush();
                    }
                }

                $unfollowersIDs = array();
                foreach ($existingFollowersIDs as $existingFollowersID) {
                    if (!GeneralHelpers::binary_search($existingFollowersID, $followersIDs)) {
                        $unfollowersIDs[] = $existingFollowersID;
                        $queryData = array(
                            "tw_user_id" => $twUserId,
                            'tw_user_id_x' => $existingFollowersID,
                            "created_at" => date("Y-m-d H:i:s"),
                            "modified_at" => date("Y-m-d H:i:s")
                        );
                        try {
                            $conn->insert('twitter_unfollowers', $queryData);
                        } catch (UniqueConstraintViolationException $constraintViolationException) {
                            // Ignore if duplicate key error as that can occur in case of multiple processes with
                            // high number of accounts. Record will be ignored by MYSQL
                            if ($constraintViolationException->getErrorCode() != 1062) {
                                continue;
                                //throw $constraintViolationException;
                            }
                        }
                        $conn->delete('twitter_followers', array(
                            'tw_user_id_x' => $existingFollowersID,
                            'tw_user_id' => $twUserId
                        ));
                    }

                }
            }
        } else {
            return array(
                'success' => false,
                'error' => "Auth Creds not enabled"
            );
        }

        $end_time = microtime(true);
        $response = array(
            'success' => true,
            'start' => $start_time,
            'end' => $end_time,
            'count_followers' => count($followersIDs),
            'count_new_followers' => count($newFollowersIDs),
            'count_unfollowers' => count($unfollowersIDs),
            'memory' => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true))
        );
        return $response;
    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @param array $friendsIDs
     * @param bool $enforceAll
     * @param bool $waitOnRateLimit
     * @return array|bool
     * @throws DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function executeFriendsTask($authCreds, $twUserId = -1, $friendsIDs = array(), $enforceAll = true, $waitOnRateLimit = true, $deleteExisting = false)
    {
        if ($twUserId === -1) {
            $twUserId = $authCreds->getTwUserId();
        }
        $ignoreNotFriends = false;
        GeneralHelpers::increase_php_limits();
        $apiAuth = $this->getApiAuth($authCreds);
        $newFriendsIDs = array();

        $lastHeadersFriendsIDs = null;
        $start_time = microtime(true);

        if ($authCreds->getEnabled()) {
            $conn = $this->entityManager->getConnection();
            try {
                $existingTwitterAccounts = $this->entityManager->getRepository('App:TwitterFriends')
                    ->createQueryBuilder('twitterFriends')
                    ->select('twitterFriends.twUserIdX')
                    ->andWhere('twitterFriends.twUserId = :tw_user_id')
                    ->setParameters(array(
                        'tw_user_id' => $twUserId
                    ))
                    ->orderBy('twitterFriends.twUserIdX', 'ASC')
                    ->getQuery()
                    ->getArrayResult();
                $existingFriendsIDs = GeneralHelpers::getArrayFromAssociateArray($existingTwitterAccounts, 'twUserIdX');
            } catch (\Exception $e) {
                $exceptionMsg = $e->getMessage();
                return array(
                    'success' => false,
                    'error' => "Database server error [{$exceptionMsg}]"
                );
            }
            if (count($friendsIDs) == 0) {
                $friendsIDs = $this->getFriendsIDs($authCreds, $twUserId, $enforceAll, $waitOnRateLimit);
            } else {
                $ignoreNotFriends = true;
            }
            if (!is_array($friendsIDs)) {
                return array(
                    'success' => false,
                    'error' => "Unable to fetch complete friends"
                );
            }
            if ($deleteExisting) {
                $this->deleteAllFriends($twUserId);
                $existingFriendsIDs = array();
            }
            $newFriendsIDs = array();
            foreach ($friendsIDs as $friendsID) {
                if (!GeneralHelpers::binary_search($friendsID, $existingFriendsIDs)) {
                    $newFriendsIDs[] = $friendsID;
                }
            }


            foreach ($newFriendsIDs as $newFriendsID) {
                $twitterAccount = new \ArrayObject();
                $twitterAccount->id = $newFriendsID;

                // create twitter account record
                if ($this->saveTwitterUser($twitterAccount, false) == 0) {
                    continue;
                }

                // set twitter account as follower of current authCreds
                $queryData = array(
                    "tw_user_id" => $twUserId,
                    'tw_user_id_x' => $twitterAccount->id,
                    "created_at" => date("Y-m-d H:i:s"),
                    "modified_at" => date("Y-m-d H:i:s")
                );
                try {
                    $conn->insert('twitter_friends', $queryData);
                } catch (UniqueConstraintViolationException $constraintViolationException) {
                    // Ignore if duplicate key error as that can occur in case of multiple processes with
                    // high number of accounts. Record will be ignored by MYSQL
                    if ($constraintViolationException->getErrorCode() != 1062) {
                        continue;
                        //throw $constraintViolationException;
                    }
                }
            }
            if (!$ignoreNotFriends) {
                sort($friendsIDs);
                foreach ($existingFriendsIDs as $existingFriendsID) {
                    if (!GeneralHelpers::binary_search(intval($existingFriendsID), $friendsIDs)) {
                        $conn->delete('twitter_friends', array(
                            'tw_user_id_x' => $existingFriendsID,
                            'tw_user_id' => $twUserId
                        ));
                    }
                }
            }

        } else {
            return array(
                'success' => false,
                'error' => "Auth Creds not enabled"
            );
        }
        $end_time = microtime(true);
        $response = array(
            'success' => true,
            'start' => $start_time,
            'end' => $end_time,
            'count_friends' => count($friendsIDs),
            'count_new_friends' => count($newFriendsIDs),
            'memory' => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true))
        );
        return $response;
    }

    /**
     * @param AuthCreds $authCreds
     * @param bool $enforceAll
     * @param bool $waitOnRateLimit
     * @return array|bool
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function executeMutesTask($authCreds, $enforceAll = true, $waitOnRateLimit = true)
    {
        GeneralHelpers::increase_php_limits();
        $mutedIDs = array();
        $newMutedIDs = array();

        $lastHeadersFollowersIDs = null;
        $start_time = microtime(true);

        if ($authCreds->getEnabled()) {
            $conn = $this->entityManager->getConnection();
            $existingMutedIDs = array();
            try {
                $existingTwitterAccounts = $this->entityManager->getRepository('App:TwitterMutes')
                    ->createQueryBuilder('twitterMutes')
                    ->select('twitterMutes.twUserIdX')
                    ->andWhere('twitterMutes.twUserId = :tw_user_id')
                    ->setParameters(array(
                        'tw_user_id' => $authCreds->getTwUserId()
                    ))
                    ->orderBy('twitterMutes.twUserIdX', 'ASC')
                    ->getQuery()
                    ->getArrayResult();
                $existingMutedIDs = GeneralHelpers::getArrayFromAssociateArray($existingTwitterAccounts, 'twUserIdX');
            } catch (\Exception $e) {
                $exceptionMsg = $e->getMessage();
                return array(
                    'success' => false,
                    'error' => "Database server error [{$exceptionMsg}]"
                );
            }
            $mutedIDs = $this->getMutedIDs($authCreds, $enforceAll, $waitOnRateLimit);
            if (!is_array($mutedIDs)) {
                return array(
                    'success' => false,
                    'error' => "Unable to fetch complete muted IDs"
                );
            }
            $newMutedIDs = array();
            foreach ($mutedIDs as $mutedID) {
                if (!GeneralHelpers::binary_search($mutedID, $existingMutedIDs)) {
                    $newMutedIDs[] = $mutedID;
                }
            }
            foreach ($newMutedIDs as $newMutedID) {
                $twitterAccount = new \ArrayObject();
                $twitterAccount->id = $newMutedID;

                // create twitter account record
                if ($this->saveTwitterUser($twitterAccount, false) == 0) {
                    continue;
                }

                // set twitter account as follower of current authCreds
                $queryData = array(
                    "tw_user_id" => $authCreds->getTwUserId(),
                    'tw_user_id_x' => $twitterAccount->id,
                    "created_at" => date("Y-m-d H:i:s"),
                    "modified_at" => date("Y-m-d H:i:s")
                );
                try {
                    $conn->insert('twitter_mutes', $queryData);
                } catch (UniqueConstraintViolationException $constraintViolationException) {
                    // Ignore if duplicate key error as that can occur in case of multiple processes with
                    // high number of accounts. Record will be ignored by MYSQL
                    if ($constraintViolationException->getErrorCode() != 1062) {
                        continue;
                        //throw $constraintViolationException;
                    }
                }
            }
            sort($mutedIDs);
            foreach ($existingMutedIDs as $existingMutedID) {
                if (!GeneralHelpers::binary_search($existingMutedID, $mutedIDs)) {
                    $conn->delete('twitter_mutes', array(
                        'tw_user_id_x' => $existingMutedID,
                        'tw_user_id' => $authCreds->getTwUserId()
                    ));
                }
            }
        } else {
            return array(
                'success' => false,
                'error' => "Auth Creds not enabled"
            );
        }

        $end_time = microtime(true);
        $response = array(
            'success' => true,
            'start' => $start_time,
            'end' => $end_time,
            'count_muted' => count($mutedIDs),
            'count_new_muted' => count($newMutedIDs),
            'memory' => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true))
        );
        return $response;
    }

    /**
     * @param AuthCreds $authCreds
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setTwLastUsedDatetime(AuthCreds $authCreds)
    {
        $authCreds->setLastUsedAt(new \DateTime());
        $this->entityManager->persist($authCreds);
        $this->entityManager->flush();
    }

    /**
     * @param AuthCreds $authCreds
     * @internal param string $oauthToken
     * @internal param string $oauthTokenSecret
     * @return TwApiWrapper
     */
    public function getApiAuth(AuthCreds $authCreds)
    {
        $apiAuth = new TwApiWrapper($this->consumerKey, $this->consumerSecret,
            $authCreds->getTwOauthToken(), $authCreds->getTwOauthTokenSecret(), $this, $authCreds);
        return $apiAuth;
    }

    /**
     * @param array $accountsIDs
     * @param bool $waitOnRateLimit
     * @return array
     * @throws DBALException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function executeTwAccountsUpdateTask($accountsIDs = array(), $waitOnRateLimit = true)
    {
        GeneralHelpers::increase_php_limits();
        $start_time = microtime(true);
        $conn = $this->entityManager->getConnection();
        $accountsToUpdate_existing = array();
        $updatedAccounts = 0;
        $error_in_process = false;
        $noAuthCreds = false;
        $twMinLastUsedAuthCredTime = new \DateTime('NOW');
        $twMinLastUsedAuthCredTime->modify($this->twMinLastUsedAuthCredTime);
        $twMinLastUsedAuthCredTime = $twMinLastUsedAuthCredTime->format('Y-m-d H:i:s');
        $process_response = array();
        try {
            $authCreds = $this->entityManager->getRepository('App:AuthCreds')
                ->createQueryBuilder('authCreds')
                ->select('authCreds.twOauthToken')
                ->addSelect('authCreds.twOauthTokenSecret')
                ->addSelect('authCreds.id')
                ->andWhere('authCreds.lastUsedAt < :last_used_min')
                ->andWhere('authCreds.enabled = 1')
                ->setParameters(array(
                    'last_used_min' => GeneralHelpers::addSubDatetime(date('Y-m-d H:i:s'),
                        'Y-m-d H:i:s', $twMinLastUsedAuthCredTime)
                ))
                ->orderBy('authCreds.lastUsedAt')
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
        if ($authCreds) {

            $timeToUpdateAccount = new \DateTime('NOW');
            $timeToUpdateAccount->modify($this->timeToUpdateAccount);
            $timeToUpdateAccount = $timeToUpdateAccount->format('Y-m-d H:i:s');
            $accountsToUpdate_new = $this->entityManager->getRepository('App:TwitterAccounts')
                ->createQueryBuilder('twitterAccounts')
                ->select('twitterAccounts.twUserId')
                ->where('twitterAccounts.isDisabled = :isDisabled')
                ->andWhere('twitterAccounts.twUsername = :twUsername')
                ->andWhere('twitterAccounts.createdAt = twitterAccounts.modifiedAt')
                ->setParameters(array(
                    'twUsername' => '',
                    'isDisabled' => '0'
                ));
            if (count($accountsIDs) > 0) {
                $accountsToUpdate_new = $accountsToUpdate_new->andWhere('twitterAccounts.twUserId IN (:accountIDs)')
                    ->setParameter('accountIDs', $accountsIDs);
            }
            $accountsToUpdate_new = $accountsToUpdate_new->orderBy('twitterAccounts.modifiedAt')
                ->getQuery()
                ->setMaxResults($this->maxAccountsToUpdate)
                ->getArrayResult();
            $accountsToUpdate_new = GeneralHelpers::getArrayFromAssociateArray($accountsToUpdate_new, 'twUserId');
            if (count($accountsToUpdate_new) < $this->maxAccountsToUpdate && count($accountsIDs) == 0) {
                $accountsToUpdate_existing = $this->entityManager->getRepository('App:TwitterAccounts')
                    ->createQueryBuilder('twitterAccounts')
                    ->select('twitterAccounts.twUserId')
                    ->andWhere('twitterAccounts.modifiedAt < :timeToUpdateAccount')
                    ->setParameters(array(
                        'timeToUpdateAccount' => $timeToUpdateAccount,
                    ))
                    ->orderBy('twitterAccounts.modifiedAt')
                    ->getQuery()
                    ->setMaxResults($this->maxAccountsToUpdate - count($accountsToUpdate_new))
                    ->getArrayResult();
                $accountsToUpdate_existing = GeneralHelpers::getArrayFromAssociateArray($accountsToUpdate_existing, 'twUserId');
            }
            $totalAccountsToUpdate = array_merge($accountsToUpdate_new, $accountsToUpdate_existing);

            $accountsToUpdate = \array_chunk($totalAccountsToUpdate, $this->twAccountsInApiCall);
            $authCreds = $this->entityManager->getRepository('App:AuthCreds')
                ->find($authCreds['id']);

            foreach ($accountsToUpdate as $accountsChunk) {
                $process_response = $this->fetchAndCreateTwitterUsers($authCreds, $accountsChunk);
                $updatedAccounts += $process_response['count'];
                if (!$process_response['success']) {
                    $error_in_process = true;
                    break;
                }
                if (!$this->sleep_or_break_on_rate_limit($process_response['last_headers'], $waitOnRateLimit) && $process_response['success']) {
                    break;
                }
            }
        } else {
            $noAuthCreds = true;
        }

        // Delete disabled accounts
        $conn->createQueryBuilder()
            ->delete('twitter_accounts')
            ->where('update_retries > :twDeleteDisabledAccountsAfterRetries')
            ->setParameter('twDeleteDisabledAccountsAfterRetries', $this->twDeleteDisabledAccountsAfterRetries)
            ->execute();

        $end_time = microtime(true);
        $response = array(
            'success' => true,
            'start' => $start_time,
            'end' => $end_time,
            'updated_accounts' => $updatedAccounts,
            'memory' => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true)),
            'error' => ""
        );
        if ($error_in_process && count($process_response)) {
            $response['success'] = false;
            $response['error'] = $process_response['error'];
        }
        if ($noAuthCreds) {
            $response['success'] = false;
            $response['error'] = "No Auth Creds available.";
        }
        return $response;
    }


    /**
     * @param $twUserId
     * @return \App\Entity\TwitterAccounts|null|object
     */
    public function getTwitterAccountObject($twUserId)
    {
        return $this->entityManager->getRepository('App:TwitterAccounts')
            ->find($twUserId);
    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     */
    public function createBlacklist(AuthCreds $authCreds, $twUserId)
    {
        if (!$this->entityManager->getRepository('App:TwitterAccounts')->find($twUserId)) {
            return array(
                'success' => false,
                'error' => "Can not add to blacklist. Account is not in our system."
            );
        }
        $twitterWhitelist = $this->entityManager->getRepository('App:TwitterWhitelist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if ($twitterWhitelist) {
            return array(
                'success' => false,
                'silent' => true,
                'error' => "Can not add to blacklist, remove it from whitelist first."
            );
        }

        $conn = $this->entityManager->getConnection();

        $queryData = array(
            "auth_creds_id" => $authCreds->getId(),
            'tw_user_id_x' => $twUserId,
            "created_at" => date("Y-m-d H:i:s"),
            "modified_at" => date("Y-m-d H:i:s")
        );
        try {
            $conn->insert('twitter_blacklist', $queryData);
            return array(
                'success' => true
            );
        } catch (UniqueConstraintViolationException $constraintViolationException) {
            // Ignore if duplicate key error as that can occur in case of multiple processes with
            // high number of accounts. Record will be ignored by MYSQL
            if ($constraintViolationException->getErrorCode() == 1062) {
                return array(
                    'success' => true
                );
            }
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function destroyBlacklist(AuthCreds $authCreds, $twUserId)
    {
        $twitterBlacklist = $this->entityManager->getRepository('App:TwitterBlacklist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if (!$twitterBlacklist) {
            return array(
                'success' => true
            );
        }
        try {
            $this->entityManager->remove($twitterBlacklist);
            $this->entityManager->flush();
            return array(
                'success' => true
            );
        } catch (Exception $exception) {
            // pass
        }

        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     */
    public function createWhitelist(AuthCreds $authCreds, $twUserId)
    {
        if (!$this->entityManager->getRepository('App:TwitterAccounts')->find($twUserId)) {
            return array(
                'success' => false,
                'error' => "Can not add to whitelist. Account is not in our system."
            );
        }
        $twitterBlacklist = $this->entityManager->getRepository('App:TwitterBlacklist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if ($twitterBlacklist) {
            return array(
                'success' => false,
                'silent' => true,
                'error' => "Can not add to whitelist, remove it from blacklist first."
            );
        }

        $conn = $this->entityManager->getConnection();
        $queryData = array(
            "auth_creds_id" => $authCreds->getId(),
            'tw_user_id_x' => $twUserId,
            "created_at" => date("Y-m-d H:i:s"),
            "modified_at" => date("Y-m-d H:i:s")
        );
        try {
            $conn->insert('twitter_whitelist', $queryData);
            return array(
                'success' => true
            );
        } catch (UniqueConstraintViolationException $constraintViolationException) {
            // Ignore if duplicate key error as that can occur in case of multiple processes with
            // high number of accounts. Record will be ignored by MYSQL
            if ($constraintViolationException->getErrorCode() == 1062) {
                return array(
                    'success' => true
                );
            }
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function destroyWhitelist(AuthCreds $authCreds, $twUserId)
    {
        $twitterWhitelist = $this->entityManager->getRepository('App:TwitterWhitelist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if (!$twitterWhitelist) {
            return array(
                'success' => true
            );
        }
        try {
            $this->entityManager->remove($twitterWhitelist);
            $this->entityManager->flush();
            return array(
                'success' => true
            );
        } catch (Exception $exception) {
            // pass
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createMute(AuthCreds $authCreds, $twUserId)
    {
        $twitterWhitelist = $this->entityManager->getRepository('App:TwitterWhitelist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if ($twitterWhitelist) {
            return array(
                'success' => false,
                'silent' => true,
                'error' => "Can not mute user, user is whitelisted."
            );
        }
        $conn = $this->entityManager->getConnection();

        $apiAuth = $this->getApiAuth($authCreds);
        $result = $apiAuth->post("mutes/users/create", array('user_id' => $twUserId));
        if (isset($result->errors)) {
            $error = $result->errors[0];
            $code = $error->code;
            $message = $error->message;
            if ($code == 88) {
                return array(
                    'success' => false,
                    'rate_limit_status' => true,
                    'error' => "Twitter rate limit error. "
                );
            } else {
                return array(
                    'success' => false,
                    'silent' => false,
                    'error' => "An error occurred.<br>Error code: {$code}<br>Error message: {$message}",
                );
            }
        }
        if (isset($result->screen_name)) {
            $queryData = array(
                "tw_user_id" => $authCreds->getTwUserId(),
                'tw_user_id_x' => $twUserId,
                "created_at" => date("Y-m-d H:i:s"),
                "modified_at" => date("Y-m-d H:i:s")
            );
            try {
                $conn->insert('twitter_mutes', $queryData);
                return array(
                    'success' => true,
                    'screen_name' => $result->screen_name
                );
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                // Ignore if duplicate key error as that can occur in case of multiple processes with
                // high number of accounts. Record will be ignored by MYSQL
                if ($constraintViolationException->getErrorCode() == 1062) {
                    return array(
                        'success' => true,
                        'screen_name' => isset($result->screen_name) ? $result->screen_name : ""
                    );
                }
            }
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function destroyMute(AuthCreds $authCreds, $twUserId)
    {
        $twitterBlacklisted = $this->entityManager->getRepository('App:TwitterBlacklist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if ($twitterBlacklisted) {
            return array(
                'success' => false,
                'silent' => true,
                'error' => "Can not unmute user, user is blacklisted."
            );
        }

        $twitterMute = $this->entityManager->getRepository('App:TwitterMutes')
            ->findOneBy(array(
                'twUserId' => $authCreds->gettwUserId(),
                'twUserIdX' => $twUserId
            ));

        $apiAuth = $this->getApiAuth($authCreds);
        $result = $apiAuth->post("mutes/users/destroy", array('user_id' => $twUserId));
        if (isset($result->errors)) {
            $error = $result->errors[0];
            $code = $error->code;
            $message = $error->message;
            if ($code == 88) {
                return array(
                    'success' => false,
                    'rate_limit_status' => true,
                    'error' => "Twitter rate limit error. "
                );
            } else if ($code == 272) {
                $user = $this->entityManager->getRepository('App:TwitterAccounts')
                    ->find($twUserId);
                if ($user) {
                    $umutedUsername = $user->getTwUsername();
                    if ($twitterMute) {
                        $this->entityManager->remove($twitterMute);
                        $this->entityManager->flush();
                    }
                    return array(
                        'success' => true,
                        'screen_name' => $umutedUsername
                    );
                }
            } else {
                return array(
                    'success' => false,
                    'silent' => false,
                    'error' => "An error occurred.<br>Error code: {$code}<br>Error message: {$message}",
                );
            }
        }
        if (isset($result->screen_name)) {
            if ($twitterMute) {
                $this->entityManager->remove($twitterMute);
                $this->entityManager->flush();
            }
            return array(
                'success' => true,
                'screen_name' => $result->screen_name
            );
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createFriendship(AuthCreds $authCreds, $twUserId)
    {
        $twitterBlacklist = $this->entityManager->getRepository('App:TwitterBlacklist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if ($twitterBlacklist) {
            return array(
                'success' => false,
                'silent' => true,
                'error' => "Can not follow user, user is blacklisted."
            );
        }

        $conn = $this->entityManager->getConnection();

        $apiAuth = $this->getApiAuth($authCreds);
        $result = $apiAuth->post("friendships/create", array('user_id' => $twUserId));
        if (isset($result->errors)) {
            $error = $result->errors[0];
            $code = $error->code;
            $message = $error->message;
            if ($code == 88) {
                return array(
                    'success' => false,
                    'rate_limit_status' => true,
                    'error' => "Twitter rate limit error. "
                );
            } else if ($code == 160) {
                return array(
                    'success' => true,
                    'request_sent' => true,
                );
            } else if ($code == 161) {
                return array(
                    'success' => false,
                    'silent' => false,
                    'error' => "You are unable to follow more people at this time",
                );
            } else {
                return array(
                    'success' => false,
                    'silent' => false,
                    'error' => "An error occurred.<br>Error code: {$code}<br>Error message: {$message}",
                );
            }
        }
        if (isset($result->screen_name)) {
            $queryData = array(
                "tw_user_id" => $authCreds->getTwUserId(),
                'tw_user_id_x' => $twUserId,
                "created_at" => date("Y-m-d H:i:s"),
                "modified_at" => date("Y-m-d H:i:s")
            );
            try {
                $conn->insert('twitter_friends', $queryData);
                return array(
                    'success' => true,
                    'screen_name' => $result->screen_name
                );
            } catch (UniqueConstraintViolationException $constraintViolationException) {
                // Ignore if duplicate key error as that can occur in case of multiple processes with
                // high number of accounts. Record will be ignored by MYSQL
                if ($constraintViolationException->getErrorCode() == 1062) {
                    return array(
                        'success' => true,
                        'screen_name' => $result->screen_name
                    );
                }
            }
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param AuthCreds $authCreds
     * @param int $twUserId
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function destroyFriendship(AuthCreds $authCreds, $twUserId)
    {
        $twitterWhitelist = $this->entityManager->getRepository('App:TwitterWhitelist')
            ->findOneBy(array(
                'authCredsId' => $authCreds->getId(),
                'twUserIdX' => $twUserId
            ));
        if ($twitterWhitelist) {
            return array(
                'success' => false,
                'silent' => true,
                'error' => "Can not unfollow user, user is whitelisted."
            );
        }

        $twitterFriends = $this->entityManager->getRepository('App:TwitterFriends')
            ->findOneBy(array(
                'twUserId' => $authCreds->gettwUserId(),
                'twUserIdX' => $twUserId
            ));

        $apiAuth = $this->getApiAuth($authCreds);
        $result = $apiAuth->post("friendships/destroy", array('user_id' => $twUserId));
        if (isset($result->errors)) {
            $error = $result->errors[0];
            $code = $error->code;
            $message = $error->message;
            if ($code == 88) {
                return array(
                    'success' => false,
                    'rate_limit_status' => true,
                    'error' => "Twitter rate limit error. "
                );
            } else {
                return array(
                    'success' => false,
                    'silent' => false,
                    'error' => "An error occurred.<br>Error code: {$code}<br>Error message: {$message}",
                );
            }
        }
        if (isset($result->screen_name)) {
            if ($twitterFriends) {
                $this->entityManager->remove($twitterFriends);
                $this->entityManager->flush();
            }
            return array(
                'success' => true,
                'screen_name' => $result->screen_name
            );
        }
        return array(
            'success' => false,
            'error' => "Can not process request. Please try again later."
        );

    }

    /**
     * @param $oauthToken
     * @param $oauthTokenSecret
     * @param int $status
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    function setAuthCredsStatus($oauthToken, $oauthTokenSecret, $status = 0)
    {
        $authCreds = $this->entityManager->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twOauthToken' => $oauthToken,
                'twOauthTokenSecret' => $oauthTokenSecret
            ));
        if ($authCreds) {
            $authCreds->setEnabled($status);
            $this->entityManager->persist($authCreds);
            $this->entityManager->flush();
        }
    }

    /**
     * @param $twUserId
     * @throws DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    function deleteAllFollowers($twUserId)
    {
        if ($twUserId) {
            $conn = $this->entityManager->getConnection();
            $conn->delete('twitter_followers', array(
                'tw_user_id' => $twUserId
            ));
        }
    }

    /**
     * @param $twUserId
     * @throws DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    function deleteAllFriends($twUserId)
    {
        if ($twUserId) {
            $conn = $this->entityManager->getConnection();
            $conn->delete('twitter_friends', array(
                'tw_user_id' => $twUserId
            ));
        }
    }

    function applyDatatableFilters(QueryBuilder $query, String $tableReference, Request $request, AuthCreds $authCreds)
    {

        $twAuthCredsId = $authCreds->getId();
        $twUserId = $authCreds->getTwUserId();

        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";

        $notMyFollowers = $request->get('not_my_followers') == "true" ? true : false;
        $myFollowers = $request->get('my_followers') == "true" ? true : false;
        $iAmFollowing = $request->get('iam_following') == "true" ? true : false;
        $iAmNotFollowing = $request->get('iam_not_following') == "true" ? true : false;
        $mutedFilter = $request->get('muted') == "true" ? true : false;
        $notMutedFilter = $request->get('not_muted') == "true" ? true : false;
        $whitelistedFilter = $request->get('whitelisted') == "true" ? true : false;
        $blacklistedFilter = $request->get('blacklisted') == "true" ? true : false;
        $skipWhitelistedFilter = $request->get('skip_whitelisted') == "true" ? true : false;
        $skipBlacklistedFilter = $request->get('skip_blacklisted') == "true" ? true : false;
        $showWithProfilePic = $request->get('show_with_profile_pic') == "true" ? true : false;
        $showWithoutProfilePic = $request->get('show_without_profile_pic') == "true" ? true : false;
        $showVerifiedAccounts = $request->get('show_verified_accounts') == "true" ? true : false;
        $skipVerifiedAccounts = $request->get('skip_verified_accounts') == "true" ? true : false;

        $followersIDs = array('-1');
        $friendsIDs = array('-1');
        $mutedIDs = array('-1');
        $whitelistedIDs = array('-1');
        $blacklistedIDs = array('-1');

        if ($myFollowers || $notMyFollowers) {
            $followersIDs = $this->entityManager->getRepository('App:TwitterFollowers')
                ->createQueryBuilder('twitterFollowers')
                ->select('twitterFollowers.twUserIdX')
                ->where('twitterFollowers.twUserId = :twUserId')
                ->setParameter('twUserId', $twUserId)
                ->getQuery()
                ->getArrayResult();
            $followersIDs = GeneralHelpers::getArrayFromAssociateArray($followersIDs, 'twUserIdX');
            if (count($followersIDs) == 0) {
                $followersIDs = array('-1');
            }
        }
        if ($iAmFollowing || $iAmNotFollowing) {
            $friendsIDs = $this->entityManager->getRepository('App:TwitterFriends')
                ->createQueryBuilder('TwitterFriends')
                ->select('TwitterFriends.twUserIdX')
                ->where('TwitterFriends.twUserId = :twUserId')
                ->setParameter('twUserId', $twUserId)
                ->getQuery()
                ->getArrayResult();
            $friendsIDs = GeneralHelpers::getArrayFromAssociateArray($friendsIDs, 'twUserIdX');
            if (count($friendsIDs) == 0) {
                $friendsIDs = array('-1');
            }
        }
        if ($mutedFilter || $notMutedFilter) {
            $mutedIDs = $this->entityManager->getRepository('App:TwitterMutes')
                ->createQueryBuilder('twitterMutes')
                ->select('twitterMutes.twUserIdX')
                ->where('twitterMutes.twUserId = :twUserId')
                ->setParameter('twUserId', $twUserId)
                ->getQuery()
                ->getArrayResult();
            $mutedIDs = GeneralHelpers::getArrayFromAssociateArray($mutedIDs, 'twUserIdX');
            if (count($mutedIDs) == 0) {
                $mutedIDs = array('-1');
            }
        }
        if ($whitelistedFilter || $skipWhitelistedFilter) {
            $whitelistedIDs = $this->entityManager->getRepository('App:TwitterWhitelist')
                ->createQueryBuilder('twitterWhitelist')
                ->select('twitterWhitelist.twUserIdX')
                ->where('twitterWhitelist.authCredsId = :authCredsId')
                ->setParameter('authCredsId', $twAuthCredsId)
                ->getQuery()
                ->getArrayResult();
            $whitelistedIDs = GeneralHelpers::getArrayFromAssociateArray($whitelistedIDs, 'twUserIdX');
            if (count($whitelistedIDs) == 0) {
                $whitelistedIDs = array('-1');
            }
        }
        if ($blacklistedFilter || $skipBlacklistedFilter) {
            $blacklistedIDs = $this->entityManager->getRepository('App:TwitterBlacklist')
                ->createQueryBuilder('twitterBlacklist')
                ->select('twitterBlacklist.twUserIdX')
                ->where('twitterBlacklist.authCredsId = :authCredsId')
                ->setParameter('authCredsId', $twAuthCredsId)
                ->getQuery()
                ->getArrayResult();
            $blacklistedIDs = GeneralHelpers::getArrayFromAssociateArray($blacklistedIDs, 'twUserIdX');
            if (count($blacklistedIDs) == 0) {
                $blacklistedIDs = array('-1');
            }
        }

        if ($myFollowers) {
            $query->andWhere("{$tableReference}.twUserIdX IN (:followersIDs)")
                ->setParameter('followersIDs', $followersIDs);
        } else if ($notMyFollowers) {
            $query->andWhere("{$tableReference}.twUserIdX NOT IN (:followersIDs)")
                ->setParameter('followersIDs', $followersIDs);
        }

        if ($iAmFollowing) {
            $query->andWhere("{$tableReference}.twUserIdX IN (:friendsIDs)")
                ->setParameter('friendsIDs', $friendsIDs);
        } else if ($iAmNotFollowing) {
            $query->andWhere("{$tableReference}.twUserIdX NOT IN (:friendsIDs)")
                ->setParameter('friendsIDs', $friendsIDs);
        }

        if ($mutedFilter) {
            $query->andWhere("{$tableReference}.twUserIdX IN (:mutedIDs)")
                ->setParameter('mutedIDs', $mutedIDs);
        } else if ($notMutedFilter) {
            $query->andWhere("{$tableReference}.twUserIdX NOT IN (:mutedIDs)")
                ->setParameter('mutedIDs', $mutedIDs);
        }

        if ($whitelistedFilter) {
            $query->andWhere("{$tableReference}.twUserIdX IN (:whitelistedIDs)")
                ->setParameter('whitelistedIDs', $whitelistedIDs);
        } else if ($skipWhitelistedFilter) {
            $query->andWhere("{$tableReference}.twUserIdX NOT IN (:whitelistedIDs)")
                ->setParameter('whitelistedIDs', $whitelistedIDs);
        }
        if ($blacklistedFilter) {
            $query->andWhere("{$tableReference}.twUserIdX IN (:blacklistedIDs)")
                ->setParameter('blacklistedIDs', $blacklistedIDs);
        } else if ($skipBlacklistedFilter) {
            $query->andWhere("{$tableReference}.twUserIdX NOT IN (:blacklistedIDs)")
                ->setParameter('blacklistedIDs', $blacklistedIDs);
        }

        if ($search) {
            $query->andWhere('twitterAccount.twName LIKE :search OR twitterAccount.twUsername LIKE :search OR twitterAccount.twLocation LIKE :search')
                ->setParameter(':search', '%' . $search . '%');
        }

        if ($showWithProfilePic) {
            $query->andWhere('twitterAccount.twProfileImage NOT LIKE :profile_pic_url')
                ->setParameter(':profile_pic_url', '%' . $this->defaultTwitterProfilePictureUrl . '%');
        }
        if ($showWithoutProfilePic) {
            $query->andWhere('twitterAccount.twProfileImage LIKE :profile_pic_url')
                ->setParameter(':profile_pic_url', '%' . $this->defaultTwitterProfilePictureUrl . '%');
        }

        if ($showVerifiedAccounts) {
            $query->andWhere('twitterAccount.twVerified = :verified_status')
                ->setParameter(':verified_status', '1');
        }
        if ($skipVerifiedAccounts) {
            $query->andWhere('twitterAccount.twVerified = :verified_status')
                ->setParameter(':verified_status', '0');
        }

        return $query;

    }


}