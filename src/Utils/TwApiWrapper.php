<?php
/**
 * Created by PhpStorm.
 * User: kamii
 * Date: 1/14/18
 * Time: 2:54 AM
 */

namespace App\Utils;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Entity\AuthCreds;
use Doctrine\ORM\OptimisticLockException;

class TwApiWrapper extends TwitterOAuth
{
    protected $twitterHelpers;
    protected $consumerKey, $consumerSecret, $oauthToken, $oauthTokenSecret;
    protected $authCreds;

    public function __construct($consumerKey, $consumerSecret, $oauthToken = null, $oauthTokenSecret = null,
                                TwitterHelpers $twitterHelpers, AuthCreds $authCreds)
    {
        parent::__construct($consumerKey, $consumerSecret, $oauthToken, $oauthTokenSecret);
        $this->setTimeouts(15, 20);
        $this->twitterHelpers = $twitterHelpers;
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->oauthToken = $oauthToken;
        $this->oauthTokenSecret = $oauthTokenSecret;
        $this->authCreds = $authCreds;
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array|bool|null|object
     * @throws OptimisticLockException
     */
    public function get($path, array $parameters = [])
    {
        $result = null;

        // Try at least 5 times on failed API attempts
        $apiExceptionCount = 1;
        while (true) {
            try {
                $result = parent::get($path, $parameters);
                $this->twitterHelpers->setTwLastUsedDatetime($this->authCreds);
                break;
            } catch (OptimisticLockException $e) {
                throw $e;
            } catch (\Exception $exception) {
                if($apiExceptionCount >= 5){
                    return false;
                }
                $apiExceptionCount++;
                sleep(3);
            }
        }
        if (isset(($result->errors)[0]->code) && ($result->errors)[0]->code == "89") {
            $this->twitterHelpers->setAuthCredsStatus($this->oauthToken, $this->oauthTokenSecret, 0);
        } else {
            $this->twitterHelpers->setAuthCredsStatus($this->oauthToken, $this->oauthTokenSecret, 1);
        }
        return $result;
    }

    /**
     * @param string $path
     * @param array $parameters
     * @return array|bool|null|object
     * @throws OptimisticLockException
     */
    public function post($path, array $parameters = [])
    {
        $result = null;

        // Try at least 5 times on failed API attempts
        $apiExceptionCount = 1;
        while (true) {
            try {
                $result = parent::post($path, $parameters);
                $this->twitterHelpers->setTwLastUsedDatetime($this->authCreds);
                break;
            } catch (OptimisticLockException $e) {
                throw $e;
            } catch (\Exception $exception) {
                if ($apiExceptionCount >= 5) {
                    return false;
                }
                $apiExceptionCount++;
                sleep(3);
            }
        }
        if (isset(($result->errors)[0]->code) && ($result->errors)[0]->code == "89") {
            $this->twitterHelpers->setAuthCredsStatus($this->oauthToken, $this->oauthTokenSecret, 0);
        } else {
            $this->twitterHelpers->setAuthCredsStatus($this->oauthToken, $this->oauthTokenSecret, 1);
        }
        return $result;
    }
}