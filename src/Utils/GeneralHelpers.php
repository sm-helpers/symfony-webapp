<?php
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: GeneralHelpers.php
 * Date: 12/1/17
 * Time: 12:22 AM
 */


namespace App\Utils;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GeneralHelpers
{
    /*
     * CONST variables start
     */
    const BULK_QUERIES_BATCH_SIZE = 50;

    /**
     * GeneralHelpers constructor.
     */
    public function __construct()
    {
    }

    /*
     * CONST variables end
     */

    /**
     * @param $array
     * @param $key
     * @return array
     */
    public static function getArrayFromAssociateArray($array, $key)
    {
        $result = array();
        foreach ($array as $value) {
            $result[] = $value[$key];
        }
        return $result;
    }

    public static function binary_search($needle, $haystack)
    {
        $top = sizeof($haystack) - 1;
        $bot = 0;

        while ($top >= $bot) {
            $p = floor(($top + $bot) / 2);
            if ($haystack[$p] < $needle) $bot = $p + 1;
            elseif ($haystack[$p] > $needle) $top = $p - 1;
            else return TRUE;
        }

        return FALSE;
    }

    /**
     * @param string $date
     * @param string $format
     * @param string $subValue
     * @return false|string
     */
    public static function addSubDatetime($date, $format, $subValue)
    {
        $source_timestamp = strtotime($date);
        $new_timestamp = strtotime($subValue, $source_timestamp);
        return date($format, $new_timestamp);
    }

    /**
     * @param array|\ArrayObject $data
     * @param int $status
     * @return JsonResponse
     */
    public static function makeJsonResponse($data, int $status = 200)
    {
        $response = new JsonResponse(json_encode($data), $status,
            array('content-type' => 'application/json'), true);
        $response->setEncodingOptions($response->getEncodingOptions() | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        return $response;
    }

    public static function number_format_short($n, $precision = 1)
    {
        if (!is_numeric($n)) {
            return $n;
        }
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ($precision > 0) {
            $dotzero = '.' . str_repeat('0', $precision);
            $n_format = str_replace($dotzero, '', $n_format);
        }

        return $n_format . $suffix;
    }

    public static function time_elapsed_string($datetime, $full = false)
    {
        $now = new \DateTime;
        if ($datetime instanceof \DateTime) {
            $ago = $datetime;
        } else {
            $ago = new \DateTime($datetime);
        }
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function getReadableMemorySize($size)
    {
        $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    public static function increase_php_limits()
    {
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', '3600');
    }
    

}