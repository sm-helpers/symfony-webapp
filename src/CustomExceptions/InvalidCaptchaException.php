<?php
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: InvalidCaptchaException.php
 * Date: 11/1/17
 * Time: 1:02 AM
 */


namespace App\CustomExceptions;


use Symfony\Component\Security\Core\Exception\AccountStatusException;

class InvalidCaptchaException extends AccountStatusException
{

}