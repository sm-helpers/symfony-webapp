<?php
/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 3/11/18, 5:00 AM
 */

namespace App\Command;

use App\Routines\TwitterAccountsRoutines;
use App\Utils\TasksManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class RunTaskCommand extends Command
{
    private $commandName = 'app:task:run';

    private $tasksManager;
    private $twitterAccountsRoutines;


    public function __construct(TwitterAccountsRoutines $twitterAccountsRoutines, TasksManager $tasksManager)
    {
        $this->tasksManager = $tasksManager;
        $this->twitterAccountsRoutines = $twitterAccountsRoutines;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription("This command will run a task for given ID.")
            ->setHelp($this->getCommandHelp())
            ->addArgument("id", InputArgument::REQUIRED, "ID of the task");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $taskID = $input->getArgument("id");
        try {
            $task = $this->twitterAccountsRoutines->handleRunSingleTask($taskID);
        } catch (\Exception $e) {
            try {
                $this->tasksManager->updateTaskOnClosing($taskID, TasksManager::STATUS_CANCELED);
            } catch (\Exception $exception) {
                // pass
            }
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());
        }

    }

    protected function getCommandHelp()
    {
        return "This command will run a task for given ID";

    }
}