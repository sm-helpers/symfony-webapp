<?php
/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 3/11/18, 5:00 AM
 */

namespace App\Command;

use App\Routines\TwitterAccountsRoutines;
use App\Utils\TasksManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class RunTasksCommand extends Command
{
    private $commandName = 'app:tasks:run';

    private $twitterAccountsRoutines;


    public function __construct(TwitterAccountsRoutines $twitterAccountsRoutines)
    {
        $this->twitterAccountsRoutines = $twitterAccountsRoutines;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription("This command will run multiple tasks concurrently")
            ->setHelp($this->getCommandHelp());
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->twitterAccountsRoutines->handleRunMultipleTasks();
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());
        }

    }

    protected function getCommandHelp()
    {
        return "This command will run multiple tasks concurrently";

    }
}