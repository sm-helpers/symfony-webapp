<?php
/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 3/9/18, 1:33 AM
 */

namespace App\Command;

use App\Routines\TwitterAccountsRoutines;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CreateTasksCommand extends Command
{
    private $commandName = 'app:tasks:create';

    private $twitterAccountsRoutines;


    public function __construct(TwitterAccountsRoutines $twitterAccountsRoutines)
    {
        $this->twitterAccountsRoutines = $twitterAccountsRoutines;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription("This command will run a routine to create tasks (fetch_followers, fetch_friends, fetch_unfollowers, fetch_mutes)" .
                " of each authenticated user")
            ->setHelp($this->getCommandHelp());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->twitterAccountsRoutines->handleCreateTasks(0, array(), array(), null, false, $output);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());
        }
    }

    protected function getCommandHelp()
    {
        return "Nothing to help. Command has no argument.";

    }

}