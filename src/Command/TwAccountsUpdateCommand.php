<?php
/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 3/9/18, 1:33 AM
 */

namespace App\Command;

use App\Routines\TwitterAccountsRoutines;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TwAccountsUpdateCommand extends Command
{
    private $commandName = 'app:twaccounts:update';

    private $twitterAccountsRoutines;


    public function __construct(TwitterAccountsRoutines $twitterAccountsRoutines)
    {
        $this->twitterAccountsRoutines = $twitterAccountsRoutines;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName($this->commandName)
            ->setDescription("This command will update twitter accounts information.")
            ->setHelp($this->getCommandHelp());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->twitterAccountsRoutines->handleTwAccountsUpdate($output);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());
        }
    }

    protected function getCommandHelp()
    {
        return "Nothing to help. Command has no argument.";

    }

}