<?php
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: UserRegistrationstration.php
 * Date: 10/27/17
 * Time: 1:08 AM
 */

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserRegistration extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('email', EmailType::class)
            ->add('plainPassword', PasswordType::class, array(
                'label'=>'Password'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Users::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_registration_csrf_token',
            // a unique key to help generate the secret token
            'csrf_token_id'   => 'user_registration_form_token',
        ));
    }
}