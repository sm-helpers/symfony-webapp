<?php
/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 3/9/18, 12:59 AM
 */

namespace App\Routines;


use App\Entity\AuthCreds;
use App\Entity\FetchTasksLogs;
use App\Entity\Tasks;
use App\Utils\TasksManager;
use App\Utils\TwitterHelpers;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TwitterAccountsRoutines
{
    const TASK_NAME_FOR_FOLLLOWERS = "fetch_followers";
    const FUNCTION_NAME_FOR_FOLLLOWERS = "fetchFollowers";
    const TASK_NAME_FOR_FRIENDS = "fetch_friends";
    const FUNCTION_NAME_FOR_FRIENDS = "fetchFriends";
    const TASK_NAME_FOR_MUTES = "fetch_mutes";
    const FUNCTION_NAME_FOR_MUTES = "fetchMutes";


    /*
     * Class Data Members
     */

    protected $entityManager;
    protected $tasksManager;
    protected $twAccountsUpdateLogger;
    protected $taskLogger;
    protected $maxTasksOfEachType;
    protected $twitterHelpers;


    /**
     * TwitterAccountsRoutines constructor.
     * @param $maxTasksOfEachType
     * @param TwitterHelpers $twitterHelpers
     * @param EntityManager $entityManager
     * @param LoggerInterface $twAccountsUpdateLogger
     * @param LoggerInterface $taskLogger
     * @param $appAbsolutePath
     */
    public function __construct($maxTasksOfEachType, TwitterHelpers $twitterHelpers, EntityManager $entityManager,
                                LoggerInterface $twAccountsUpdateLogger, LoggerInterface $taskLogger, $appAbsolutePath)
    {
        $this->twitterHelpers = $twitterHelpers;
        $this->tasksManager = new TasksManager($entityManager, $this, $appAbsolutePath);
        $this->entityManager = $entityManager;
        $this->twAccountsUpdateLogger = $twAccountsUpdateLogger;
        $this->taskLogger = $taskLogger;
        $this->maxTasksOfEachType = $maxTasksOfEachType;
    }

    /**
     * Handles tasks:create command. Creates tasks related to authenticated twitter accounts (fetch followers, friends, mutes, etc).
     * @param int $authCredsId
     * @param array $taskNames
     * @param array $params
     * @param null $taskDatetime
     * @param bool $force
     * @param OutputInterface|null $output
     * @throws \Doctrine\DBAL\DBALException
     */
    public function handleCreateTasks($authCredsId = 0, Array $taskNames = array(), Array $params = array(), $taskDatetime = null, bool $force = false, OutputInterface $output = null)
    {
        $date = new \DateTime();
        if (!$taskDatetime) {
            $taskDatetime = $date;
        }
        if (!$taskNames) {
            $taskNames = array(
                TwitterAccountsRoutines::TASK_NAME_FOR_FOLLLOWERS,
                TwitterAccountsRoutines::TASK_NAME_FOR_FRIENDS,
                TwitterAccountsRoutines::TASK_NAME_FOR_MUTES
            );
        }
        if ($authCredsId) {
            $authCreds = $this->entityManager->getRepository('App:AuthCreds')
                ->findBy(array(
                    'enabled' => true,
                    'id' => $authCredsId
                ));
        } else {
            $authCreds = $this->entityManager->getRepository('App:AuthCreds')
                ->findBy(array(
                    'enabled' => true
                ));
        }

        foreach ($authCreds as $authCred) {
            if ($output) {
                $output->writeln("Creating '@{$authCred->getTwUsername()}' tasks...");
            }
            foreach ($taskNames as $taskName) {

                if (in_array($taskName, array(TwitterAccountsRoutines::TASK_NAME_FOR_FOLLLOWERS, TwitterAccountsRoutines::TASK_NAME_FOR_FRIENDS))) {
                    // create parameters only if task is fetch_followers or fetch_friends
                    if (!$params) { // create parameters only if parameters are null - just a safe check
                        $params = array(
                            'tw_user_id' => $authCred->getTwUserId()
                        );
                    }
                } else {
                    $params = array();
                }

                $taskQB = $this->entityManager->getRepository('App:Tasks')
                    ->findExistingTasks($taskDatetime, $authCred, $taskName, $params);

                if (count($taskQB) === 0 || $force) {
                    try {
                        $this->tasksManager->createTask($taskName, $authCred->getId(), $params, 0, $taskDatetime);
                        if ($output) {
                            $output->writeln("\t{$taskName} - Created " .
                                " | " . $date->format('Y-m-d H:i:s'));
                        }
                    } catch (OptimisticLockException $e) {
                        if ($output) {
                            $output->writeln("\t{$taskName}. OptimisticLockException: {$e->getMessage()}" .
                                " | " . $date->format('Y-m-d H:i:s'));
                        }
                    }
                } else {
                    if ($output) {
                        $output->writeln("\t{$taskName}. Ignored (Already Created)" .
                            " | " . $date->format('Y-m-d H:i:s'));
                    }
                }
            }

        }
    }

    /**
     * Handles twaccounts:update command. Will be called on tasks:run command.
     * @param OutputInterface|null $output
     */
    public function handleTwAccountsUpdate(OutputInterface $output = null)
    {
        $date = new \DateTime();
        $this->twAccountsUpdateLogger->info("Starting service at {$date->format('Y-m-d H:i:s')}");
        try {
            $info = $this->twitterHelpers->executeTwAccountsUpdateTask();
            $error = "";
            if (!$info['success']) {
                $error = "Error: {$info['error']} | ";
            }
            $msg = "{$error}Updated {$info['updated_accounts']} accounts. " . "Memory usage {$info['memory']}.";
            $this->twAccountsUpdateLogger->info($msg);
            if ($output) {
                $date = new \DateTime();
                $output->writeln("{$date->format('Y-m-d H:i:s')} :: {$msg}");
            }
        } catch (\Exception $e) {

            $date = new \DateTime();
            $this->twAccountsUpdateLogger->error("Unexpected Exception: {$e->getMessage()}");
            $this->twAccountsUpdateLogger->info("Closing service at {$date->format('Y-m-d H:i:s')}");

            if ($output) {
                $output->writeln($e->getMessage());
                $output->writeln($e->getTraceAsString());
            }
        } finally {
            if ($output) {
                $output->writeln("\n********************************************************************\n");
            }
        }
        $date = new \DateTime();
        $this->twAccountsUpdateLogger->info("Closing service at {$date->format('Y-m-d H:i:s')}");
    }

    /**
     * Handles tasks:run command. Executes multiple tasks simultaneously, no of tasks are limited by max_tasks_of_one_type_to_run_in_one_instance in parameters.yml
     * @throws \Exception
     */
    public function handleRunMultipleTasks()
    {
        for ($i = 0; $i < $this->maxTasksOfEachType; $i++) {
            $task = $this->tasksManager->getNextTask(TwitterAccountsRoutines::TASK_NAME_FOR_FOLLLOWERS);
            if ($task) {
                $this->tasksManager->runTask($task->getId());
            }
            $task = $this->tasksManager->getNextTask(TwitterAccountsRoutines::TASK_NAME_FOR_MUTES);
            if ($task) {
                $this->tasksManager->runTask($task->getId());
            }
            $task = $this->tasksManager->getNextTask(TwitterAccountsRoutines::TASK_NAME_FOR_FRIENDS);
            if ($task) {
                $this->tasksManager->runTask($task->getId());
            }
        }
    }

    /**
     * Handles task:run command. Run the task according to its type.
     * @param $taskID
     * @return array
     * @throws \Exception
     */
    public function handleRunSingleTask($taskID)
    {
        $tasksRunning = $this->entityManager->getRepository('App:Tasks')
            ->createQueryBuilder('tasks')
            ->select('count(tasks)')
            ->andWhere('tasks.status = :status')
            ->setParameters(array(
                'status' => TasksManager::STATUS_PROCESSING
            ))
            ->getQuery()
            ->getSingleScalarResult();
        if ($tasksRunning > 40) {
            $date = new \DateTime();
            $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - {$tasksRunning} tasks are already running. Unable to start task");
            return array(
                'success' => false,
                "error" => "Task ID {$taskID} - {$tasksRunning} tasks are already running. Unable to start task"
            );
        }

        $conn = $this->entityManager->getConnection();
        $statement = $conn->prepare("SHOW STATUS WHERE 'variable_name' = 'Threads_connected'");
        $statement->execute();
        $currentMysqlConnections = $statement->fetch();
        $currentMysqlConnections = intval($currentMysqlConnections['Value']);
        if ($currentMysqlConnections > 500) {
            $date = new \DateTime();
            $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - {$currentMysqlConnections} mysql threads are already in use. Unable to start task");
            return array(
                'success' => false,
                "error" => "Task ID {$taskID} - {$currentMysqlConnections} mysql threads are already in use. Unable to start task"
            );
        }
        $task = $this->tasksManager->getTaskById($taskID);
        if (!$task) {
            return array(
                'success' => false,
                "error" => "No task found with ID " . $taskID
            );
        }
        $date = new \DateTime();
        $this->tasksManager->updateTaskOnStart($taskID);
        $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: Starting");
        $authCreds = $task->getAuthCreds();
        if (!$authCreds->getEnabled()) {
            $date = new \DateTime();
            $this->taskLogger->error("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: Auth Creds are not enabled");
            $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: Closing");
            $this->tasksManager->updateTaskOnClosing($taskID, TasksManager::STATUS_CANCELED);
            return array(
                'success' => false,
                "error" => "Auth Creds are not enabled"
            );
        }
        $functionName = null;
        if ($task->getName() == TwitterAccountsRoutines::TASK_NAME_FOR_FOLLLOWERS) {
            $functionName = TwitterAccountsRoutines::FUNCTION_NAME_FOR_FOLLLOWERS;
        } else if ($task->getName() == TwitterAccountsRoutines::TASK_NAME_FOR_FRIENDS) {
            $functionName = TwitterAccountsRoutines::FUNCTION_NAME_FOR_FRIENDS;
        } else if ($task->getName() == TwitterAccountsRoutines::TASK_NAME_FOR_MUTES) {
            $functionName = TwitterAccountsRoutines::FUNCTION_NAME_FOR_MUTES;
        }

        if ($functionName) {
            $response = $this->$functionName($task);
            $date = new \DateTime();
            if ($response['success']) {
                $this->tasksManager->updateTaskOnClosing($taskID, TasksManager::STATUS_COMPLETE);
                $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: {$response['message']}");
            } else {
                $this->tasksManager->updateTaskOnClosing($taskID, TasksManager::STATUS_CANCELED);
                $this->taskLogger->error("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: {$response['error']}");
            }
            $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: Closing");
        } else {
            $this->taskLogger->error("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: Unknown task type.");
            $this->taskLogger->info("{$date->format('Y-m-d H:i:s')}: Task ID {$taskID} - Type {$task->getName()} :: Closing");
            $this->tasksManager->updateTaskOnClosing($taskID, TasksManager::STATUS_CANCELED);
            return array(
                'success' => false,
                "error" => "Unknown task type."
            );
        }
    }

    /**
     * @param $twUserId
     * @param \DateTime $dateTime
     * @return FetchTasksLogs|null|object
     * @throws OptimisticLockException
     */
    private function getFetchTasksLogs($twUserId, \DateTime $dateTime)
    {
        $twitterAccount = $this->entityManager->getRepository('App:TwitterAccounts')
            ->find($twUserId);
        $fetchTasksLogs = $this->entityManager->getRepository('App:FetchTasksLogs')
            ->findOneBy(array(
                'twitterAccount' => $twitterAccount,
                'date' => $dateTime
            ));
        if (!$fetchTasksLogs) {
            $fetchTasksLogs = new FetchTasksLogs();
            $fetchTasksLogs->setTwitterAccount($twitterAccount);
            $fetchTasksLogs->setDatetime($dateTime);
            $fetchTasksLogs->setDate($dateTime);
            $fetchTasksLogs->setCreatedAt(new \DateTime());
            $fetchTasksLogs->setModifiedAt(new \DateTime());
            $this->entityManager->persist($fetchTasksLogs);
            $this->entityManager->flush();
        }
        return $fetchTasksLogs;
    }

    /**
     * @param Tasks $task
     * @return array
     * @throws OptimisticLockException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function fetchFollowers(Tasks $task)
    {
        $authCreds = $task->getAuthCreds();

        $taskParams = $task->getParameters();
        if (isset($taskParams['tw_user_id'])) {
            $twUserId = $taskParams['tw_user_id'];
        } else {
            $twUserId = $authCreds->getTwUserId();
        }

        if (isset($taskParams['date'])) {
            $dateTime = date_create($taskParams['date']);
        } else {
            $dateTime = (new \DateTime())->modify('-1 day');
        }

        if (isset($taskParams['delete_existing'])) {
            $deleteExisting = $taskParams['delete_existing'];
        } else {
            $deleteExisting = false;
        }

        $fetchTasksLogs = $this->getFetchTasksLogs($twUserId, $dateTime);
        $response = $this->twitterHelpers->executeFollowersTask($authCreds, $twUserId, array(), true, true, $deleteExisting);
        if (isset($response['success']) && $response['success']) {
            $totalFollowers = array(
                'count' => $response['count_followers'],
                'success' => true,
                'error' => ''
            );
            $fetchTasksLogs->setTotalFollowers($totalFollowers);

            if ($deleteExisting) {
                $newFollowers = $totalFollowers;
            } else {
                // Adds new followers to existing count of new followers
                $existing = $fetchTasksLogs->getNewFollowers();
                $countExisting = $existing['count'];
                $newFollowers = array(
                    'count' => $response['count_new_followers'] + $countExisting,
                    'success' => true,
                    'error' => ''
                );
            }
            $fetchTasksLogs->setNewFollowers($newFollowers);

            // Adds new unfollowers to existing count of new unfollowers
            $existing = $fetchTasksLogs->getUnfollowers();
            $unfollowers = array(
                'count' => $response['count_unfollowers'] + $existing['count'],
                'success' => true,
                'error' => ''
            );
            $fetchTasksLogs->setUnfollowers($unfollowers);

            $output = array(
                'success' => true,
                "message" => "{$authCreds->getTwUsername()} - {$response['count_followers']} Total Followers - " .
                    " {$response['count_new_followers']} New Followers - {$response['memory']} Memory utilized"
            );
        } else {
            $existing = $fetchTasksLogs->getTotalFollowers();
            $error = array(
                'count' => $existing['count'],
                'success' => false,
                'error' => $response['error']
            );
            $fetchTasksLogs->setTotalFollowers($error);

            $existing = $fetchTasksLogs->getNewFollowers();
            $error = array(
                'count' => $existing['count'],
                'success' => false,
                'error' => $response['error']
            );
            $fetchTasksLogs->setNewFollowers($error);

            $existing = $fetchTasksLogs->getUnfollowers();

            $error = array(
                'count' => $existing['count'],
                'success' => false,
                'error' => $response['error']
            );
            $fetchTasksLogs->setUnfollowers($error);
            $output = array(
                'success' => false,
                'error' => $response['error']
            );
        }
        $fetchTasksLogs->setModifiedAt(new \DateTime());
        $this->entityManager->persist($fetchTasksLogs);
        $this->entityManager->flush();
        return $output;
    }

    /**
     * @param Tasks $task
     * @return array
     * @throws OptimisticLockException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function fetchFriends(Tasks $task)
    {
        $authCreds = $task->getAuthCreds();

        $taskParams = $task->getParameters();
        if (isset($taskParams['tw_user_id'])) {
            $twUserId = $taskParams['tw_user_id'];
        } else {
            $twUserId = $authCreds->getTwUserId();
        }

        if (isset($taskParams['date'])) {
            $dateTime = date_create($taskParams['date']);
        } else {
            $dateTime = (new \DateTime())->modify('-1 day');
        }

        if (isset($taskParams['delete_existing'])) {
            $deleteExisting = $taskParams['delete_existing'];
        } else {
            $deleteExisting = false;
        }

        $fetchTasksLogs = $this->getFetchTasksLogs($twUserId, $dateTime);
        $response = $this->twitterHelpers->executeFriendsTask($authCreds, $twUserId, array(), true, true, $deleteExisting);
        if (isset($response['success']) && $response['success']) {
            $totalFriends = array(
                'count' => $response['count_friends'],
                'success' => true,
                'error' => ''
            );
            $fetchTasksLogs->setTotalFriends($totalFriends);

            if ($deleteExisting) {
                $newFriends = $totalFriends;
            } else {
                // Adds new friends to existing count of new friends
                $existing = $fetchTasksLogs->getNewFriends();
                $newFriends = array(
                    'count' => $response['count_new_friends'] + $existing['count'],
                    'success' => true,
                    'error' => ''
                );
            }
            $fetchTasksLogs->setNewFriends($newFriends);

            $output = array(
                'success' => true,
                "message" => "{$authCreds->getTwUsername()} - {$response['count_friends']} Total Friends - " .
                    " {$response['count_new_friends']} New Friends - {$response['memory']} Memory utilized"
            );
        } else {
            $existing = $fetchTasksLogs->getTotalFriends();
            $error = array(
                'count' => $existing['count'],
                'success' => false,
                'error' => $response['error']
            );
            $fetchTasksLogs->setTotalFriends($error);

            $existing = $fetchTasksLogs->getNewFriends();
            $error = array(
                'count' => $existing['count'],
                'success' => false,
                'error' => $response['error']
            );
            $fetchTasksLogs->setNewFriends($error);

            $output = array(
                'success' => false,
                'error' => $response['error']
            );
        }
        $fetchTasksLogs->setModifiedAt(new \DateTime());
        $this->entityManager->persist($fetchTasksLogs);
        $this->entityManager->flush();
        return $output;
    }

    /**
     * @param Tasks $task
     * @return array
     * @throws OptimisticLockException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function fetchMutes(Tasks $task)
    {
        $authCreds = $task->getAuthCreds();

        $fetchTasksLogs = $this->getFetchTasksLogs($authCreds->getTwUserId(), (new \DateTime())->modify('-1 day'));
        $response = $this->twitterHelpers->executeMutesTask($authCreds);
        if (isset($response['success']) && $response['success']) {
            $muted = array(
                'count' => $response['count_muted'],
                'success' => true,
                'error' => ''
            );
            $fetchTasksLogs->setMutes($muted);
            $output = array(
                'success' => true,
                "message" => "{$authCreds->getTwUsername()} - {$response['count_muted']} Total Mutes - " .
                    " {$response['memory']} Memory utilized"
            );
        } else {
            $existing = $fetchTasksLogs->getMutes();
            $error = array(
                'count' => $existing['count'],
                'success' => false,
                'error' => $response['error']
            );
            $fetchTasksLogs->setMutes($error);
            $output = array(
                'success' => false,
                'error' => $response['error']
            );
        }
        $fetchTasksLogs->setModifiedAt(new \DateTime());
        $this->entityManager->persist($fetchTasksLogs);
        $this->entityManager->flush();
        return $output;
    }

    /**
     * @param AuthCreds $authCreds
     * @param array $taskNames
     * @param array $params
     * @param null $taskDatetime
     * @param bool $force
     * @throws \Doctrine\DBAL\DBALException
     */
    public function createAndRunTasksForAUser(AuthCreds $authCreds, Array $taskNames = array(), Array $params = array(), $taskDatetime = null, bool $force = true)
    {
        if ($authCreds) {
            $this->handleCreateTasks($authCreds->getId(), $taskNames, $params, $taskDatetime, $force, null);
            $tasks = $this->entityManager->getRepository('App:Tasks')
                ->findBy(array(
                    'authCreds' => $authCreds,
                    'status' => TasksManager::STATUS_PENDING
                ));
            foreach ($tasks as $task) {
                //$this->tasksManager->runTask($task->getId());
            }
        }
    }

}