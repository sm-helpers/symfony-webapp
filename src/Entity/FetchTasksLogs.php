<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FetchTasksLogsRepository")
 */
class FetchTasksLogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    // add your own fields

    /**
     * @ORM\Column(type="array")
     */
    private $totalFollowers;


    /**
     * @ORM\Column(type="array")
     */
    private $newFollowers;


    /**
     * @ORM\Column(type="array")
     */
    private $totalFriends;


    /**
     * @ORM\Column(type="array")
     */
    private $newFriends;


    /**
     * @ORM\Column(type="array")
     */
    private $unfollowers;

    /**
     * @ORM\Column(type="array")
     */
    private $mutes;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $modifiedAt;

    /**
     * @var \App\Entity\TwitterAccounts
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TwitterAccounts", inversedBy="fetchTasksLogs", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tw_user_id", referencedColumnName="tw_user_id", onDelete="CASCADE")
     * })
     */
    private $twitterAccount;

    public function __construct()
    {
        $stats = array(
            'count' => 0,
            'success' => false,
            'error' => "Value is not set yet."
        );
        $this->setTotalFollowers($stats);
        $this->setTotalFriends($stats);
        $this->setNewFollowers($stats);
        $this->setNewFriends($stats);
        $this->setMutes($stats);
        $this->setUnfollowers($stats);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalFollowers
     *
     * @param array $totalFollowers
     *
     * @return FetchTasksLogs
     */
    public function setTotalFollowers($totalFollowers)
    {
        $this->totalFollowers = $totalFollowers;

        return $this;
    }

    /**
     * Get totalFollowers
     *
     * @return array
     */
    public function getTotalFollowers()
    {
        return $this->totalFollowers;
    }

    /**
     * Set newFollowers
     *
     * @param array $newFollowers
     *
     * @return FetchTasksLogs
     */
    public function setNewFollowers($newFollowers)
    {
        $this->newFollowers = $newFollowers;

        return $this;
    }

    /**
     * Get newFollowers
     *
     * @return array
     */
    public function getNewFollowers()
    {
        return $this->newFollowers;
    }

    /**
     * Set totalFriends
     *
     * @param array $totalFriends
     *
     * @return FetchTasksLogs
     */
    public function setTotalFriends($totalFriends)
    {
        $this->totalFriends = $totalFriends;

        return $this;
    }

    /**
     * Get totalFriends
     *
     * @return array
     */
    public function getTotalFriends()
    {
        return $this->totalFriends;
    }

    /**
     * Set newFriends
     *
     * @param array $newFriends
     *
     * @return FetchTasksLogs
     */
    public function setNewFriends($newFriends)
    {
        $this->newFriends = $newFriends;

        return $this;
    }

    /**
     * Get newFriends
     *
     * @return array
     */
    public function getNewFriends()
    {
        return $this->newFriends;
    }

    /**
     * Set unfollowers
     *
     * @param array $unfollowers
     *
     * @return FetchTasksLogs
     */
    public function setUnfollowers($unfollowers)
    {
        $this->unfollowers = $unfollowers;

        return $this;
    }

    /**
     * Get unfollowers
     *
     * @return array
     */
    public function getUnfollowers()
    {
        return $this->unfollowers;
    }

    /**
     * Set mutes
     *
     * @param array $mutes
     *
     * @return FetchTasksLogs
     */
    public function setMutes($mutes)
    {
        $this->mutes = $mutes;

        return $this;
    }

    /**
     * Get mutes
     *
     * @return array
     */
    public function getMutes()
    {
        return $this->mutes;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return FetchTasksLogs
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return FetchTasksLogs
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return FetchTasksLogs
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return FetchTasksLogs
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set twitterAccount
     *
     * @param \App\Entity\TwitterAccounts $twitterAccount
     *
     * @return FetchTasksLogs
     */
    public function setTwitterAccount(\App\Entity\TwitterAccounts $twitterAccount = null)
    {
        $this->twitterAccount = $twitterAccount;

        return $this;
    }

    /**
     * Get twitterAccount
     *
     * @return \App\Entity\TwitterAccounts
     */
    public function getTwitterAccount()
    {
        return $this->twitterAccount;
    }
}
