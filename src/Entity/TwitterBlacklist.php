<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TwitterBlacklist
 *
 * @ORM\Table(name="twitter_blacklist")
 * @ORM\Entity(repositoryClass="App\Repository\TwitterBlacklistRepository")
 */
class TwitterBlacklist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="auth_creds_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $authCredsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_user_id_x", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $twUserIdX;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint", columnDefinition="BIGINT AUTO_INCREMENT NOT NULL", unique=true)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $modifiedAt;

    /**
     * @var \App\Entity\AuthCreds
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\AuthCreds", inversedBy="myBlacklist", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="auth_creds_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $authCreds;

    /**
     * @var \App\Entity\TwitterAccounts
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TwitterAccounts", inversedBy="twitterBlacklist", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tw_user_id_x", referencedColumnName="tw_user_id", onDelete="CASCADE")
     * })
     */
    private $twitterAccount;


    /**
     * Set authCredsId
     *
     * @param integer $authCredsId
     *
     * @return TwitterBlacklist
     */
    public function setAuthCredsId($authCredsId)
    {
        $this->authCredsId = $authCredsId;

        return $this;
    }

    /**
     * Get authCredsId
     *
     * @return integer
     */
    public function getAuthCredsId()
    {
        return $this->authCredsId;
    }

    /**
     * Set twUserIdX
     *
     * @param integer $twUserIdX
     *
     * @return TwitterBlacklist
     */
    public function setTwUserIdX($twUserIdX)
    {
        $this->twUserIdX = $twUserIdX;

        return $this;
    }

    /**
     * Get twUserIdX
     *
     * @return integer
     */
    public function getTwUserIdX()
    {
        return $this->twUserIdX;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return TwitterBlacklist
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TwitterBlacklist
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return TwitterBlacklist
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set authCreds
     *
     * @param \App\Entity\AuthCreds $authCreds
     *
     * @return TwitterBlacklist
     */
    public function setAuthCreds(\App\Entity\AuthCreds $authCreds = null)
    {
        $this->authCreds = $authCreds;

        return $this;
    }

    /**
     * Get authCreds
     *
     * @return \App\Entity\AuthCreds
     */
    public function getAuthCreds()
    {
        return $this->authCreds;
    }

    /**
     * Set twitterAccount
     *
     * @param \App\Entity\TwitterAccounts $twitterAccount
     *
     * @return TwitterBlacklist
     */
    public function setTwitterAccount(\App\Entity\TwitterAccounts $twitterAccount = null)
    {
        $this->twitterAccount = $twitterAccount;

        return $this;
    }

    /**
     * Get twitterAccount
     *
     * @return \App\Entity\TwitterAccounts
     */
    public function getTwitterAccount()
    {
        return $this->twitterAccount;
    }
}
