<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TasksRepository")
 */
class Tasks
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $parameters;

    /**
     * @ORM\Column(type="string")
     */
    private $priority;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $modifiedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $taskDatetime;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startDatetime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $closeDatetime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastStartDatetime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastCloseDatetime;

    /**
     * @ORM\Column(type="integer")
     */
    private $errorCount;

    /**
     * @var \App\Entity\AuthCreds
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\AuthCreds", inversedBy="tasks", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="auth_creds_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $authCreds;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tasks
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parameters
     *
     * @param array $parameters
     *
     * @return Tasks
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Get parameters
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set priority
     *
     * @param string $priority
     *
     * @return Tasks
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Tasks
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return Tasks
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set taskDatetime
     *
     * @param \DateTime $taskDatetime
     *
     * @return Tasks
     */
    public function setTaskDatetime($taskDatetime)
    {
        $this->taskDatetime = $taskDatetime;

        return $this;
    }

    /**
     * Get taskDatetime
     *
     * @return \DateTime
     */
    public function getTaskDatetime()
    {
        return $this->taskDatetime;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Tasks
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set errorCount
     *
     * @param integer $errorCount
     *
     * @return Tasks
     */
    public function setErrorCount($errorCount)
    {
        $this->errorCount = $errorCount;

        return $this;
    }

    /**
     * Get errorCount
     *
     * @return integer
     */
    public function getErrorCount()
    {
        return $this->errorCount;
    }

    /**
     * Set startDatetime
     *
     * @param \DateTime $startDatetime
     *
     * @return Tasks
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * Get startDatetime
     *
     * @return \DateTime
     */
    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    /**
     * Set closeDatetime
     *
     * @param \DateTime $closeDatetime
     *
     * @return Tasks
     */
    public function setCloseDatetime($closeDatetime)
    {
        $this->closeDatetime = $closeDatetime;

        return $this;
    }

    /**
     * Get closeDatetime
     *
     * @return \DateTime
     */
    public function getCloseDatetime()
    {
        return $this->closeDatetime;
    }

    /**
     * Set lastStartDatetime
     *
     * @param \DateTime $lastStartDatetime
     *
     * @return Tasks
     */
    public function setLastStartDatetime($lastStartDatetime)
    {
        $this->lastStartDatetime = $lastStartDatetime;

        return $this;
    }

    /**
     * Get lastStartDatetime
     *
     * @return \DateTime
     */
    public function getLastStartDatetime()
    {
        return $this->lastStartDatetime;
    }

    /**
     * Set lastCloseDatetime
     *
     * @param \DateTime $lastCloseDatetime
     *
     * @return Tasks
     */
    public function setLastCloseDatetime($lastCloseDatetime)
    {
        $this->lastCloseDatetime = $lastCloseDatetime;

        return $this;
    }

    /**
     * Get lastCloseDatetime
     *
     * @return \DateTime
     */
    public function getLastCloseDatetime()
    {
        return $this->lastCloseDatetime;
    }

    /**
     * Set authCreds
     *
     * @param \App\Entity\AuthCreds $authCreds
     *
     * @return Tasks
     */
    public function setAuthCreds(\App\Entity\AuthCreds $authCreds = null)
    {
        $this->authCreds = $authCreds;

        return $this;
    }

    /**
     * Get authCreds
     *
     * @return \App\Entity\AuthCreds
     */
    public function getAuthCreds()
    {
        return $this->authCreds;
    }
}
