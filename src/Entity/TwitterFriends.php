<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TwitterFriends
 *
 * @ORM\Table(name="twitter_friends")
 * @ORM\Entity(repositoryClass="App\Repository\TwitterFriendsRepository")
 */
class TwitterFriends
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tw_user_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $twUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tw_user_id_x", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $twUserIdX;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint", columnDefinition="BIGINT AUTO_INCREMENT NOT NULL", unique=true)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $modifiedAt;

    /**
     * @var \App\Entity\TwitterAccounts
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TwitterAccounts", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tw_user_id", referencedColumnName="tw_user_id", onDelete="CASCADE")
     * })
     */
    private $parentAccount;

    /**
     * @var \App\Entity\TwitterAccounts
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TwitterAccounts", inversedBy="myFriends", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tw_user_id_x", referencedColumnName="tw_user_id", onDelete="CASCADE")
     * })
     */
    private $childAccount;


    /**
     * Set twUserId
     *
     * @param integer $twUserId
     *
     * @return TwitterFriends
     */
    public function setTwUserId($twUserId)
    {
        $this->twUserId = $twUserId;

        return $this;
    }

    /**
     * Get twUserId
     *
     * @return integer
     */
    public function getTwUserId()
    {
        return $this->twUserId;
    }

    /**
     * Set twUserIdX
     *
     * @param integer $twUserIdX
     *
     * @return TwitterFriends
     */
    public function setTwUserIdX($twUserIdX)
    {
        $this->twUserIdX = $twUserIdX;

        return $this;
    }

    /**
     * Get twUserIdX
     *
     * @return integer
     */
    public function getTwUserIdX()
    {
        return $this->twUserIdX;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return TwitterFriends
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TwitterFriends
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return TwitterFriends
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set parentAccount
     *
     * @param \App\Entity\TwitterAccounts $parentAccount
     *
     * @return TwitterFriends
     */
    public function setParentAccount(\App\Entity\TwitterAccounts $parentAccount = null)
    {
        $this->parentAccount = $parentAccount;

        return $this;
    }

    /**
     * Get parentAccount
     *
     * @return \App\Entity\TwitterAccounts
     */
    public function getParentAccount()
    {
        return $this->parentAccount;
    }

    /**
     * Set childAccount
     *
     * @param \App\Entity\TwitterAccounts $childAccount
     *
     * @return TwitterFriends
     */
    public function setChildAccount(\App\Entity\TwitterAccounts $childAccount = null)
    {
        $this->childAccount = $childAccount;

        return $this;
    }

    /**
     * Get childAccount
     *
     * @return \App\Entity\TwitterAccounts
     */
    public function getChildAccount()
    {
        return $this->childAccount;
    }
}
