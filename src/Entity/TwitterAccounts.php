<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * TwitterAccounts
 *
 * @ORM\Table(name="twitter_accounts", indexes={@Index(name="tw_username_idx", columns={"tw_username"})})
 * @ORM\Entity(repositoryClass="App\Repository\TwitterAccountsRepository")
 */
class TwitterAccounts
{
    /**
     * ################################# IMPORTANT
     * @internal Doctrine does not support mapping entities on keys other than primary keys.
     * @internal So, it was necessary to make tw_user_id primary key and define "id" as non-primary but unique, auto_increment.
     * @internal Please contact Kamran Akram <kamranakram1000@live.com> before making any change to this schema.
     */
    /**
     * @var integer
     *
     * @ORM\Column(name="tw_user_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $twUserId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $twUsername;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint", columnDefinition="BIGINT AUTO_INCREMENT NOT NULL", unique=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $twName;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $twLocation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $twCreatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $modifiedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=190, nullable=true)
     */
    private $twLang;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $twProtected;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=190, nullable=true)
     */
    private $twTimezone;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $twFollowersCount;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $twFriendsCount;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $twStatusesCount;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $twGeoEnabled;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $twProfileImage;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $twVerified;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $isDisabled;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false, options={"default" : 0})
     */
    private $updateRetries;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterFollowers", mappedBy="childAccount")
     */
    private $myFollowers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterFriends", mappedBy="childAccount")
     */
    private $myFriends;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterMutes", mappedBy="childAccount")
     */
    private $myMutes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterUnfollowers", mappedBy="childAccount")
     */
    private $myUnfollowers;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterWhitelist", mappedBy="twitterAccount")
     */
    private $twitterWhitelist;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterBlacklist", mappedBy="twitterAccount")
     */
    private $twitterBlacklist;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\FetchTasksLogs", mappedBy="twitterAccount")
     */
    private $fetchTasksLogs;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->myFollowers = new ArrayCollection();
        $this->myFriends = new ArrayCollection();
        $this->myMutes = new ArrayCollection();
        $this->myUnfollowers = new ArrayCollection();
        $this->twitterWhitelist = new ArrayCollection();
        $this->twitterBlacklist = new ArrayCollection();
        $this->fetchTasksLogs = new FetchTasksLogs();
    }


    /**
     * Set twUserId
     *
     * @param integer $twUserId
     *
     * @return TwitterAccounts
     */
    public function setTwUserId($twUserId)
    {
        $this->twUserId = $twUserId;

        return $this;
    }

    /**
     * Get twUserId
     *
     * @return integer
     */
    public function getTwUserId()
    {
        return $this->twUserId;
    }

    /**
     * Set twUsername
     *
     * @param string $twUsername
     *
     * @return TwitterAccounts
     */
    public function setTwUsername($twUsername)
    {
        $this->twUsername = $twUsername;

        return $this;
    }

    /**
     * Get twUsername
     *
     * @return string
     */
    public function getTwUsername()
    {
        return $this->twUsername;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return TwitterAccounts
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set twName
     *
     * @param string $twName
     *
     * @return TwitterAccounts
     */
    public function setTwName($twName)
    {
        $this->twName = $twName;

        return $this;
    }

    /**
     * Get twName
     *
     * @return string
     */
    public function getTwName()
    {
        return $this->twName;
    }

    /**
     * Set twLocation
     *
     * @param string $twLocation
     *
     * @return TwitterAccounts
     */
    public function setTwLocation($twLocation)
    {
        $this->twLocation = $twLocation;

        return $this;
    }

    /**
     * Get twLocation
     *
     * @return string
     */
    public function getTwLocation()
    {
        return $this->twLocation;
    }

    /**
     * Set twCreatedAt
     *
     * @param \DateTime $twCreatedAt
     *
     * @return TwitterAccounts
     */
    public function setTwCreatedAt($twCreatedAt)
    {
        $this->twCreatedAt = $twCreatedAt;

        return $this;
    }

    /**
     * Get twCreatedAt
     *
     * @return \DateTime
     */
    public function getTwCreatedAt()
    {
        return $this->twCreatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TwitterAccounts
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return TwitterAccounts
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set twLang
     *
     * @param string $twLang
     *
     * @return TwitterAccounts
     */
    public function setTwLang($twLang)
    {
        $this->twLang = $twLang;

        return $this;
    }

    /**
     * Get twLang
     *
     * @return string
     */
    public function getTwLang()
    {
        return $this->twLang;
    }

    /**
     * Set twProtected
     *
     * @param integer $twProtected
     *
     * @return TwitterAccounts
     */
    public function setTwProtected($twProtected)
    {
        $this->twProtected = $twProtected;

        return $this;
    }

    /**
     * Get twProtected
     *
     * @return integer
     */
    public function getTwProtected()
    {
        return $this->twProtected;
    }

    /**
     * Set twTimezone
     *
     * @param string $twTimezone
     *
     * @return TwitterAccounts
     */
    public function setTwTimezone($twTimezone)
    {
        $this->twTimezone = $twTimezone;

        return $this;
    }

    /**
     * Get twTimezone
     *
     * @return string
     */
    public function getTwTimezone()
    {
        return $this->twTimezone;
    }

    /**
     * Set twFollowersCount
     *
     * @param integer $twFollowersCount
     *
     * @return TwitterAccounts
     */
    public function setTwFollowersCount($twFollowersCount)
    {
        $this->twFollowersCount = $twFollowersCount;

        return $this;
    }

    /**
     * Get twFollowersCount
     *
     * @return integer
     */
    public function getTwFollowersCount()
    {
        return $this->twFollowersCount;
    }

    /**
     * Set twFriendsCount
     *
     * @param integer $twFriendsCount
     *
     * @return TwitterAccounts
     */
    public function setTwFriendsCount($twFriendsCount)
    {
        $this->twFriendsCount = $twFriendsCount;

        return $this;
    }

    /**
     * Get twFriendsCount
     *
     * @return integer
     */
    public function getTwFriendsCount()
    {
        return $this->twFriendsCount;
    }

    /**
     * Set twStatusesCount
     *
     * @param integer $twStatusesCount
     *
     * @return TwitterAccounts
     */
    public function setTwStatusesCount($twStatusesCount)
    {
        $this->twStatusesCount = $twStatusesCount;

        return $this;
    }

    /**
     * Get twStatusesCount
     *
     * @return integer
     */
    public function getTwStatusesCount()
    {
        return $this->twStatusesCount;
    }

    /**
     * Set twGeoEnabled
     *
     * @param integer $twGeoEnabled
     *
     * @return TwitterAccounts
     */
    public function setTwGeoEnabled($twGeoEnabled)
    {
        $this->twGeoEnabled = $twGeoEnabled;

        return $this;
    }

    /**
     * Get twGeoEnabled
     *
     * @return integer
     */
    public function getTwGeoEnabled()
    {
        return $this->twGeoEnabled;
    }

    /**
     * Set twProfileImage
     *
     * @param string $twProfileImage
     *
     * @return TwitterAccounts
     */
    public function setTwProfileImage($twProfileImage)
    {
        $this->twProfileImage = $twProfileImage;

        return $this;
    }

    /**
     * Get twProfileImage
     *
     * @return string
     */
    public function getTwProfileImage()
    {
        return $this->twProfileImage;
    }

    /**
     * Set twVerified
     *
     * @param integer $twVerified
     *
     * @return TwitterAccounts
     */
    public function setTwVerified($twVerified)
    {
        $this->twVerified = $twVerified;

        return $this;
    }

    /**
     * Get twVerified
     *
     * @return integer
     */
    public function getTwVerified()
    {
        return $this->twVerified;
    }

    /**
     * Set isDisabled
     *
     * @param integer $isDisabled
     *
     * @return TwitterAccounts
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled
     *
     * @return integer
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * Set updateRetries
     *
     * @param integer $updateRetries
     *
     * @return TwitterAccounts
     */
    public function setUpdateRetries(int $updateRetries)
    {
        $this->updateRetries = $updateRetries;
        return $this;
    }

    /**
     * Get updateRetries
     *
     * @return int
     */
    public function getUpdateRetries()
    {
        return $this->updateRetries;
    }

    /**
     * Add myFollower
     *
     * @param \App\Entity\TwitterFollowers $myFollower
     *
     * @return TwitterAccounts
     */
    public function addMyFollower(\App\Entity\TwitterFollowers $myFollower)
    {
        $this->myFollowers[] = $myFollower;

        return $this;
    }

    /**
     * Remove myFollower
     *
     * @param \App\Entity\TwitterFollowers $myFollower
     */
    public function removeMyFollower(\App\Entity\TwitterFollowers $myFollower)
    {
        $this->myFollowers->removeElement($myFollower);
    }

    /**
     * Get myFollowers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyFollowers()
    {
        return $this->myFollowers;
    }

    /**
     * Add myFriend
     *
     * @param \App\Entity\TwitterFriends $myFriend
     *
     * @return TwitterAccounts
     */
    public function addMyFriend(\App\Entity\TwitterFriends $myFriend)
    {
        $this->myFriends[] = $myFriend;

        return $this;
    }

    /**
     * Remove myFriend
     *
     * @param \App\Entity\TwitterFriends $myFriend
     */
    public function removeMyFriend(\App\Entity\TwitterFriends $myFriend)
    {
        $this->myFriends->removeElement($myFriend);
    }

    /**
     * Get myFriends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyFriends()
    {
        return $this->myFriends;
    }

    /**
     * Add myMute
     *
     * @param \App\Entity\TwitterMutes $myMute
     *
     * @return TwitterAccounts
     */
    public function addMyMute(\App\Entity\TwitterMutes $myMute)
    {
        $this->myMutes[] = $myMute;

        return $this;
    }

    /**
     * Remove myMute
     *
     * @param \App\Entity\TwitterMutes $myMute
     */
    public function removeMyMute(\App\Entity\TwitterMutes $myMute)
    {
        $this->myMutes->removeElement($myMute);
    }

    /**
     * Get myMutes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyMutes()
    {
        return $this->myMutes;
    }

    /**
     * Add myUnfollower
     *
     * @param \App\Entity\TwitterUnfollowers $myUnfollower
     *
     * @return TwitterAccounts
     */
    public function addMyUnfollower(\App\Entity\TwitterUnfollowers $myUnfollower)
    {
        $this->myUnfollowers[] = $myUnfollower;

        return $this;
    }

    /**
     * Remove myUnfollower
     *
     * @param \App\Entity\TwitterUnfollowers $myUnfollower
     */
    public function removeMyUnfollower(\App\Entity\TwitterUnfollowers $myUnfollower)
    {
        $this->myUnfollowers->removeElement($myUnfollower);
    }

    /**
     * Get myUnfollowers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyUnfollowers()
    {
        return $this->myUnfollowers;
    }

    /**
     * Add twitterWhitelist
     *
     * @param \App\Entity\TwitterWhitelist $twitterWhitelist
     *
     * @return TwitterAccounts
     */
    public function addTwitterWhitelist(\App\Entity\TwitterWhitelist $twitterWhitelist)
    {
        $this->twitterWhitelist[] = $twitterWhitelist;

        return $this;
    }

    /**
     * Remove twitterWhitelist
     *
     * @param \App\Entity\TwitterWhitelist $twitterWhitelist
     */
    public function removeTwitterWhitelist(\App\Entity\TwitterWhitelist $twitterWhitelist)
    {
        $this->twitterWhitelist->removeElement($twitterWhitelist);
    }

    /**
     * Get twitterWhitelist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTwitterWhitelist()
    {
        return $this->twitterWhitelist;
    }

    /**
     * Add twitterBlacklist
     *
     * @param \App\Entity\TwitterBlacklist $twitterBlacklist
     *
     * @return TwitterAccounts
     */
    public function addTwitterBlacklist(\App\Entity\TwitterBlacklist $twitterBlacklist)
    {
        $this->twitterBlacklist[] = $twitterBlacklist;

        return $this;
    }

    /**
     * Remove twitterBlacklist
     *
     * @param \App\Entity\TwitterBlacklist $twitterBlacklist
     */
    public function removeTwitterBlacklist(\App\Entity\TwitterBlacklist $twitterBlacklist)
    {
        $this->twitterBlacklist->removeElement($twitterBlacklist);
    }

    /**
     * Get twitterBlacklist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTwitterBlacklist()
    {
        return $this->twitterBlacklist;
    }

    /**
     * Add fetchTasksLog
     *
     * @param \App\Entity\Tasks $fetchTasksLog
     *
     * @return TwitterAccounts
     */
    public function addFetchTasksLog(\App\Entity\Tasks $fetchTasksLog)
    {
        $this->fetchTasksLogs[] = $fetchTasksLog;

        return $this;
    }

    /**
     * Remove fetchTasksLog
     *
     * @param \App\Entity\Tasks $fetchTasksLog
     */
    public function removeFetchTasksLog(\App\Entity\Tasks $fetchTasksLog)
    {
        $this->fetchTasksLogs->removeElement($fetchTasksLog);
    }

    /**
     * Get fetchTasksLogs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFetchTasksLogs()
    {
        return $this->fetchTasksLogs;
    }
}
