<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AuthCreds
 *
 * @ORM\Table(name="auth_creds")
 * @ORM\Entity(repositoryClass="App\Repository\AuthCredsRepository")
 */
class AuthCreds
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=190)
     */
    private $twOauthToken;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=190)
     */
    private $twOauthTokenSecret;

    /**
     * @var integer
     *
     * @ORM\Column(type="bigint")
     */
    private $twUserId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=190)
     */
    private $twUsername;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint")
     */
    private $enabled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $modifiedAt;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $platformId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUsedAt;

    /**
     * @var \App\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="authCreds")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $user;

    /**
     * @var \App\Entity\Platforms
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Platforms", inversedBy="authCreds")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="platform_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $platform;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterWhitelist", mappedBy="authCreds")
     */
    private $myWhitelist;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TwitterBlacklist", mappedBy="authCreds")
     */
    private $myBlacklist;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Tasks", mappedBy="authCreds")
     */
    private $tasks;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->myWhitelist = new ArrayCollection();
        $this->myBlacklist = new ArrayCollection();

        $this->tasks = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return AuthCreds
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set twOauthToken
     *
     * @param string $twOauthToken
     *
     * @return AuthCreds
     */
    public function setTwOauthToken($twOauthToken)
    {
        $this->twOauthToken = $twOauthToken;

        return $this;
    }

    /**
     * Get twOauthToken
     *
     * @return string
     */
    public function getTwOauthToken()
    {
        return $this->twOauthToken;
    }

    /**
     * Set twOauthTokenSecret
     *
     * @param string $twOauthTokenSecret
     *
     * @return AuthCreds
     */
    public function setTwOauthTokenSecret($twOauthTokenSecret)
    {
        $this->twOauthTokenSecret = $twOauthTokenSecret;

        return $this;
    }

    /**
     * Get twOauthTokenSecret
     *
     * @return string
     */
    public function getTwOauthTokenSecret()
    {
        return $this->twOauthTokenSecret;
    }

    /**
     * Set twUserId
     *
     * @param integer $twUserId
     *
     * @return AuthCreds
     */
    public function setTwUserId($twUserId)
    {
        $this->twUserId = $twUserId;

        return $this;
    }

    /**
     * Get twUserId
     *
     * @return integer
     */
    public function getTwUserId()
    {
        return $this->twUserId;
    }

    /**
     * Set twUsername
     *
     * @param string $twUsername
     *
     * @return AuthCreds
     */
    public function setTwUsername($twUsername)
    {
        $this->twUsername = $twUsername;

        return $this;
    }

    /**
     * Get twUsername
     *
     * @return string
     */
    public function getTwUsername()
    {
        return $this->twUsername;
    }

    /**
     * Set enabled
     *
     * @param integer $enabled
     *
     * @return AuthCreds
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return integer
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AuthCreds
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     *
     * @return AuthCreds
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set platformId
     *
     * @param integer $platformId
     *
     * @return AuthCreds
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;

        return $this;
    }

    /**
     * Get platformId
     *
     * @return integer
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * Set lastUsedAt
     *
     * @param \DateTime $lastUsedAt
     *
     * @return AuthCreds
     */
    public function setLastUsedAt($lastUsedAt)
    {
        $this->lastUsedAt = $lastUsedAt;

        return $this;
    }

    /**
     * Get lastUsedAt
     *
     * @return \DateTime
     */
    public function getLastUsedAt()
    {
        return $this->lastUsedAt;
    }

    /**
     * Set user
     *
     * @param \App\Entity\Users $user
     *
     * @return AuthCreds
     */
    public function setUser(\App\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set platform
     *
     * @param \App\Entity\Platforms $platform
     *
     * @return AuthCreds
     */
    public function setPlatform(\App\Entity\Platforms $platform = null)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return \App\Entity\Platforms
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Add myWhitelist
     *
     * @param \App\Entity\TwitterWhitelist $myWhitelist
     *
     * @return AuthCreds
     */
    public function addMyWhitelist(\App\Entity\TwitterWhitelist $myWhitelist)
    {
        $this->myWhitelist[] = $myWhitelist;

        return $this;
    }

    /**
     * Remove myWhitelist
     *
     * @param \App\Entity\TwitterWhitelist $myWhitelist
     */
    public function removeMyWhitelist(\App\Entity\TwitterWhitelist $myWhitelist)
    {
        $this->myWhitelist->removeElement($myWhitelist);
    }

    /**
     * Get myWhitelist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyWhitelist()
    {
        return $this->myWhitelist;
    }

    /**
     * Add myBlacklist
     *
     * @param \App\Entity\TwitterBlacklist $myBlacklist
     *
     * @return AuthCreds
     */
    public function addMyBlacklist(\App\Entity\TwitterBlacklist $myBlacklist)
    {
        $this->myBlacklist[] = $myBlacklist;

        return $this;
    }

    /**
     * Remove myBlacklist
     *
     * @param \App\Entity\TwitterBlacklist $myBlacklist
     */
    public function removeMyBlacklist(\App\Entity\TwitterBlacklist $myBlacklist)
    {
        $this->myBlacklist->removeElement($myBlacklist);
    }

    /**
     * Get myBlacklist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMyBlacklist()
    {
        return $this->myBlacklist;
    }

    /**
     * Add task
     *
     * @param \App\Entity\Tasks $task
     *
     * @return AuthCreds
     */
    public function addTask(\App\Entity\Tasks $task)
    {
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param \App\Entity\Tasks $task
     */
    public function removeTask(\App\Entity\Tasks $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTasks()
    {
        return $this->tasks;
    }
}
