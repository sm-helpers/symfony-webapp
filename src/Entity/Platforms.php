<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Platforms
 *
 * @ORM\Table(name="platforms")
 * @ORM\Entity(repositoryClass="App\Repository\PlatformsRepository")
 */
class Platforms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=191, unique=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint")
     */
    private $isActive;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\AuthCreds", mappedBy="platform")
     */
    private $authCreds;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->authCreds = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Platforms
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param integer $isActive
     *
     * @return Platforms
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return integer
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add authCred
     *
     * @param \App\Entity\AuthCreds $authCred
     *
     * @return Platforms
     */
    public function addAuthCred(\App\Entity\AuthCreds $authCred)
    {
        $this->authCreds[] = $authCred;

        return $this;
    }

    /**
     * Remove authCred
     *
     * @param \App\Entity\AuthCreds $authCred
     */
    public function removeAuthCred(\App\Entity\AuthCreds $authCred)
    {
        $this->authCreds->removeElement($authCred);
    }

    /**
     * Get authCreds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthCreds()
    {
        return $this->authCreds;
    }
}
