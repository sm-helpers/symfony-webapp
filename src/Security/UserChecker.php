<?php
namespace App\Security;
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: UserChecker.php
 * Date: 10/29/17
 * Time: 2:43 AM
 */

use App\CustomExceptions\InvalidCaptchaException;
use App\Entity\Users as DashboardUser;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class UserChecker implements UserCheckerInterface
{
    protected $request;
    protected $securityHelpers;
    protected $secretKey;
    protected $websiteUrl;
    protected $enableLoginRecaptcha;

    public function __construct(RequestStack $requestStack, SecurityHelpers $securityHelpers, $secretKey, $websiteUrl, $enableLoginRecaptcha)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->securityHelpers = $securityHelpers;
        $this->secretKey = $secretKey;
        $this->websiteUrl = $websiteUrl;
        $this->enableLoginRecaptcha = $enableLoginRecaptcha;
    }

    public function checkPreAuth(UserInterface $user)
    {
        // get referer
        $referer = $this->request->headers->get('referer');

        // create login URLs for prod and dev
        $loginUrl = $this->websiteUrl . "/login";

        // Check captcha only if the request is for login and login captcha is enabled in config
        if($this->request->isMethod("POST") && ($referer == $loginUrl) && $this->enableLoginRecaptcha){
            if(!$this->securityHelpers->captchaverify($this->request->get('g-recaptcha-response'), $this->secretKey)){
                throw new InvalidCaptchaException('Request not verified by recaptcha.');
            }
        }

        if (!$user instanceof DashboardUser) {
            return;
        }


    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof DashboardUser) {
            return;
        }

        // Check if user is disabled
        if (!$user->isEnabled()) {
            throw new DisabledException('Account is not activated. Check your email to activate.');
        }
    }
}
 