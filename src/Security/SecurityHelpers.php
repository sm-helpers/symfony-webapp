<?php
/**
 * Author: Kamran Akram <kamranakram1000@live.com>
 * File: SecurityHelpers.php
 * Date: 10/31/17
 * Time: 2:30 AM
 */


namespace App\Security;


class SecurityHelpers
{
    protected $enableRecaptcha;

    public function __construct($enableRecaptcha)
    {
        $this->enableRecaptcha = $enableRecaptcha;
    }

    public function captchaverify($recaptcha, $secret_key){
        if(!$this->enableRecaptcha){
            return true;
        }
        $recaptcha_url = "https://www.google.com/recaptcha/api/siteverify";
        $verification_request = curl_init();
        curl_setopt($verification_request, CURLOPT_URL, $recaptcha_url);
        curl_setopt($verification_request, CURLOPT_HEADER, 0);
        curl_setopt($verification_request, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($verification_request, CURLOPT_POST, true);
        curl_setopt($verification_request, CURLOPT_POSTFIELDS, array(
            "secret"=>$secret_key,"response"=>$recaptcha));
        $verification_response = curl_exec($verification_request);
        curl_close($verification_request);
        $verification = json_decode($verification_response);

        return $verification->success;
    }

}