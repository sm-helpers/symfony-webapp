<?php
/**
 * Author: Kamran Akram
 * Author URI: http://kamranakram.me
 * Author Email: kamranakram1000@live.com
 * Created at: 2/18/18, 2:14 AM
 */

namespace App\EventSubscriber;


use App\Controller\Dashboard\Interfaces\TwitterPreInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class PreEventsListener implements EventSubscriberInterface
{
    private $container;
    private $tokenStorage;

    public function __construct(ContainerInterface $container, TokenStorageInterface $tokenStorage)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::RESPONSE => 'onKernelResponse',
            SecurityEvents::SWITCH_USER => 'onSwitchUser',
        );
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        // Ignore if its embedded controllers
        if (!$event->isMasterRequest()) {
            return;
        }

        $controller = $event->getController();
        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }
        $request = $this->container->get('request_stack');
        // Ignore if its Ajax hit
        if ($request->getCurrentRequest()->isXmlHttpRequest()) {
            return;
        }

        $this->preCheckSelectedAccount($event);
        $this->setLastLoggedInDatetime();


    }

    public function onSwitchUser(SwitchUserEvent $event)
    {
        $event->getRequest()->getSession()->remove('selectedAccountTwUserId');
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {

    }

    private function setLastLoggedInDatetime()
    {
        $authorizationChecker = $this->container->get('security.authorization_checker');
        if (null !== $this->container->get('security.token_storage')->getToken()) {
            if ($authorizationChecker->isGranted('ROLE_USER') && !$authorizationChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
                $entityManager = $this->container->get('doctrine.orm.entity_manager');
                $user = $this->tokenStorage->getToken()->getUser();
                $user = $entityManager->getRepository('App:Users')
                    ->find($user->getId());
                $lastLoggedInDatetime = $user->getLastLoginDatetime();

                $diff = $lastLoggedInDatetime->diff(new \DateTime());

                // Ignore if last logged in date in equal to today
                if ($diff->format('%a') !== '0') {
                    $user->setLastLoginDatetime(new \DateTime());
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
            }
        }
    }

    private function preCheckSelectedAccount(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        $session = $this->container->get('session');
        $ignoredActions = array("oauthAction", "callbackAction");
        if ($controller[0] instanceof TwitterPreInterface && !in_array($controller[1], $ignoredActions)) {
            $authorizationChecker = $this->container->get('security.authorization_checker');
            if ($authorizationChecker->isGranted('ROLE_USER')) {
                $entityManager = $this->container->get('doctrine.orm.entity_manager');
                $user = $this->tokenStorage->getToken()->getUser();
                $authCreds = $entityManager->getRepository('App:AuthCreds')
                    ->createQueryBuilder('authCreds')
                    ->select('authCreds')
                    ->where('authCreds.userId = :user_id')
                    ->andWhere('authCreds.enabled = :enabled')
                    ->setParameter('user_id', $user->getId())
                    ->setParameter('enabled', false)
                    ->getQuery()
                    ->getResult();
                if ($authCreds) { // If any auth creds is disabled, redirect to connected account page
                    $dashboardSettingsURL = $this->container->get('router')->generate('dashboard_settings', array(
                        '0' => 'tab',
                        '1' => 'connected-accounts'
                    ));
                    $event->setController(function () use ($dashboardSettingsURL) {
                        return new RedirectResponse($dashboardSettingsURL);
                    });
                    $session->getFlashBag()->add("error", "Twitter account needs authentication, please re-authenticate your account to continue.");
                } else {
                    $authCreds = $entityManager->getRepository('App:AuthCreds')
                        ->createQueryBuilder('authCreds')
                        ->select('authCreds')
                        ->where('authCreds.userId = :user_id')
                        ->setParameter('user_id', $user->getId())
                        ->getQuery()
                        ->getResult();
                    if (count($authCreds) == 1) {
                        if (!$session->has('selectedAccountTwUserId')) {
                            $session->set('selectedAccountTwUserId', $authCreds[0]->getTwUserId());
                            $session->getFlashBag()->add('success', "@" . $authCreds[0]->getTwUsername() . " has been selected.");
                        }
                    } else if (count($authCreds) > 1) {
                        if (!$session->has('selectedAccountTwUserId') && !$session->getFlashBag()->has("noSelectedAccountWarning")) {
                            $twitterIndexURL = $this->container->get('router')->generate('twitter_index');
                            $event->setController(function () use ($twitterIndexURL) {
                                return new RedirectResponse($twitterIndexURL);
                            });
                            $session->getFlashBag()->add("noSelectedAccountWarning", "Please select an account. Click here");
                        }
                    } else {
                        $dashboardSettingsURL = $this->container->get('router')->generate('dashboard_settings', array(
                            '0' => 'tab',
                            '1' => 'connected-accounts'
                        ));
                        $event->setController(function () use ($dashboardSettingsURL) {
                            return new RedirectResponse($dashboardSettingsURL);
                        });
                        $session->getFlashBag()->add("info", "Please connect a twitter account to continue.");
                    }
                }
            }
        }
    }
}