<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
//        return $this->render('default/index.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
//        ]);
        return $this->render('DefaultController/index.html.twig');
    }

    /**
     * @Route("/privacy-policy", name="privacy_policy")
     */
    public function privacyPolicyAction(Request $request)
    {
        return $this->render('DefaultController/privacy_policy.html.twig');
    }

    /**
     * @Route("/pp", name="privacy_policy_raw")
     */
    public function pPRawAction(Request $request)
    {
        return $this->render('DefaultController/privacy_policy_raw.html.twig');
    }

    /**
     * @Route("/terms-conditions", name="terms_conditions")
     */
    public function termsConditionsAction(Request $request)
    {
        return $this->render('DefaultController/terms_conditions.html.twig');
    }

    /**
     * @Route("/tc", name="terms_conditions_raw")
     */
    public function tCRawAction(Request $request)
    {
        return $this->render('DefaultController/terms_conditions_raw.html.twig');
    }
}
