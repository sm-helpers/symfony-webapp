<?php

namespace App\Controller\Admin;

use App\Utils\GeneralHelpers;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     */
    public function index()
    {
        $minus24HoursDatetime = (new \DateTime())->modify('-24 hours');

        $allUsers = $this->getDoctrine()->getRepository('App:Users')
            ->countTotalUsers();
        $last24HoursUsers = $this->getDoctrine()->getRepository('App:Users')
            ->countTotalUsers($minus24HoursDatetime);

        $activatedAllUsers = $this->getDoctrine()->getRepository('App:Users')
            ->countTotalUsers(null, 1);
        $last24HoursActivatedUsers = $this->getDoctrine()->getRepository('App:Users')
            ->countTotalUsers($minus24HoursDatetime, 1);

        $notActivatedAllUsers = $this->getDoctrine()->getRepository('App:Users')
            ->countTotalUsers(null, 0);

        $last24HoursLoggedInUsers = $this->getDoctrine()->getRepository('App:Users')
            ->countTotalUsers(null, null, $minus24HoursDatetime);

        return $this->render('Admin/AdminController/index.html.twig', array(
            'allUsers' => $allUsers,
            'last24HoursUsers' => $last24HoursUsers,
            'last24HoursActivatedUsers' => $last24HoursActivatedUsers,
            'activatedAllUsers' => $activatedAllUsers,
            'last24HoursLoggedInUsers' => $last24HoursLoggedInUsers,
            'notActivatedAllUsers' => $notActivatedAllUsers
        ));
    }

    /**
     * @Route("/users", name="admin_site_users")
     */
    public function usersAction()
    {
        return $this->render('Admin/AdminController/users.html.twig', array());
    }

    /**
     * @Route("/users-ajax", name="admin_site_users_ajax")
     */
    public function usersAjaxAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $startTime = microtime(true);

        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        if ($order[0]['column'] == 3) {
            $orderCol = "users.createdAt";
        } else if ($order[0]['column'] == 6) {

            $orderCol = "users.lastLoginDatetime";
        }
        $orderDir = $order[0]['dir'];

        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $userActivated = $request->get('user_activated') == "true" ? true : false;
        $notUserActivated = $request->get('not_user_activated') == "true" ? true : false;

        $twitterAccounts = $this->getDoctrine()->getRepository('App:Users');
        $queryBuilder = $twitterAccounts->createQueryBuilder('users');

        $query = $queryBuilder->leftJoin('users.authCreds', 'authCreds')
            ->select('users')
            ->addSelect('authCreds');

        if ($search) {
            $query = $query->andWhere("CONCAT(users.firstName,' ',users.lastName) LIKE :search 
            OR authCreds.twUsername LIKE :search OR users.email LIKE :search")
                ->setParameter(':search', '%' . $search . '%');
        }

        if ($userActivated) {
            $query->andWhere("users.activated = 1");
        } else if ($notUserActivated) {
            $query->andWhere("users.activated = 0");
        }

        if ($orderCol) {
            $query->orderBy($orderCol, $orderDir);
        }
        $filteredRecords = clone $query;
        $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(users.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:Users')
            ->createQueryBuilder('users')
            ->select('count(users.id) as total')
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $connectedAccounts = array();
            foreach ($result['authCreds'] as $authCred) {
                $connectedAccounts[] = $authCred['twUsername'];
            }
            $resultArray[] = array(
                "user_id" => $result['id'],
                'connected_accounts' => $connectedAccounts,
                'email' => $result['email'],
                'activated' => $result['activated'],
                'last_login' => GeneralHelpers::time_elapsed_string($result['lastLoginDatetime']->format('Y-m-d H:i:s')),
                'last_login_datetime' => $result['lastLoginDatetime']->format('j M Y g:i a'),
                'last_login_timestamp' => $result['lastLoginDatetime']->getTimestamp(),
                'name' => $result['firstName'] . " " . $result['lastName'],
                'modified_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'created_at' => date_format($result['createdAt'], 'j M Y'),
                'actions' => "",
            );
        }
        $endTime = microtime(true);
        $performance = array(
            "start" => $startTime,
            "end" => $endTime,
            "time" => $endTime - $startTime,
            "memory" => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true)),
        );

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray,
            'debug' => array(
                "performance" => $performance
            )
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }
}
