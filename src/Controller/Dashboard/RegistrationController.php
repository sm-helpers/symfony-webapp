<?php

namespace App\Controller\Dashboard;

use App\Entity\Users;
use App\Form\UserRegistration;
use App\Security\SecurityHelpers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, SecurityHelpers $securityHelpers)
    {
        if($this->is_logged_in()){
            return $this->redirectToRoute("dashboard_home");
        }

        if (!$this->getParameter('allow_user_registration')) {
            $this->addFlash('warning', $this->getParameter('reject_user_registration_msg'));
        }
        // build the form
        $user = new Users();
        $form = $this->createForm(UserRegistration::class, $user);

        // handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$request->get('acceptPolicies') == true) {
                $this->addFlash("error", "You must accept our terms and conditions.");
                return $this->render(
                    'RegistrationController/register.html.twig',
                    array('form' => $form->createView())
                );
            }
            if($securityHelpers->captchaverify($request->get('g-recaptcha-response'), $this->getParameter('recaptcha_secret'))) {

                // Check if email already exists
                $email_exists = $this->getDoctrine()
                    ->getRepository("App:Users")
                    ->findOneByEmail($user->getEmail());
                if ($email_exists) {
                    $this->addFlash("error", "Email already registered. Please login to access your account.");
                }
                /** Enable beta version check for limited users >> */
//                if (!$this->isTestingUser($user->getEmail())) {
//                    $this->addFlash("error", "App is currently in beta version, not available for public. If you still want access, please request at info@managetweeps.com");
//                    return $this->render(
//                        'RegistrationController/register.html.twig',
//                        array('form' => $form->createView())
//                    );
//                }
                /** << */
                if ($email_exists || !$this->getParameter('allow_user_registration')) {
                    return $this->render(
                        'RegistrationController/register.html.twig',
                        array('form' => $form->createView())
                    );
                }


                // Encode the password (you could also do this via Doctrine listener)
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $activationKey = base64_encode(random_bytes(40) . time());
                $createdAt = new \DateTime();


                $user->setActivationKey($activationKey);
                $user->setPassword($password);
                $user->setCreatedAt($createdAt);
                $user->setModifiedAt($createdAt);
                $user->setRole("ROLE_USER");
                $user->setActivated("0");

                // save the Users!
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();


                // set a "flash" success message for the user
                $this->addFlash("success", "Account Created Successfully.");

                // send account activation email to user
                $this->sendActivationEmail($user->getId());

                return $this->redirectToRoute('security_login');
            } else {
                $this->addFlash('warning', 'Bad request, verify you are human by checking the captcha.');
            }
        }

        return $this->render(
            'RegistrationController/register.html.twig',
            array('form' => $form->createView())
        );
    }

    private function isTestingUser($email)
    {
        $email = strtolower($email);
        $testing_users = array(
            'kamranakram1000@gmail.com',
            'kamranakram1000@live.com',
            'kamranakram@kamranakram.me',
            'kamranakram1000@gmail.com',
            'iihtisham@outlook.com',
            'arsimch9337@gmail.com',
            'taqdeesashraf@gmail.com',
            'rida.nayyab.98@gmail.com',
            'saharghazali@hotmail.com',
            'fahadshakoor95@gmail.com',
            'Fatimasajjad14@yahoo.com',
            'ranameezanhaider5@gmail.com',
            'haniaafatima12@gmail.com',
            'Mohammadqamarabbas@gmail.com',
            'yaqoobmian27@gmail.com',
            'arsalchaudhry7@yahoo.com',
            'nazishghafoor7@gmail.com',
            'Harishanif821@gmail.com',
            'Rabiyaafzaal97@gmail.com',
            'huzaifaehsan123@gmail.com',
            'qalbehassan140@gmail.com',
            'ranarashig4567@gmail.com',
            'Arizamin41@gmail.com',
            'nimrahskp@gmail.com',
            'samreenjaved133@gmail.com',
            'mirzanida555@gmail.com',
            'Arslch217@gmail.com',
            'syedatrimzi786@gmail.com',
            'Jessicaabeer7@gmail.com',
            'Sumiya2426@gmail.com',
            'meesumali110@gmail.com',
            'haniafatimaa@gmail.com',
            'hinashaheen149@gmail.com',
            'Aashylan.r@gmail.com',
            'Khizar3434@gmail.com',
            'omeralvi56@gmail.com',
            'Shaheen_tayba@yahoo.com',
            'Carnivoreashley@gmail.com',
            'hoorainrana799@gmail.com',
            'Kainatyameen1@gmail.com',
            'ihaq99@gmail.com',
            'syedyaman222@gmail.com',
            'deosecondarymultan@gmail.com',
            'Shafiquesoomro00@gmail.com',
            'Zuhaa.zuhaa12@gmail.com',
            'Aiman.qaiser1997@gmail.com',
            'Farankhalid007@gmail.com',
            'm.saim@eximioussol.com',
            'grewalayesha45@gmail.com',
            'zuhasurti4@gmail.com',
            'maxroger513@yahoo.com',
            'admin@thesupersol.com',
            'haniafatimaa12@gmail.com',
            'batmanb280@gmail.com',
            'Waqasyounas213@gmail.com',
            'Aishakomal67@gmail.com',
            'maryyamwaris@gmail.com',
            'Mohsin.raja95@gmail.com',
            'maria.raja94@hotmail.fr',
            'maria.raja98@hotmail.fr',
            'ameerhamzakhanpti7786@gmail.com',
        );
        foreach ($testing_users as $testing_user) {
            $testing_user = strtolower($testing_user);
            if ($email === $testing_user) {
                return true;
            }
        }
        return false;
    }


    /**
     * @Route("/verify", name="user_verification")
     */
    public function verifyAction(Request $request, SecurityHelpers $securityHelpers)
    {
        if($this->is_logged_in()){
            return $this->redirectToRoute("dashboard_home");
        }
        $activate_again = false;
        if($request->isMethod('POST')){
            if($securityHelpers->captchaverify($request->get('g-recaptcha-response'), $this->getParameter('recaptcha_secret'))) {
                $user_email = $request->get('_email');
                $user = $this->getDoctrine()->getRepository("App:Users");
                $query = $user->createQueryBuilder('users')
                    ->andWhere('users.activated = 0')
                    ->andWhere('users.email = :email')
                    ->setParameter("email", $user_email);
                $user = $query->getQuery()
                    ->setMaxResults(1)->getOneOrNullResult();
                if ($user) {
                    $this->sendActivationEmail($user->getId());
                    return $this->redirectToRoute('security_login');
                } else {
                    $this->addFlash("error", "Email is already verified or does not exist.");
                }
            } else {
                $this->addFlash('warning', 'Bad request, verify you are human by checking the captcha.');
            }
        }

        $activation_key = $request->get('activate');
        if ($activation_key) {
            $user = $this->getDoctrine()->getRepository("App:Users")
                ->findOneByActivationKey($activation_key);
            if ($user) {
                // Activate user
                $user->setActivationKey(null);
                $user->setActivated("1");
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                // redirect to login
                $this->addFlash("success", "Account is activated successfully.");
                return $this->redirectToRoute('security_login');
            } else {
                $this->addFlash("error", "Activation link is either invalid or expired");
            }
        }
        $activate_again = true;
        return $this->render('RegistrationController/verify.html.twig',
            array(
                'activate_again'=>$activate_again
            ));

    }
    private function sendActivationEmail($id = null, $email = null, $activation_key = null){
        $mailer = $this->get('swiftmailer.mailer.memory_mailer');
        $user = $this->getDoctrine()->getRepository("App:Users");
        $query = $user->createQueryBuilder('users')
            ->andWhere('users.activated = 0');
        if($id){
            $query = $query->andWhere('users.id = :id')
            ->setParameter('id', $id);
        } else if($email){
            $query = $query->andWhere('users.email = :email')
            ->setParameter('email', $email);
        } else if($activation_key){
            $query = $query->andWhere('users.activationKey = :activation_key')
            ->setParameter('activation_key', $activation_key);
        }
        $user = $query->getQuery()
            ->setMaxResults(1)->getOneOrNullResult();
        if($user) {
            $activationKey = base64_encode(random_bytes(40) . time());
            $user->setActivationKey($activationKey);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            // send account activation email to users
            // Check if user is new or old
            if($user->getCreatedAt() == $user->getModifiedAt()){
                $renderedEmailBody = $this->renderView(
                    'emails/activate_account_email_template.html.twig',
                    array(
                        'name' => $user->getFirstName(),
                        'activation_key' => $user->getActivationKey(),
                        'email' => $user->getEmail()
                    )
                );
            } else{
                $renderedEmailBody = $this->renderView(
                    'emails/update_account_email_template.html.twig',
                    array(
                        'name' => $user->getFirstName(),
                        'activation_key' => $user->getActivationKey(),
                        'email' => $user->getEmail()
                    )
                );
            }
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom($this->getParameter("send_emails_from"), "Twitter Accounts Manager")
                ->setTo($user->getEmail())
                ->setSubject("Verify your email address - " . $this->getParameter("website_title"))
                ->setBody(
                    $renderedEmailBody,
                    'text/html'
                );
//
            if($mails_sent_count = $mailer->send($message)) {
                $this->addFlash("info", "An email has been sent to your email address,"
                    . " please follow the instructions in email to activate your account. May take 5 to 10 minutes.");
                return true;
            } else{
                $this->addFlash("error", "Email server was too busy, please try again in few minutes.");
                return false;
            }
        }
        return false;

    }

    private function is_logged_in()
    {
        if($this->isGranted("ROLE_USER")){
            return true;
        }
        return false;
    }



}
