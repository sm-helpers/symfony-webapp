<?php

namespace App\Controller\Dashboard;


use App\Entity\Users;
use App\Entity\TwitterAccounts;
use App\Utils\GeneralHelpers;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/dashboard")
 * @Security("is_granted('ROLE_USER')")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="dashboard_home")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('twitter_index');
    }

    /**
     * @Route(path="/settings", name="dashboard_settings")
     */
    public function settingsAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->getUser();
        $available_tabs = array(
            'profile' => 'profile',
            'password' => 'password',
            'connected-accounts' => 'connected-accounts'
        );
        $active_tab = 'profile';
        if ($request->get('0') == "tab") {
            $tab = $request->get('1');
            foreach ($available_tabs as $index => $available_tab) {
                if ($tab == $available_tab) {
                    $active_tab = $available_tab;
                }
            }
        } else if ($request->get('active_tab')) {
            $tab = $request->get('active_tab');
            foreach ($available_tabs as $index => $available_tab) {
                if ($tab == $available_tab) {
                    $active_tab = $available_tab;
                }
            }
        }
        $connectedAuthCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findBy(array(
                'userId' => $user->getId()
            ));
        $connectedAccounts = array();
        foreach ($connectedAuthCreds as $connectedAuthCred) {
            $twUserId = $connectedAuthCred->getTwUserId();
            $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
                ->find($twUserId);
            $temp = $connectedAuthCred;
            $temp->twitterAccount = $twitterAccount;
            $connectedAccounts[] = $temp;
        }

        if ($request->isMethod('POST')) {
            if ($request->get('form') == 'profile') {
                $active_tab = "profile";

                $newData = array();
                $newData['firstName'] = $request->get("_first_name");
                $newData['lastName'] = $request->get("_last_name");
                $newData['password'] = $request->get("_plain_password");
                $newData['email'] = $request->get("_email");
                $status = $this->updateUserProfile($newData);
                if ($status['success'] && $status['logout']) {
                    return $this->redirectToRoute('security_login');
                }
                $user = $newData;
            } else if ($request->get('form') == 'password') {
                $active_tab = "password";
                $newData = array();
                $newData['oldPassword'] = $request->get('_old_plain_password');
                $newData['newPassword'] = $request->get('_plain_password');
                $is_valid_length = strlen($newData['newPassword']) > 4 ? true : false;
                if (!$is_valid_length) {
                    $this->addFlash("warning", "Password must be of minimum 5 characters.");
                } else {
                    $is_valid_password = $passwordEncoder->isPasswordValid($this->getUser(), $newData['oldPassword']);
                    if ($is_valid_password) {
                        $newPassword = $passwordEncoder->encodePassword($this->getUser(), $newData['newPassword']);
                        $user = $this->getUser();
                        $user->setPassword($newPassword);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($user);
                        $em->flush();
                        $this->addFlash("success", "Password is successfully updated");
                    } else {
                        $this->addFlash("error", "Invalid old password.");
                        $this->addFlash("info", "If you do not remember, you can logout and try to reset password by Forgot password option.");
                    }
                }

            }
        }
        if ($request->isMethod('POST')) {
            return $this->redirectToRoute('dashboard_settings', array(
                'active_tab' => $active_tab
            ));
        }
        return $this->render('DashboardController/settings.html.twig', array(
            'active_tab' => $active_tab,
            'user' => $user,
            'connected_accounts' => $connectedAccounts
        ));
    }

    /**
     * @Route(path="/removeAuth", name="dashboard_remove_auth")
     */
    public function removeAuthAction(Request $request)
    {
        /*
         * @TODO - Implement remove auth on close icon in Settings -> Connected Account
         */
        if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {
            $authId = $request->get("id");
            $authExist = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    "id" => $authId,
                    "userId" => $this->getUser()->getId()
                ));
            if ($authExist) {
                $em = $this->getDoctrine()->getManager();
                $tw_usename = $authExist->getTwUsername();
                $tw_user_id = $authExist->gettwUserId();
                $em->remove($authExist);
                $em->flush();
                $session = $this->get('session');
                if ($session->get('selectedAccountTwUserId') == $tw_user_id) {
                    $session->remove('selectedAccountTwUserId');
                }
                $response = array("success" => true);
                $this->addFlash('success', "@{$tw_usename} has been removed.");
                return GeneralHelpers::makeJsonResponse($response);
            }
        }
        $response = array("success" => false, "error" => "False request.");
        return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route(path="/setTwSelectedAccount", name="set_tw_selected_account")
     */
    public function setSelectedAccountAction(Request $request)
    {
        $session = $this->get('session');
        $twUserId = $request->get('twUserId');
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'enabled' => true,
                'userId' => $this->getUser()->getId()
            ));
        $response = array();
        if ($authCreds) {
            $session->set('selectedAccountTwUserId', $twUserId);
            $this->addFlash('success', "@" . $authCreds->getTwUsername() . " has been selected.");
            $response['success'] = true;
        } else {
            $response['success'] = false;
            $response['error'] = "Twitter account needs authentication, please go to settings and re-authenticate your account.";
        }


        $response = GeneralHelpers::makeJsonResponse($response);
        return $response;
    }


    /**********************************************
     * General Helper Functions Below this
     **********************************************/

    private function updateUserProfile($newData)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');
        $request = ($this->container->get('request_stack'))->getCurrentRequest();

        $email_exists = false;
        $user = $this->getUser();
        $status = array(
            'success' => false,
            'logout' => false
        );
        // Check if password is correct
        $is_valid_password = $passwordEncoder->isPasswordValid($user, $newData['password']);
        if (!$is_valid_password) {
            $this->addFlash("error", "Password does not match. Enter correct password to update your account info.");
            return false;
        } else {

            // Check if email already exists
            $email_exists = $this->getDoctrine()
                ->getRepository("App:Users")
                ->findOneByEmail($newData['email']);
            $email_changed = false;
            if ($email_exists) {
                if ($email_exists->getId() != $this->getUser()->getId()) {
                    $this->addFlash("error", "Email already registered and is associated with another account.");
                    return $status;
                } else {
                    $email_exists = false;
                }
            } else {
                $email_changed = true;
            }
        }

        if ($email_exists || !$is_valid_password) {
            return $status;
        }

        $modifiedAt = new \DateTime();
        $user->setModifiedAt($modifiedAt);
        if ($email_changed) {
            $user->setActivated("0");
        }

        $user->setEmail($newData['email']);
        $user->setFirstName($newData['firstName']);
        $user->setLastName($newData['lastName']);

        // save the Users!
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        // send account activation email to user
        if ($email_changed) {
            $this->sendActivationEmail($user->getId());
            $this->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();
            // set a "flash" success message for the user
            $this->addFlash("success", "Account Updated Successfully.");
            $this->addFlash("info to proceed", "An email has been sent to your new email address, " .
                "please follow the instructions in email to activate your account. May take 5 to 10 minutes.");
            $status['success'] = true;
            $status['logout'] = true;
            return $status;
        }
        // set a "flash" success message for the user
        $this->addFlash("success", "Account Updated Successfully.");
        $status['success'] = true;
        return $status;
    }

    public function getConnectedAccountsAction()
    {
        $user_id = $this->getUser()->getId();
        $platforms = $this->getDoctrine()->getRepository("App:Users")
            ->getConnectedAccounts($user_id, false);

        return $this->render('includes/get_connected_accounts.html.twig', array(
            'platforms' => $platforms
        ));
    }

    public function getSelectedAccountNameAction()
    {
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId', 0);
        $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->find($twUserId);
        $name = "";
        if ($twitterAccount) {
            $name = $twitterAccount->getTwUsername();
            $name = "Using as @{$name}";
        }
        $response = new Response($name);
        return $response;
    }

    public function getAllUsers()
    {
        $users = array();
        if ($this->isGranted('ROLE_ADMIN')) {
            $users = $this->getDoctrine()->getRepository("App:Users")
                ->findBy(array(
                    'role' => "ROLE_USER"
                ));
        }
        return $this->render('includes/get_all_users.html.twig', array(
            'users' => $users
        ));
    }

    private function sendActivationEmail($id = null, $email = null, $activation_key = null)
    {
        $mailer = $this->get('swiftmailer.mailer.memory_mailer');
        $user = $this->getDoctrine()->getRepository("App:Users");
        $query = $user->createQueryBuilder('users')
            ->andWhere('users.activated = 0');
        if ($id) {
            $query = $query->andWhere('users.id = :id')
                ->setParameter('id', $id);
        } else if ($email) {
            $query = $query->andWhere('users.email = :email')
                ->setParameter('email', $email);
        } else if ($activation_key) {
            $query = $query->andWhere('users.activationKey = :activation_key')
                ->setParameter('activation_key', $activation_key);
        }
        $user = $query->getQuery()
            ->setMaxResults(1)->getOneOrNullResult();
        if ($user) {
            $activationKey = base64_encode(random_bytes(40) . time());
            $user->setActivationKey($activationKey);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            // send account activation email to users
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom($this->getParameter("send_emails_from"), "Twitter Accounts Manager")
                ->setTo($user->getEmail())
                ->setSubject("Verify your email address - " . $this->getParameter("website_title"))
                ->setBody(
                    $this->renderView(
                        'emails/update_account_email_template.html.twig',
                        array(
                            'name' => $user->getFirstName(),
                            'activation_key' => $user->getActivationKey(),
                            'email' => $user->getEmail()
                        )
                    ),
                    'text/html'
                );
//
            if ($mails_sent_count = $mailer->send($message)) {
                $this->addFlash("info to proceed", "An email has been sent to your email address, " .
                    "please follow the instructions in email to activate your account. May take 5 to 10 minutes.");
                return true;
            } else {
                $this->addFlash("error", "Email server was too busy, please try again in few minutes.");
                return false;
            }
        }
        return false;

    }
}
