<?php

namespace App\Controller\Dashboard;

use App\CustomExceptions\InvalidCaptchaException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Security\SecurityHelpers;


class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request)
    {
        if($this->is_logged_in()){
            return $this->redirectToRoute("dashboard_home");
        }

        $disabled_user = false;
        $authUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        if($error instanceof InvalidCaptchaException){
            $error = null;
            $this->addFlash("warning", "Bad request, verify you are human by checking the captcha.");
        } else if ($error instanceof DisabledException) {
            $error = null;
            $this->addFlash("warning", "Account is not activated. Verify your email to activate.");
            $disabled_user = true;
        }

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('SecurityController/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
            'disabled_user'=>$disabled_user,
        ));
    }
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request) {

    }

    /**
     * @Route("/forgot", name="security_forgot_password")
     */
    public function forgotAction(Request $request, SecurityHelpers $securityHelpers) {
        if($this->is_logged_in()){
            return $this->redirectToRoute("dashboard_home");
        }
        $reset_password_form = false;
        if($request->isMethod('POST')){
            if($securityHelpers->captchaverify($request->get('g-recaptcha-response'), $this->getParameter('recaptcha_secret'))) {
                $user_email = $request->get('_email');
                $user = $this->getDoctrine()->getRepository("App:Users");
                $query = $user->createQueryBuilder('users')
                    ->andWhere('users.activated = 1')
                    ->andWhere('users.email = :email')
                    ->setParameter("email", $user_email);
                $user = $query->getQuery()
                    ->setMaxResults(1)->getOneOrNullResult();
                if ($user) {
                    $this->sendResetPasswordEmail($user->getId());
                    return $this->redirectToRoute('security_login');
                } else {
                    $this->addFlash("error", "Email is invalid or does not exists.");
                }
            } else {
                $this->addFlash('warning', 'Bad request, verify you are human by checking the captcha.');
            }
        }
        $reset_password_form = true;
        return $this->render('SecurityController/forgot.html.twig',
            array(
                'reset_password'=> $reset_password_form,
            ));
    }

    /**
     * @Route("/reset", name="security_reset_password")
     */
    public function resetPasswordAction(Request $request, UserPasswordEncoderInterface $encoder){
        if($this->is_logged_in()){
            return $this->redirectToRoute("dashboard_home");
        }
        if($request->isMethod('POST')){
            $resetPasswordKey = $request->get("_reset_key");
            $newPassword = $request->get("_password");
            if(\strlen($newPassword) > 4) {
                $user = $this->getDoctrine()->getRepository("App:Users")
                    ->findOneByResetPasswordKey($resetPasswordKey);
                if ($user) {
                    $password = $encoder->encodePassword($user, $newPassword);
                    $user->setPassword($password);
                    $user->setResetPasswordKey(null);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $this->addFlash("success", "Password has been reset.");
                    return $this->redirectToRoute("security_login");
                } else{
                    $this->addFlash("error", "Invalid or expired password reset link.");
                }
            } else {
                $this->addFlash("warning", "Password must be 5 characters long.");
            }
            return $this->redirectToRoute("security_reset_password",
                array(
                    'initiate'=> $resetPasswordKey
                ));
        }
        $resetPasswordKey = $request->get("initiate");
        return $this->render("SecurityController/reset_password.html.twig",
            array(
                'reset_password_key'=> $resetPasswordKey
            )
        );


    }

    private function sendResetPasswordEmail($id = null, $email = null){
        $mailer = $this->get('swiftmailer.mailer.memory_mailer');
        if($this->is_logged_in()){
            return $this->redirectToRoute("dashboard_home");
        }
        $temp = 0;
        $user = $this->getDoctrine()->getRepository("App:Users");
        $query = $user->createQueryBuilder('users')
        ->andWhere('users.activated = 1');
        if($id){
            $query = $query->andWhere('users.id = :id')
                ->setParameter('id', $id);
        } else if($email){
            $query = $query->andWhere('users.email = :email')
                ->setParameter('email', $email);
        }
        $user = $query->getQuery()
            ->setMaxResults(1)->getOneOrNullResult();
        if($user) {
            $resetPasswordKey = base64_encode(random_bytes(40) . time());
            $user->setResetPasswordKey($resetPasswordKey);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            // send reset password email to user
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom($this->getParameter("send_emails_from"), "Twitter Accounts Manager")
                ->setTo($user->getEmail())
                ->setSubject("Reset your password - " . $this->getParameter("website_title"))
                ->setBody(
                    $this->renderView(
                        'emails/reset_password_email_template.html.twig',
                        array(
                            'name' => $user->getFirstName(),
                            'reset_password_key' => $user->getResetPasswordKey(),
                            'email' => $user->getEmail()
                        )
                    ),
                    'text/html'
                );
//
            if($mails_sent_count = $mailer->send($message)) {
                $this->addFlash("info", "An email has been sent to your email address, "
                    . " please follow the instructions in email to reset your password. May take 5 to 10 minutes.");
                return true;
            } else{
                $this->addFlash("error", "Email server was too busy, please try again in few minutes.");
                return false;
            }
        }
        return false;

    }
    private function is_logged_in()
    {
        if($this->isGranted("ROLE_USER")){
            return true;
        }
        return false;
    }

}
