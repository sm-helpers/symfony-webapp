<?php

namespace App\Controller\Dashboard;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Controller\Dashboard\Interfaces\TwitterPreInterface;
use App\Entity\AuthCreds;
use App\Entity\FetchTasksLogs;
use App\Routines\TwitterAccountsRoutines;
use App\Utils\TwitterHelpers;
use App\Utils\GeneralHelpers;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard/twitter")
 * @Security("is_granted('ROLE_USER')")
 */
class TwitterController extends Controller implements TwitterPreInterface
{
    /**
     * @Route("/", name="twitter_index")
     */
    public function indexAction(Request $request)
    {
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId', 0);
        $isAccountSelected = false;
        $isDataAvailable = false;
        $newFollowers = 0;
        $newFriends = 0;
        $newUnfollowers = 0;
        $totalFollowers = 0;
        $totalFriends = 0;
        $totalUnfollowers = 0;
        $lastFetchedDate = "Not fetched yet";

        $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->find($twUserId);

        if ($twitterAccount) {
            $isAccountSelected = true;

            /** @var FetchTasksLogs $twitterAccountLog */
            $twitterAccountLog = null;
            try {
                $twitterAccountLog = $this->getDoctrine()->getRepository('App:FetchTasksLogs')
                    ->createQueryBuilder('fetchTasksLogs');
                $twitterAccountLog = $twitterAccountLog->select('fetchTasksLogs')
                    ->where("fetchTasksLogs.twitterAccount = :twAccount")
                    ->setParameter('twAccount', $twitterAccount)
                    ->orderBy('fetchTasksLogs.date', 'DESC')
                    ->setMaxResults(1)
                    ->setFirstResult(0)
                    ->getQuery()
                    ->getOneOrNullResult();
            } catch (NonUniqueResultException $e) {
                // pass as it will never occur, we have already setMaxResults(1)
                // created to ignore IDE exceptions hints
            }
            if ($twitterAccountLog) {
                $isDataAvailable = true;
                $newFollowers = $twitterAccountLog->getNewFollowers();
                $newFriends = $twitterAccountLog->getNewFriends();
                $newUnfollowers = $twitterAccountLog->getUnfollowers();

                if ($newFollowers['success']) {
                    $newFollowers = $newFollowers['count'];
                } else {
                    $newFollowers = "N/A";
                }
                if ($newFriends['success']) {
                    $newFriends = $newFriends['count'];
                } else {
                    $newFriends = "N/A";
                }
                if ($newUnfollowers['success']) {
                    $newUnfollowers = $newUnfollowers['count'];
                } else {
                    $newUnfollowers = "N/A";
                }

                $lastFetchedDate = $twitterAccountLog->getDate();
                $lastFetchedDate = $lastFetchedDate->format('Y-m-d');
            } else {
                $isDataAvailable = false;
            }
        }
        $totalFollowers = $this->getDoctrine()->getRepository('App:TwitterFollowers')
            ->countTotalFollowers($twUserId);
        $totalFriends = $this->getDoctrine()->getRepository('App:TwitterFriends')
            ->countTotalFriends($twUserId);
        $totalUnfollowers = $this->getDoctrine()->getRepository('App:TwitterUnfollowers')
            ->countTotalUnfollowers($twUserId);

        return $this->render('TwitterController/index.html.twig', array(
            'twUserId' => $twUserId,
            'isAccountSelected' => $isAccountSelected,
            'isDataAvailable' => $isDataAvailable,
            'newFollowers' => GeneralHelpers::number_format_short($newFollowers),
            'newFriends' => GeneralHelpers::number_format_short($newFriends),
            'newUnfollowers' => GeneralHelpers::number_format_short($newUnfollowers),
            'totalFollowers' => GeneralHelpers::number_format_short($totalFollowers),
            'totalFriends' => GeneralHelpers::number_format_short($totalFriends),
            'totalUnfollowers' => GeneralHelpers::number_format_short($totalUnfollowers),
            'lastFetchedDate' => $lastFetchedDate
        ));
    }


    /**
     * @Route("/executeTasks", name="twitter_execute_tasks")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function executeTasksAction(Request $request, TwitterHelpers $twitterHelpers, TwitterAccountsRoutines $twitterAccountsRoutines)
    {
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        $response = array();
//        $response = $twitterHelpers->executeFollowersTask($authCreds);
//        $response = $twitterHelpers->executeFriendsTask($authCreds);
//        $response = $twitterHelpers->executeMutesTask($authCreds);
//        $response = $twitterHelpers->executeTwAccountsUpdateTask();

//        $twitterAccountsRoutines->createAndRunTasksForAUser($authCreds, array(
//            TwitterAccountsRoutines::TASK_NAME_FOR_FOLLLOWERS, TwitterAccountsRoutines::TASK_NAME_FOR_FRIENDS
//        ), array(
//            'tw_user_id' => "845371967448498179",
//            'date' => date("Y-m-d H:i:s")
//        ), true);
//        $twitterAccountsRoutines->handleCreateTasks($authCreds->getId());
//        $twitterAccountsRoutines->handleRunSingleTask(1573);
//        $response = $this->getDoctrine()->getRepository('App:Tasks')
//            ->findBy(array(
//                'parameters'=> serialize(array())
//            ));
        return $this->render('TwitterController/executeTasks.html.twig', array(
            "response" => $response
        ));
    }

    /**
     * @Route("/stats-details", name="twitter_stats_details")
     */
    public function statsDetailsAction(Request $request)
    {
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->find($twUserId);
        $twUsername = "";
        if ($twitterAccount) {
            $twUsername = $twitterAccount->getTwUsername();
        }

        return $this->render('TwitterController/stats.html.twig', array(
            'twUsername' => $twUsername
        ));

    }

    /**
     * @Route("/stats-details-ajax", name="twitter_stats_details_ajax")
     */
    public function statsDetailsAjaxAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $orderCol = $order[0]['column'] == 0 ? "fetchTasksLogs.date" : null;
        $orderDir = $order[0]['dir'];
        $responseArray = array();
        $totalRecords = 0;
        $filteredRecords = 0;

        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->find($twUserId);
        if ($twitterAccount) {
            $stats = $this->getDoctrine()->getRepository('App:FetchTasksLogs')
                ->createQueryBuilder('fetchTasksLogs');

            $query = $stats->select('fetchTasksLogs.id')
                ->andWhere('fetchTasksLogs.twitterAccount = :twitter_account')
                ->setParameter('twitter_account', $twitterAccount);
            if ($orderCol) {
                $query->orderBy($orderCol, $orderDir);
            }
            $filteredRecords = clone $query;
            $query->setMaxResults($length)->setFirstResult($start);
            $results = $query->getQuery()
                ->getArrayResult();
            $filteredRecords = $filteredRecords->select('count(fetchTasksLogs.id)')
                ->getQuery()->getSingleScalarResult();
            $totalRecords = ($this->getDoctrine()->getRepository('App:FetchTasksLogs')
                ->createQueryBuilder('fetchTasksLogs')
                ->select('count(fetchTasksLogs.id) as total')
                ->where('fetchTasksLogs.twitterAccount = :twitter_account')
                ->setParameter('twitter_account', $twitterAccount)
                ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

            foreach ($results as $result) {
                $stat = $this->getDoctrine()->getRepository('App:FetchTasksLogs')
                    ->find($result['id']);
                $newFollowers = $stat->getNewFollowers();
                $newFriends = $stat->getNewFriends();
                $newUnfollowers = $stat->getUnfollowers();

                if ($newFollowers['success']) {
                    $newFollowers = $newFollowers['count'];
                } else {
                    $newFollowers = "N/A";
                }
                if ($newFriends['success']) {
                    $newFriends = $newFriends['count'];
                } else {
                    $newFriends = "N/A";
                }
                if ($newUnfollowers['success']) {
                    $newUnfollowers = $newUnfollowers['count'];
                } else {
                    $newUnfollowers = "N/A";
                }
                $temp['followers'] = $newFollowers;
                $temp['friends'] = $newFriends;
                $temp['unfollowers'] = $newUnfollowers;
                /** @var \DateTime $date */
                $date = $stat->getDate();
                $temp['date'] = $date->format('d M');
                $temp['date_table'] = $date->format('F j, Y');
                $resultArray[] = $temp;
            }
        }

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray
        );

        return GeneralHelpers::makeJsonResponse($responseArray);
    }


    /**
     * @Route("/followers", name="twitter_followers")
     */
    public function followersAction(Request $request)
    {
        return $this->render('TwitterController/followers.html.twig');
    }

    /**
     * @Route("/followers-ajax", name="twitter_followers_ajax")
     */
    public function followersAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $startTime = microtime(true);
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $orderCol = $order[0]['column'] == 2 ? "myFollowers.id" : null;
        $orderDir = $order[0]['dir'];
        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.myFollowers', 'myFollowers')
            ->select('myFollowers.createdAt')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('myFollowers.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId);

        $query = $twitterHelpers->applyDatatableFilters($query, "myFollowers", $request, $authCreds);

        if ($orderCol) {
            $query->orderBy($orderCol, $orderDir);
        }
        $filteredRecords = clone $query;
        $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:TwitterFollowers')
            ->createQueryBuilder('twitterFollowers')
            ->select('count(twitterFollowers.twUserIdX) as total')
            ->where('twitterFollowers.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'created_at' => date_format($result['createdAt'], 'F j, Y'),
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'created_at' => date_format($result['createdAt'], 'M j, Y'),
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }
        $endTime = microtime(true);
        $performance = array(
            "start" => $startTime,
            "end" => $endTime,
            "time" => $endTime - $startTime,
            "memory" => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true)),
        );

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray,
            'debug' => array(
                "performance" => $performance
            )
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/friends", name="twitter_friends")
     */
    public function friendsAction(Request $request)
    {
        return $this->render('TwitterController/friends.html.twig');
    }

    /**
     * @Route("/friends-ajax", name="twitter_friends_ajax")
     */
    public function friendsAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $startTime = microtime(true);
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');

        $orderCol = $order[0]['column'] == 2 ? "myFriends.id" : null;
        $orderDir = $order[0]['dir'];

        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.myFriends', 'myFriends')
            ->select('myFriends.createdAt')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('myFriends.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId);


        $query = $twitterHelpers->applyDatatableFilters($query, "myFriends", $request, $authCreds);
        if ($orderCol) {
            $query->orderBy($orderCol, $orderDir);
        }
        $filteredRecords = clone $query;
        $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:TwitterFriends')
            ->createQueryBuilder('twitterFriends')
            ->select('count(twitterFriends.twUserIdX) as total')
            ->where('twitterFriends.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'created_at' => date_format($result['createdAt'], 'F j, Y'),
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'created_at' => date_format($result['createdAt'], 'M j, Y'),
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }
        $endTime = microtime(true);
        $performance = array(
            "start" => $startTime,
            "end" => $endTime,
            "time" => $endTime - $startTime,
            "memory" => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true)),
        );

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray,
            'debug' => array(
                "performance" => $performance
            )
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/mutes", name="twitter_mutes")
     */
    public function mutesAction(Request $request)
    {
        return $this->render('TwitterController/mutes.html.twig');
    }

    /**
     * @Route("/mutes-ajax", name="twitter_mutes_ajax")
     */
    public function mutesAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $orderCol = $order[0]['column'] == 1 ? "myMutes.id" : "myMutes.id";
        $orderDir = $order[0]['dir'] ? "desc" : "desc";
        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.myMutes', 'myMutes')
            ->select('myMutes.createdAt')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('myMutes.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId);
        $query = $twitterHelpers->applyDatatableFilters($query, "myMutes", $request, $authCreds);
        if ($orderCol) {
            $query->orderBy('myMutes.id', $orderDir);
        }
        $filteredRecords = clone $query;
        $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:TwitterMutes')
            ->createQueryBuilder('twitterMutes')
            ->select('count(twitterMutes.twUserIdX) as total')
            ->where('twitterMutes.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'created_at' => date_format($result['createdAt'], 'F j, Y'),
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'created_at' => date_format($result['createdAt'], 'M j, Y'),
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/whitelist", name="twitter_whitelist")
     */
    public function whitelistAction(Request $request)
    {


        return $this->render('TwitterController/whitelist.html.twig');
    }

    /**
     * @Route("/whitelistAjax", name="twitter_whitelist_ajax")
     */
    public function whitelistAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $orderCol = $order[0]['column'] == 1 ? "twitterWhitelist.id" : "twitterWhitelist.id";
        $orderDir = $order[0]['dir'] ? "desc" : "desc";
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.twitterWhitelist', 'twitterWhitelist')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('twitterWhitelist.authCredsId = :authCredsId')
            ->setParameter('authCredsId', $twAuthCredsId);
        $query = $twitterHelpers->applyDatatableFilters($query, "twitterWhitelist", $request, $authCreds);
        if ($orderCol) {
            $query->orderBy('twitterWhitelist.id', $orderDir);
        }
        $filteredRecords = clone $query;
        $query = $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:TwitterWhitelist')
            ->createQueryBuilder('twitterWhitelist')
            ->select('count(twitterWhitelist.twUserIdX) as total')
            ->where('twitterWhitelist.authCredsId = :authCredsId')
            ->setParameter('authCredsId', $twAuthCredsId)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/blacklist", name="twitter_blacklist")
     */
    public function blacklistAction(Request $request)
    {


        return $this->render('TwitterController/blacklist.html.twig');
    }

    /**
     * @Route("/blacklistAjax", name="twitter_blacklist_ajax")
     */
    public function blacklistAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $orderCol = $order[0]['column'] == 1 ? "twitterBlacklist.id" : "twitterBlacklist.id";
        $orderDir = $order[0]['dir'] ? "desc" : "desc";
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.twitterBlacklist', 'twitterBlacklist')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('twitterBlacklist.authCredsId = :authCredsId')
            ->setParameter('authCredsId', $twAuthCredsId);
        $query = $twitterHelpers->applyDatatableFilters($query, "twitterBlacklist", $request, $authCreds);
        if ($orderCol) {
            $query->orderBy('twitterBlacklist.id', $orderDir);
        }

        $filteredRecords = clone $query;
        $query = $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:twitterBlacklist')
            ->createQueryBuilder('twitterBlacklist')
            ->select('count(twitterBlacklist.twUserIdX) as total')
            ->where('twitterBlacklist.authCredsId = :authCredsId')
            ->setParameter('authCredsId', $twAuthCredsId)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/unfollowers", name="twitter_unfollowers")
     */
    public function unfollowersAction(Request $request)
    {


        return $this->render('TwitterController/unfollowers.html.twig');
    }

    /**
     * @Route("/unfollowersAjax", name="twitter_unfollowers_ajax")
     */
    public function unfollowersAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $orderCol = $order[0]['column'] == 2 ? "myUnfollowers.id" : null;
        $orderDir = $order[0]['dir'];
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.myUnfollowers', 'myUnfollowers')
            ->select('myUnfollowers.createdAt')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('myUnfollowers.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId);
        $query = $twitterHelpers->applyDatatableFilters($query, "myUnfollowers", $request, $authCreds);
        if ($orderCol) {
            $query->orderBy($orderCol, $orderDir);
        }
        $filteredRecords = clone $query;
        $query = $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:twitterUnfollowers')
            ->createQueryBuilder('twitterUnfollowers')
            ->select('count(twitterUnfollowers.twUserIdX) as total')
            ->where('twitterUnfollowers.twUserId = :twUserId')
            ->setParameter('twUserId', $twUserId)
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'created_at' => date_format($result['createdAt'], 'M j, Y'),
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/create/blacklist", name="twitter_create_blacklist")
     */
    public function createBlacklistAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $user = $this->getUser();
        $twUserId = $request->get('tw_user_id', -1);

        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'userId' => $this->getUser()->getId(),
                'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
            ));
        $processResponse = $twitterHelpers->createBlacklist($authCreds, $twUserId);
        if ($processResponse['success']) {
            $processResponse['message'] = "Added to blacklist";
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/destroy/blacklist", name="twitter_destroy_blacklist")
     */
    public function destroyBlacklistAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $user = $this->getUser();
        $twUserId = $request->get('tw_user_id', -1);

        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'userId' => $this->getUser()->getId(),
                'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
            ));
        $processResponse = $twitterHelpers->destroyBlacklist($authCreds, $twUserId);
        if ($processResponse['success']) {
            $processResponse['message'] = "Removed from blacklist";
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/create/whitelist", name="twitter_create_whitelist")
     */
    public function createWhitelistAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $user = $this->getUser();
        $twUserId = $request->get('tw_user_id', -1);

        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'userId' => $this->getUser()->getId(),
                'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
            ));
        $processResponse = $twitterHelpers->createWhitelist($authCreds, $twUserId);
        if ($processResponse['success']) {
            $processResponse['message'] = "Added to whitelist";
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/destroy/whitelist", name="twitter_destroy_whitelist")
     */
    public function destroyWhitelistAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $user = $this->getUser();
        $twUserId = $request->get('tw_user_id', -1);

        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'userId' => $this->getUser()->getId(),
                'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
            ));
        $processResponse = $twitterHelpers->destroyWhitelist($authCreds, $twUserId);
        if ($processResponse['success']) {
            $processResponse['message'] = "Removed from whitelist";
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/create/mute", name="twitter_create_mute")
     */
    public function createMuteAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $processResponse = array('success' => true);
        $rate_limits = $this->get('session')->get('rate_limits', array());
        if (isset($rate_limits['create_mute'])) {
            if ($rate_limits['create_mute_reset'] > time()) {
                $processResponse = array(
                    'success' => false,
                    "rate_limit_status" => true,
                    'error' => "Twitter rate limit error."
                );
            }
        }
        if ($processResponse['success']) {
            $user = $this->getUser();

            $twUserId = $request->get('tw_user_id', -1);

            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'userId' => $this->getUser()->getId(),
                    'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
                ));
            $processResponse = $twitterHelpers->createMute($authCreds, $twUserId);
            $processResponse['silent'] = (isset($processResponse['silent']) && $processResponse['silent']) ? true : false;
            if ($processResponse['success'] && !$processResponse['silent']) {
                $processResponse['message'] = "@" . $processResponse['screen_name'] . " has been muted";
            } else if (isset($processResponse['rate_limit_status'])) {
                $rate_limits = $this->get('session')->get('rate_limits', array());
                $rate_limits['create_mute'] = true;
                $rate_limits['create_mute_reset'] = time() + 600;
                $this->get('session')->set('rate_limits', $rate_limits);
            }
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/destroy/mute", name="twitter_destroy_mute")
     */
    public function destroyMuteAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $processResponse = array('success' => true);
        $rate_limits = $this->get('session')->get('rate_limits', array());
        if (isset($rate_limits['destroy_mute'])) {
            if ($rate_limits['destroy_mute_reset'] > time()) {
                $processResponse = array(
                    'success' => false,
                    "rate_limit_status" => true,
                    'error' => "Twitter rate limit error."
                );
            }
        }
        if ($processResponse['success']) {
            $user = $this->getUser();

            $twUserId = $request->get('tw_user_id', -1);

            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'userId' => $this->getUser()->getId(),
                    'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
                ));
            $processResponse = $twitterHelpers->destroyMute($authCreds, $twUserId);
            $processResponse['silent'] = (isset($processResponse['silent']) && $processResponse['silent']) ? true : false;
            if ($processResponse['success'] && !$processResponse['silent']) {
                $processResponse['message'] = "@" . $processResponse['screen_name'] . " has been unmuted";
            } else if (isset($processResponse['rate_limit_status'])) {
                $rate_limits = $this->get('session')->get('rate_limits', array());
                $rate_limits['destroy_mute'] = true;
                $rate_limits['destroy_mute_reset'] = time() + 600;
                $this->get('session')->set('rate_limits', $rate_limits);
            }
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/create/friendship", name="twitter_create_friendship")
     */
    public function createFriendshipAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $processResponse = array('success' => true);
        $rate_limits = $this->get('session')->get('rate_limits', array());
        if (isset($rate_limits['create_friendship'])) {
            if ($rate_limits['create_friendship_reset'] > time()) {
                $processResponse = array(
                    'success' => false,
                    "rate_limit_status" => true,
                    'error' => "Twitter rate limit error."
                );
            }
        }
        if ($processResponse['success']) {
            $user = $this->getUser();

            $twUserId = $request->get('tw_user_id', -1);

            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'userId' => $this->getUser()->getId(),
                    'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
                ));
            $sleepTime = rand(100, 500);
            $sleepTime = $sleepTime * 10000;
            usleep($sleepTime);
            $processResponse = $twitterHelpers->createFriendship($authCreds, $twUserId);
            $processResponse['silent'] = (isset($processResponse['silent']) && $processResponse['silent']) ? true : false;
            if ($processResponse['success'] && !$processResponse['silent']) {
                if (isset($processResponse['request_sent'])) {
                    $twUser = $this->getDoctrine()->getRepository('App:TwitterAccounts')
                        ->find($twUserId);
                    if ($twUser) {
                        $screen_name = $twUser->getTwUsername();
                        $processResponse['message'] = "Follow request sent to @" . $screen_name;
                    } else {
                        $processResponse['message'] = "Follow request sent";
                    }
                } else {
                    $processResponse['message'] = "@" . $processResponse['screen_name'] . " has been followed";
                }
            } else if (isset($processResponse['rate_limit_status'])) {
                $rate_limits = $this->get('session')->get('rate_limits', array());
                $rate_limits['create_friendship'] = true;
                $rate_limits['create_friendship_reset'] = time() + 600;
                $this->get('session')->set('rate_limits', $rate_limits);
            }
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/destroy/friendship", name="twitter_destroy_friendship")
     */
    public function destroyFriendshipAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $processResponse = array('success' => true);
        $rate_limits = $this->get('session')->get('rate_limits', array());
        if (isset($rate_limits['destroy_friendship'])) {
            if ($rate_limits['destroy_friendship_reset'] > time()) {
                $processResponse = array(
                    'success' => false,
                    "rate_limit_status" => true,
                    'error' => "Twitter rate limit error."
                );
            }
        }
        if ($processResponse['success']) {
            $user = $this->getUser();

            $twUserId = $request->get('tw_user_id', -1);

            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'userId' => $this->getUser()->getId(),
                    'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
                ));
            $sleepTime = rand(100, 500);
            $sleepTime = $sleepTime * 10000;
            usleep($sleepTime);
            $processResponse = $twitterHelpers->destroyFriendship($authCreds, $twUserId);
            $processResponse['silent'] = (isset($processResponse['silent']) && $processResponse['silent']) ? true : false;
            if ($processResponse['success'] && !$processResponse['silent']) {
                $processResponse['message'] = "@" . $processResponse['screen_name'] . " has been unfollowed";
            } else if (isset($processResponse['rate_limit_status'])) {
                $rate_limits = $this->get('session')->get('rate_limits', array());
                $rate_limits['destroy_friendship'] = true;
                $rate_limits['destroy_friendship_reset'] = time() + 600;
                $this->get('session')->set('rate_limits', $rate_limits);
            }
        }
        $responseArray = $processResponse;
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;
    }

    /**
     * @Route("/search-account", name="twitter_search_account")
     */
    public function searchAccountAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        return $this->render('TwitterController/search_account.html.twig');
    }

    /**
     * @Route("/search-account-ajax", name="twitter_search_account_ajax")
     */
    public function searchAccountAjaxAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $output = array(
            'success' => false,
            'output' => '',
            'tw_username' => '',
            'error' => ''
        );
        $accountExists = array(
            'success' => false
        );

        $twUsername = $request->get('tw_username');
        if (empty($twUsername)) {
            $output['success'] = false;
            $output['error'] = "No twitter account found.";
            return GeneralHelpers::makeJsonResponse($output);
        }

        $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->findOneBy(array(
                'twUsername' => $twUsername
            ));
        if ($this->get('session')->has('selectedAccountTwUserId') && !$twitterAccount) {

            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'userId' => $this->getUser()->getId(),
                    'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
                ));
            $accountExists = $twitterHelpers->fetchAndCreateTwitterUsers($authCreds, null, $twUsername);
            if ($accountExists['success']) {
                $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
                    ->findOneBy(array(
                        'twUsername' => $twUsername
                    ));
            }
        }

        if ($twitterAccount) {
            $twUserId = $this->get('session')->get('selectedAccountTwUserId');
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'twUserId' => $twUserId
                ));
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $twitterAccount->getTwUserId());
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $twitterAccount->getTwUserId());
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($authCreds->getId(), $twitterAccount->getTwUserId());
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($authCreds->getId(), $twitterAccount->getTwUserId());
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $twitterAccount->getTwUserId());

            $profileData = array(
                "user_id" => $twitterAccount->getTwUserId(),
                'username' => $twitterAccount->getTwUsername(),
                'name' => $twitterAccount->getTwName(),
                'profile_image' => $twitterAccount->getTwProfileImage(),
                'followers_count' => GeneralHelpers::number_format_short($twitterAccount->getTwFollowersCount()),
                'friends_count' => GeneralHelpers::number_format_short($twitterAccount->getTwFriendsCount()),
                'statuses_count' => GeneralHelpers::number_format_short($twitterAccount->getTwStatusesCount()),
                'joined' => $twitterAccount->getTwCreatedAt() != "" ? $twitterAccount->getTwCreatedAt()->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($twitterAccount->getModifiedAt()->format('Y-m-d H:i:s')),
                'is_verified' => boolval($twitterAccount->getTwVerified()) ? true : false,
                'created_at' => date_format($twitterAccount->getCreatedAt(), 'F j, Y'),
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $output['success'] = true;
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $output['output'] = $this->renderView('TwitterController/widgets/twitter-profile-card.html.twig', array(
                'data' => $profileData,
                'actions' => $profileActionsHTML
            ));

            $output['tw_username'] = $twitterAccount->getTwUsername();
        } else {
            $output['success'] = false;
            $output['error'] = "Unable to fetch information of @{$twUsername}.<br>Check if username is correct.";
        }

        $response = GeneralHelpers::makeJsonResponse($output);
        return $response;
    }

    /**
     * @Route("/fetch-followers-friends/{tw_username}", name="twitter_fetch_followers_friends", defaults={"tw_username"=""})
     */
    public function fetchFollowersAndFriendsAction(Request $request, TwitterHelpers $twitterHelpers, TwitterAccountsRoutines $twitterAccountsRoutines, $tw_username)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $output = array(
            'success' => false,
            'message' => '',
            'error' => '',
            'fetchMessage' => ""
        );

        $twitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->findOneBy(array(
                'twUsername' => $tw_username
            ));
        if (!$this->get('session')->has('selectedAccountTwUserId') || !$twitterAccount || !$tw_username) {
            $output['success'] = false;
            $output['error'] = "No twitter account found.";
            return GeneralHelpers::makeJsonResponse($output);
        }
        if ($twitterAccount->getTwFollowersCount() > 500000 || $twitterAccount->getTwFriendsCount() > 500000) {
            $output['success'] = false;
            $output['error'] = "Followers/Following of @{$twitterAccount->getTwUsername()} is too high, we don't allow such big accounts. Max followers/following must be less than 500000";
            return GeneralHelpers::makeJsonResponse($output);
        }
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'userId' => $this->getUser()->getId(),
                'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
            ));
        $fetchTasksLog = $this->getDoctrine()->getRepository('App:FetchTasksLogs')
            ->createQueryBuilder('fetchTasksLogs')
            ->select('fetchTasksLogs')
            ->where('fetchTasksLogs.twitterAccount = :twitterAccount')
            ->setParameter('twitterAccount', $twitterAccount)
            ->orderBy('fetchTasksLogs.datetime', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        if (!$fetchTasksLog) {
            $followersIDs = array();
            $friendsIDs = array();

            $followers = $this->getDoctrine()->getRepository('App:TwitterFollowers')
                ->createQueryBuilder('twitterFollowers')
                ->select("count(twitterFollowers.id)")
                ->where('twitterFollowers.twUserId = :twUserId')
                ->setParameter('twUserId', $twitterAccount->getTwUserId())
                ->getQuery()
                ->getSingleScalarResult();
            if ($followers == 0 && $twitterAccount->getTwFollowersCount() > 0) {
                $rate_limits = $this->get('session')->get('rate_limits', array());
                if (isset($rate_limits['followers_ids'])) {
                    if ($rate_limits['followers_ids_reset'] > time()) {
                        $processResponse = array(
                            'success' => false,
                            'error' => "Twitter rate limit error. "
                        );
                        return GeneralHelpers::makeJsonResponse($processResponse);
                    }
                }
                $limit = $twitterHelpers->get_rate_limits($authCreds, "/followers/ids");
                if ((isset($limit->remaining) && $limit->remaining == 0) || !isset($limit->remaining)) {
                    $rate_limits['followers_ids'] = true;
                    $rate_limits['followers_ids_reset'] = ($limit->reset + 60);
                    $this->get('session')->set('rate_limits', $rate_limits);
                    $processResponse = array(
                        'success' => false,
                        'error' => "Twitter rate limit error. "
                    );
                    return GeneralHelpers::makeJsonResponse($processResponse);
                }
                $followersIDs = $twitterHelpers->getFollowersIDs($authCreds, $twitterAccount->getTwUserId(), false, false, 1);
            }
            $friends = $this->getDoctrine()->getRepository('App:TwitterFriends')
                ->createQueryBuilder('twitterFriends')
                ->select("count(twitterFriends.id)")
                ->where('twitterFriends.twUserId = :twUserId')
                ->setParameter('twUserId', $twitterAccount->getTwUserId())
                ->getQuery()
                ->getSingleScalarResult();
            if ($friends == 0 && $twitterAccount->getTwFriendsCount() > 0) {
                $rate_limits = $this->get('session')->get('rate_limits', array());
                if (isset($rate_limits['friends_ids'])) {
                    if ($rate_limits['friends_ids_reset'] > time()) {
                        $processResponse = array(
                            'success' => false,
                            'error' => "Twitter rate limit error. "
                        );
                        return GeneralHelpers::makeJsonResponse($processResponse);
                    }
                }
                $limit = $twitterHelpers->get_rate_limits($authCreds, "/friends/ids");
                if ((isset($limit->remaining) && $limit->remaining == 0) || !isset($limit->remaining)) {
                    $rate_limits['friends_ids'] = true;
                    $rate_limits['friends_ids_reset'] = ($limit->reset + 60);
                    $this->get('session')->set('rate_limits', $rate_limits);
                    $processResponse = array(
                        'success' => false,
                        'error' => "Twitter rate limit error. "
                    );
                    return GeneralHelpers::makeJsonResponse($processResponse);
                }
                $friendsIDs = $twitterHelpers->getFriendsIDs($authCreds, $twitterAccount->getTwUserId(), false, false, 1);
            }
            $followersIDs = array_reverse($followersIDs);
            $slicedFollowersIDs = array_slice($followersIDs, 0, 2000);
            $slicedFollowersIDs = array_reverse($slicedFollowersIDs);
            if ($slicedFollowersIDs) {
                $followersData = $twitterHelpers->executeFollowersTask($authCreds, $twitterAccount->getTwUserId(), $slicedFollowersIDs, false, false);
            }

            $friendsIDs = array_reverse($friendsIDs);
            $slicedFriendsIDs = array_slice($friendsIDs, 0, 2000);
            $slicedFriendsIDs = array_reverse($slicedFriendsIDs);
            if ($slicedFriendsIDs) {
                $friendsData = $twitterHelpers->executeFriendsTask($authCreds, $twitterAccount->getTwUserId(), $slicedFriendsIDs, false, false);
            }

            $followersFriendsIDs = array_merge($slicedFollowersIDs, $slicedFriendsIDs);
            if (count($followersFriendsIDs) > 0) {
                $accountsUpdateData = $twitterHelpers->executeTwAccountsUpdateTask($followersFriendsIDs, false);
            }
            $this->addFlash('info', "We have fetched some data for now, detailed data is being fetched in background and will be available soon.");
        }
        $fetchTasksLogInLast7Days = $this->getDoctrine()->getRepository('App:FetchTasksLogs')
            ->createQueryBuilder('fetchTasksLogs')
            ->select('fetchTasksLogs')
            ->where('fetchTasksLogs.twitterAccount = :twitterAccount')
            ->andWhere('fetchTasksLogs.datetime > :date')
            ->setParameters(array(
                'twitterAccount' => $twitterAccount,
                'date' => (new \DateTime())->modify('-7 day')
            ))
            ->getQuery()
            ->getResult();
        if (count($fetchTasksLogInLast7Days) == 0) {
            $params = array(
                'tw_user_id' => $twitterAccount->getTwUserId()
            );
            if (!$fetchTasksLog) {
                $params['delete_existing'] = true;
            }
            $taskDatetime = (new \DateTime())->modify("+1 hour");
            $tasksToCreate = array(
                TwitterAccountsRoutines::TASK_NAME_FOR_FOLLLOWERS,
                TwitterAccountsRoutines::TASK_NAME_FOR_FRIENDS
            );
            $twitterAccountsRoutines->handleCreateTasks($authCreds, $tasksToCreate, $params, $taskDatetime, false);
        }
        $response = array(
            'success' => true,
            'message' => "",
            "error" => "",
            "tw_username" => $tw_username
        );
        return GeneralHelpers::makeJsonResponse($response);
    }

    /**
     * @Route("/account-followers/{tw_username}", name="twitter_account_followers", defaults={"tw_username"=""})
     */
    public function accountFollowersAction(Request $request, TwitterHelpers $twitterHelpers, $tw_username)
    {
        return $this->render('TwitterController/account_followers.html.twig', array(
            'tw_username' => $tw_username
        ));
    }

    /**
     * @Route("/account-followers-ajax/{tw_username}", name="twitter_account_followers_ajax", defaults={"tw_username"=""})
     */
    public function accountFollowersAjaxAction(Request $request, TwitterHelpers $twitterHelpers, $tw_username)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $startTime = microtime(true);
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }

        if (!$tw_username) {
            $response = array(
                "draw" => intval($request->get('draw')),
                "recordsTotal" => "0",
                "recordsFiltered" => "0",
                "data" => array()
            );
            return GeneralHelpers::makeJsonResponse($response);
        }

        $searchedAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->findOneBy(array(
                'twUsername' => $tw_username
            ));
        $myTwitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->findOneBy(array(
                'twUserId' => $twUserId
            ));
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $orderCol = $order[0]['column'] == 2 ? "myFollowers.id" : null;
        $orderDir = $order[0]['dir'];
        $orderCol = "myFollowers.id";
        $orderDir = "DESC";


        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.myFollowers', 'myFollowers')
            ->select('myFollowers.createdAt')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('myFollowers.twUserId = :twUserId')
            ->setParameter('twUserId', $searchedAccount->getTwUserId());

        $query = $twitterHelpers->applyDatatableFilters($query, "myFollowers", $request, $authCreds);
        if ($orderCol) {
            $query->orderBy($orderCol, $orderDir);
        }
        $filteredRecords = clone $query;
        $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:TwitterFollowers')
            ->createQueryBuilder('twitterFollowers')
            ->select('count(twitterFollowers.twUserIdX) as total')
            ->where('twitterFollowers.twUserId = :twUserId')
            ->setParameter('twUserId', $searchedAccount->getTwUserId())
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'created_at' => date_format($result['createdAt'], 'F j, Y'),
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'created_at' => date_format($result['createdAt'], 'M j, Y'),
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }
        $endTime = microtime(true);
        $performance = array(
            "start" => $startTime,
            "end" => $endTime,
            "time" => $endTime - $startTime,
            "memory" => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true)),
        );

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray,
            'debug' => array(
                "performance" => $performance
            )
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/account-friends/{tw_username}", name="twitter_account_friends", defaults={"tw_username"=""})
     */
    public function accountFriendsAction(Request $request, TwitterHelpers $twitterHelpers, $tw_username)
    {
        return $this->render('TwitterController/account_friends.html.twig', array(
            'tw_username' => $tw_username
        ));
    }

    /**
     * @Route("/account-friends-ajax/{tw_username}", name="twitter_account_friends_ajax", defaults={"tw_username"=""})
     */
    public function accountFriendsAjaxAction(Request $request, TwitterHelpers $twitterHelpers, $tw_username)
    {
        if (!$request->isXmlHttpRequest()) {
            $response = array("success" => false, "error" => "False request.");
            return GeneralHelpers::makeJsonResponse($response, Response::HTTP_BAD_REQUEST);
        }
        $startTime = microtime(true);
        $session = $this->get('session');
        $twUserId = $session->get('selectedAccountTwUserId');
        $twAuthCredsId = 0;
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'twUserId' => $twUserId,
                'userId' => $this->getUser()->getId()
            ));
        if ($authCreds) {
            $twAuthCredsId = $authCreds->getId();
        }

        if (!$tw_username) {
            $response = array(
                "draw" => intval($request->get('draw')),
                "recordsTotal" => "0",
                "recordsFiltered" => "0",
                "data" => array()
            );
            return GeneralHelpers::makeJsonResponse($response);
        }

        $searchedAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->findOneBy(array(
                'twUsername' => $tw_username
            ));
        $myTwitterAccount = $this->getDoctrine()->getRepository('App:TwitterAccounts')
            ->findOneBy(array(
                'twUserId' => $twUserId
            ));
        $draw = $request->get('draw');
        $start = $request->get('start', false);
        $length = $request->get('length', false);
        $order = $request->get('order');
        $search = $request->get('search');
        $search = ($search['value']) ? $search['value'] : "";
        $orderCol = $order[0]['column'] == 2 ? "myFriends.id" : null;
        $orderDir = $order[0]['dir'];
        $orderCol = "myFriends.id";
        $orderDir = "DESC";


        $twitterAccounts = $this->getDoctrine()->getRepository('App:TwitterAccounts');
        $queryBuilder = $twitterAccounts->createQueryBuilder('twitterAccount');

        $query = $queryBuilder->innerJoin('twitterAccount.myFriends', 'myFriends')
            ->select('myFriends.createdAt')
            ->addSelect('twitterAccount.twName')
            ->addSelect('twitterAccount.twUsername')
            ->addSelect('twitterAccount.twUserId')
            ->addSelect('twitterAccount.twProfileImage')
            ->addSelect('twitterAccount.twFollowersCount')
            ->addSelect('twitterAccount.twFriendsCount')
            ->addSelect('twitterAccount.twStatusesCount')
            ->addSelect('twitterAccount.twVerified')
            ->addSelect('twitterAccount.twCreatedAt')
            ->addSelect('twitterAccount.modifiedAt')
            ->where('myFriends.twUserId = :twUserId')
            ->setParameter('twUserId', $searchedAccount->getTwUserId());

        $query = $twitterHelpers->applyDatatableFilters($query, "myFriends", $request, $authCreds);

        if ($orderCol) {
            $query->orderBy($orderCol, $orderDir);
        }
        $filteredRecords = clone $query;
        $query->setMaxResults($length)->setFirstResult($start);
        $results = $query->getQuery()
            ->getArrayResult();
        $filteredRecords = $filteredRecords->select('count(twitterAccount.id)')
            ->getQuery()->getSingleScalarResult();
        $totalRecords = ($this->getDoctrine()->getRepository('App:TwitterFriends')
            ->createQueryBuilder('twitterFriends')
            ->select('count(twitterFriends.twUserIdX) as total')
            ->where('twitterFriends.twUserId = :twUserId')
            ->setParameter('twUserId', $searchedAccount->getTwUserId())
            ->getQuery()->setMaxResults(1)->getOneOrNullResult())['total'];

        $resultArray = array();
        foreach ($results as $result) {
            $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->find($twUserId);
            $isFriend = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFriend($twUserId, $result['twUserId']);
            $isFollower = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isFollower($twUserId, $result['twUserId']);
            $isWhitelisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isWhitelisted($twAuthCredsId, $result['twUserId']);
            $isBlacklisted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isBlacklisted($twAuthCredsId, $result['twUserId']);
            $isMuted = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->isMuted($twUserId, $result['twUserId']);
            $profileData = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'],
                'name' => $result['twName'],
                'profile_image' => $result['twProfileImage'],
                'followers_count' => GeneralHelpers::number_format_short($result['twFollowersCount']),
                'friends_count' => GeneralHelpers::number_format_short($result['twFriendsCount']),
                'statuses_count' => GeneralHelpers::number_format_short($result['twStatusesCount']),
                'joined' => $result['twCreatedAt'] != "" ? $result['twCreatedAt']->format('M Y') : "",
                'last_updated_at' => GeneralHelpers::time_elapsed_string($result['modifiedAt']->format('Y-m-d H:i:s')),
                'is_verified' => boolval($result['twVerified']) ? true : false,
                'created_at' => date_format($result['createdAt'], 'F j, Y'),
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'is_muted' => $isMuted ? true : false,
            );
            $profileCardHTML = $this->renderView('TwitterController/widgets/twitter-profile-card-for-row.html.twig', array(
                'data' => $profileData
            ));
            $profileActionsHTML = $this->renderView('TwitterController/widgets/twitter-profile-actions-for-row.html.twig', array(
                'data' => $profileData
            ));
            $resultArray[] = array(
                "user_id" => $result['twUserId'],
                'username' => $result['twUsername'] != "" ? $result['twUsername'] : "N/A",
                'profile_card' => $profileCardHTML,
                'profile_actions' => $profileActionsHTML,
                'created_at' => date_format($result['createdAt'], 'M j, Y'),
                'actions' => "",
                'is_friend' => $isFriend ? true : false,
                'is_follower' => $isFollower ? true : false,
                'is_whitelisted' => $isWhitelisted ? true : false,
                'is_blacklisted' => $isBlacklisted ? true : false,
                'DT_RowId' => $result['twUserId'] . "_row"
            );
        }
        $endTime = microtime(true);
        $performance = array(
            "start" => $startTime,
            "end" => $endTime,
            "time" => $endTime - $startTime,
            "memory" => GeneralHelpers::getReadableMemorySize(memory_get_peak_usage(true)),
        );

        $responseArray = array(
            'draw' => intval($draw),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $filteredRecords,
            'data' => $resultArray,
            'debug' => array(
                'performance' => $performance
            )
        );
        $response = GeneralHelpers::makeJsonResponse($responseArray);
        return $response;

    }

    /**
     * @Route("/super-actions", name="twitter_super_action")
     */
    public function superActionsAction(Request $request)
    {
        return $this->render('TwitterController/super_actions.html.twig');
    }

    /**
     * @Route("/limits", name="twitter_rate_limits")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function limitsAction(Request $request, TwitterHelpers $twitterHelpers)
    {
        $user = $this->getUser();
        $authCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
            ->findOneBy(array(
                'userId' => $this->getUser()->getId(),
                'twUserId' => $this->get('session')->get('selectedAccountTwUserId')
            ));
        $limits = $twitterHelpers->get_rate_limits($authCreds);
        $response = GeneralHelpers::makeJsonResponse($limits);
        return $response;
    }

    /**
     * @Route(path="/oauth", name="twitter_oauth")
     * @throws \Abraham\TwitterOAuth\TwitterOAuthException
     */
    public function oauthAction(Request $request)
    {
        $twoauth = new TwitterOAuth($this->getParameter("tw_consumer_key"), $this->getParameter("tw_consumer_secret"));
        $twoauth->setTimeouts(15, 20);
        $request_token = $twoauth->oauth(
            'oauth/request_token', [
                'oauth_callback' => $this->getParameter("tw_callback_url")
            ]
        );

        if ($twoauth->getLastHttpCode() != 200) {
            $this->addFlash("error", "There was an problem with twitter authentication.");
            return $this->redirectToRoute('dashboard_settings', array(
                'active_tab' => 'connected-accounts'
            ));
        }

        $request->getSession()->set('oauth_token', $request_token['oauth_token']);
        $request->getSession()->set('oauth_token_secret', $request_token['oauth_token_secret']);

        $url = $twoauth->url(
            'oauth/authorize', [
                'oauth_token' => $request_token['oauth_token']
            ]
        );
        return $this->redirect($url);
    }


    /**
     * @Route(path="/callback", name="twitter_oauth_callback")
     */
    public function callbackAction(Request $request, TwitterHelpers $twitterHelpers, TwitterAccountsRoutines $twitterAccountsRoutines)
    {
        $oauth_verifier = filter_input(INPUT_GET, 'oauth_verifier');
        if (empty($oauth_verifier) ||
            empty($request->getSession()->get('oauth_token')) ||
            empty($request->getSession()->get('oauth_token_secret'))
        ) {
            $this->addFlash("error", "Something went wrong, please try again.");
            return $this->redirectToRoute('dashboard_settings', array(
                'active_tab' => 'connected-accounts'
            ));
        }
        $twoauth = new TwitterOAuth(
            $this->getParameter('tw_consumer_key'),
            $this->getParameter('tw_consumer_secret'),
            $request->getSession()->get('oauth_token'),
            $request->getSession()->get('oauth_token_secret')
        );
        $twoauth->setTimeouts(15, 20);
        $token = $twoauth->oauth(
            'oauth/access_token', [
                'oauth_verifier' => $oauth_verifier
            ]
        );
        $apiAuth = new TwitterOAuth($this->getParameter('tw_consumer_key'), $this->getParameter('tw_consumer_secret'),
            $token['oauth_token'], $token['oauth_token_secret']
        );
        $apiAuth->setTimeouts(15, 20);
        $result = $apiAuth->get('account/verify_credentials', array(
            'include_entities' => 'false',
            'skip_status' => 'false'
        ));
        if (isset($result->screen_name)) {
            $twitterHelpers->saveTwitterUser($result);

            $existing_auth_flag = false;
            $user = $this->getDoctrine()->getRepository('App:Users')
                ->find($this->getUser()->getId());
            $authCred = $this->getDoctrine()->getRepository('App:AuthCreds')
                ->findOneBy(array(
                    'twUserId' => $token['user_id'],
                ));
            $existingAuthCredsForAnotherUser = false;
            if (!$authCred) { // Check if twitter user_id exists
                $existingAuthCreds = $this->getDoctrine()->getRepository('App:AuthCreds')
                    ->createQueryBuilder('authCreds')
                    ->select('authCreds')
                    ->where('authCreds.userId = :user_id')
                    ->andWhere('authCreds.platformId = :platform_id')
                    ->setParameters(array(
                        'user_id' => $user->getId(),
                        'platform_id' => $this->getParameter('tw_platform_id')
                    ))
                    ->getQuery()
                    ->getResult();
                if (count($existingAuthCreds) >= $this->getParameter('tw_max_accounts')) {
                    $this->addFlash("error", "You can not add more than "
                        . $this->getParameter('tw_max_accounts') . " account/s.");
                    $this->addFlash("info", "Please upgrade your account to increase this limit. 
                    (Will be available soon!)");
                    return $this->redirectToRoute('dashboard_settings', array(
                        'active_tab' => 'connected-accounts'
                    ));
                }
                $authCred = new AuthCreds();
            } else {
                $existingAuthCredsForAnotherUser = $this->getDoctrine()->getRepository('App:AuthCreds')
                    ->createQueryBuilder('authCreds')
                    ->select('authCreds')
                    ->where('authCreds.twUserId = :tw_user_id')
                    ->andWhere('authCreds.userId != :user_id')
                    ->andWhere('authCreds.platformId = :platform_id')
                    ->setParameters(array(
                        'tw_user_id' => $token['user_id'],
                        'user_id' => $user->getId(),
                        'platform_id' => $this->getParameter('tw_platform_id')
                    ))
                    ->getQuery()
                    ->getResult();
                if (count($existingAuthCredsForAnotherUser) > 0) {
                    $existingAuthCredsForAnotherUser = true;
                }
                $existing_auth_flag = true;
            }
            $authCred->setTwOauthToken($token['oauth_token']);
            $authCred->setTwOauthTokenSecret($token['oauth_token_secret']);
            $authCred->setTwUserId($token['user_id']);
            $authCred->setTwUsername($token['screen_name']);
            if (!$existingAuthCredsForAnotherUser) {
                $authCred->setUser($user);
            }
            $platform = $this->getDoctrine()->getRepository('App:Platforms')
                ->find(1);
            $authCred->setPlatform($platform);
            $currentDatetime = new \DateTime();
            if (!$existing_auth_flag) { // Only set when creating new record
                $authCred->setCreatedAt($currentDatetime);
            }
            $authCred->setModifiedAt($currentDatetime);
            $authCred->setEnabled('1');
            $em = $this->getDoctrine()->getManager();
            $em->persist($authCred);
            $em->flush();
            if ($existingAuthCredsForAnotherUser) {
                $this->addFlash("error", "This twitter account is in use of another user, please " .
                    "remove it from there and only then you can associate it with current account.");
            } else if ($existing_auth_flag) {
                $this->addFlash("info", "Account is updated successfully.");
            } else {
                $twitterAccountsRoutines->createAndRunTasksForAUser($authCred);
                $this->addFlash("success", "Account is connected successfully");
                $this->addFlash("info", "We are fetching your information, it will take a few minutes to appear.");
            }
        } else {
            $this->addFlash("error", "Unable to verify your account's credentials. Please try again.");
        }
        $this->get('session')->getFlashBag()->get('noSelectedAccountWarning');
        return $this->redirectToRoute('dashboard_settings', array(
            'active_tab' => 'connected-accounts'
        ));
    }

}
